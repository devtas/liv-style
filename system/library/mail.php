<?php
class Mail {
	protected $to;
	protected $from;
	protected $sender;
	protected $replyto;
	protected $subject;
	protected $text;
	protected $html;
	protected $attachments = array();
	public $protocol = 'mail';
	public $smtp_hostname;
	public $smtp_username;
	public $smtp_password;
	public $smtp_port = 25;
	public $smtp_timeout = 5;
	public $newline = "\n";
	public $verp = false;
	public $parameter = '';

	public function __construct($config = array()) {
		foreach ($config as $key => $value) {
			if ($value) {
				$this->$key = $value;
			}
		}
	}

	public function setTo($to) {
		$this->to = html_entity_decode($to, ENT_QUOTES, 'UTF-8');
	}

	public function setFrom($from) {
		$this->from = html_entity_decode($from, ENT_QUOTES, 'UTF-8');
	}

	public function setSender($sender) {
		$this->sender = html_entity_decode($sender, ENT_QUOTES, 'UTF-8');
	}

	public function setReplyTo($reply_to) {
		$this->replyto = html_entity_decode($reply_to, ENT_QUOTES, 'UTF-8');
	}

	public function setSubject($subject) {
		$this->subject = html_entity_decode($subject, ENT_QUOTES, 'UTF-8');
	}

	public function setText($text) {
		$this->text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
	}

	public function setHtml($html) {
		$this->html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
	}

	public function addAttachment($filename) {
		$this->attachments[] = $filename;
	}

	public function send() {
		require_once(DIR_SYSTEM . 'library/SendGridLibrary/sendgrid-php.php');
		// require_once(__DIR__ . '/SendGridLibrary/sendgrid-php.php');
		
		if(!$this->html) {
			$this->html = $this->text;
		}
		
		$from = new \SendGrid\Email("LIV Style", $this->from);
		$subject = $this->subject;
		$to = new \SendGrid\Email(null, $this->to);
		$content = new \SendGrid\Content("text/html", $this->html);
		$mail = new \SendGrid\Mail($from, $subject, $to, $content);

		// MUDAR
		$apiKey = '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';
		$sg = new \SendGrid($apiKey);

		$response = $sg->client->mail()->send()->post($mail);
	}
}