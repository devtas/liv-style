<?php
class ControllerCommonSeoUrl extends Controller {

	private $urlCustom = array (
		'common/home' => '',
		'account/account' => 'conta',
		'account/wishlist' => 'conta/desejos',
		'account/wishlist/add' => 'conta/desejos/adicionar',
		'account/register' => 'conta/cadastro',
		'account/success' => 'conta/confirmacao',
		'account/login' => 'conta/acessar',
		'account/forgotten' => 'conta/recuperar-senha',
		'account/edit' => 'conta/informacoes',
		'account/password' => 'conta/alterar-senha',
		'account/address' => 'conta/enderecos',
		'account/address/info' => 'conta/enderecos/editar',
		'account/address/add' => 'conta/enderecos/cadastro',
		'account/reward' => 'conta/pontos',
		'account/logout' => 'conta/sair',
		'account/order' => 'conta/pedidos',
		'account/order/info' => 'conta/pedido',
		'account/newsletter' => 'conta/newsletter',
		'account/download' => 'conta/downloads',
		'account/transaction' => 'conta/creditos',
		'account/recurring' => 'conta/assinaturas',
		'account/return' => 'conta/devolucoes',
		'account/return/add' => 'devolucoes/cadastro',
		'account/return/success' => 'devolucoes/confirmacao',
		'account/voucher' => 'vale-presentes/comprar',
		'account/voucher/success' => 'vale-presentes/confirmacao',
		'checkout/coupon/coupon' => 'cupom/cupom',
		'checkout/voucher/voucher' => 'vale-presentes/vale-presentes',
		'affiliate/account' => 'parceiros/conta',
		'affiliate/edit' => 'parceiros/editar',
		'affiliate/password' => 'parceiros/alterar-senha',
		'affiliate/payment' => 'parceiros/informacoes',
		'affiliate/tracking' => 'parceiros/gerador-links',
		'affiliate/tracking/autocomplete' => 'parceiros/gerador-links/autocompletar',
		'affiliate/transaction' => 'parceiros/creditos',
		'affiliate/logout' => 'parceiros/sair',
		'affiliate/forgotten' => 'parceiros/recuperar-senha',
		'affiliate/register' => 'parceiros/cadastro',
		'affiliate/login' => 'parceiros/acessar',
		'affiliate/success' => 'parceiros/confirmacao',
		'checkout/cart' => 'carrinho',
		'checkout/cart/add' => 'carrinho/adicionar',
		'checkout/cart/edit' => 'carrinho/editar',
		'checkout/cart/remove' => 'carrinho/remover',
		'common/cart/info' => 'carrinho/info',
		'common/cart/number' => 'carrinho/quantidade',
		'checkout/checkout' => 'carrinho/finalizar',
		'checkout/success' => 'carrinho/confirmacao',
		// 'payment/redepay/order' => 'redepay/pedido',
		// 'payment/redepay/confirm' => 'redepay/confirmacao',
		'information/contact' => 'contato',
		'information/contact/success' => 'contato/enviado',
		'information/sitemap' => 'mapa-do-site',
		'product/special' => 'ofertas',
		'product/manufacturer' => 'marcas',
		'product/compare' => 'comparacao',
		'product/compare/add' => 'comparacao/adicionar',
		'product/product/review' => 'review',
		'product/product/write' => 'review/adicionar',
		'product/product/getRecurringDescription' => 'produto/descricao',
		'product/search' => 'busca',
		'module/kuler_cp/liveSearch' => 'busca/live',
		'module/kuler_cp/quick_view' => 'view',
		'module/kuler_cp/onePageCheckoutLogin' => 'carrinho/conta/acessar',
		'module/kuler_cp/onePageCheckoutValidate' => 'carrinho/finalizar/validar',
		'payment/cod/confirm' => 'carrinho/pagamento/confirmacao',
		'module/kuler_cp/createAddress' => 'carrinho/endereco/adicionar',
		'tool/captcha' => 'captcha',
		'checkout/shipping/quote' => 'frete/calcular',
		'checkout/shipping/shipping' => 'frete/escolher',
		'module/kuler_cp/onePageCheckoutMethods' => 'checkout/methods',
		'module/kuler_cp/applyCoupon' => 'cupom/aplicar',
		'module/kuler_cp/applyVoucher' => 'vale-presentes/aplicar',
		'checkout/checkout/country' => 'carrinho/pais',
		'checkout/shipping/country' => 'carrinho/entrega/pais',
		'account/account/country' => 'conta/pais',
		'affiliate/register/country' => 'parceiro/conta/pais',
		'feed/google_sitemap' => 'sitemap',
		'information/information/agree' => 'termos',
		'payment/rede_rest_credito/setTransacao' => 'checkout/rede/transacao',
		'payment/rede_rest_credito/bandeiras' => 'checkout/rede/bandeiras',
		'payment/rede_rest_credito/parcelas' => 'checkout/rede/parcelas',
		'payment/rede_rest_credito/comprovante' => 'checkout/rede/confirmacao',
		'payment/rede_rest_credito/imprimir' => 'checkout/rede/comprovante',
	);
		
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}

		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);

			// remove any empty arrays from trailing
			if (utf8_strlen(end($parts)) == 0) {
				array_pop($parts);
			}

			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");

				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);

					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}

					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}

					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}

					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}
					
					if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id') {
						$this->request->get['route'] = $query->row['query'];
					}
				} else {
					if (in_array($this->request->get['_route_'], $this->urlCustom)) {
						$this->request->get['route'] = array_search($this->request->get['_route_'], $this->urlCustom);
					} else {
						$this->request->get['route'] = 'error/not_found';
					}

					break;
				}
			}

			if (!isset($this->request->get['route'])) {
				if (isset($this->request->get['product_id'])) {
					$this->request->get['route'] = 'product/product';
				} elseif (isset($this->request->get['path'])) {
					$this->request->get['route'] = 'product/category';
				} elseif (isset($this->request->get['manufacturer_id'])) {
					$this->request->get['route'] = 'product/manufacturer/info';
				} elseif (isset($this->request->get['information_id'])) {
					$this->request->get['route'] = 'information/information';
				}
			}

			if (isset($this->request->get['route'])) {
				return new Action($this->request->get['route']);
			}
		}
	}

	public function rewrite($link) {
		$url_info = parse_url(str_replace('&amp;', '&', $link));

		$url = '';

		$data = array();

		parse_str($url_info['query'], $data);

		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");

					if ($query->num_rows && $query->row['keyword']) {
						$url .= '/' . $query->row['keyword'];

						unset($data[$key]);
					}
				} elseif ($key == 'path') {
					$categories = explode('_', $value);

					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");

						if ($query->num_rows && $query->row['keyword']) {
							$url .= '/' . $query->row['keyword'];
						} else {
							$url = '';

							break;
						}
					}

					unset($data[$key]);
				} else {
					$this->urlCustom = array_flip($this->urlCustom);
					if (in_array($data['route'], $this->urlCustom)) {
						$url = '/' . array_search($data['route'], $this->urlCustom);
					}
					$this->urlCustom = array_flip($this->urlCustom);
				}
			}
		}

		if ($url) {
			unset($data['route']);

			$query = '';

			if ($data) {
				foreach ($data as $key => $value) {
					$query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((string)$value);
				}

				if ($query) {
					$query = '?' . str_replace('&', '&amp;', trim($query, '&'));
				}
			}

			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {
			return $link;
		}
	}
}