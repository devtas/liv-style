<?php
	$kuler = Kuler::getInstance();
	$theme = $kuler->getTheme();
	$kuler->language->load('kuler/zorka');

	$kuler->addStyle(array(
		"catalog/view/theme/$theme/stylesheet/stylesheet.css",
	));

	$kuler->addScript(array(
		"https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"
	));

	$kuler->addScript(array(
		"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.0/js/bootstrap.min.js",
		"https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/0.9.9/jquery.magnific-popup.min.js",
		"catalog/view/theme/$theme/js/lib/main.js?updated=1534467054",
		"https://cdnjs.cloudflare.com/ajax/libs/jquery-parallax/1.1.3/jquery-parallax-min.js",
		"https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/owl.carousel.min.js",
		"catalog/view/theme/$theme/js/utils.js?updated=1534467054"
	), true);
?>

<?php
	if($title !== "LIV Style") {
		$title = $title . " | LIV Style";
	}

	$url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$urlAux = explode("/", $_SERVER['REQUEST_URI']);

	if(count($urlAux) === 3) {
		$url = "https://".$_SERVER['HTTP_HOST']."/".$urlAux[2];
	}

	$urlAux = explode("?", $url);

	if(count($urlAux) === 2) {
		$url = $urlAux[0];
	}

	$image = $base."image/catalog/config/image.png";
?>

<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="pt-br">
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<title><?php echo $title; ?></title>
		<meta name="author" content="LIV Style">
		<meta name="description" content="<?php echo $description; ?>" />
		<meta name="keywords" content="<?php echo $keywords; ?>" />
		<meta name="google-site-verification" content="tiOzZaYtebFG-IIuhdfM_plDrUTF4rA6cJg3h7Ds9ww" />
		<link rel="image_src" href="<?php echo $image; ?>" />
		<link rel="shortcut icon" href="<?php echo $icon; ?>" />
		<link rel="apple-touch-icon" href="<?php echo $base; ?>image/catalog/config/apple-touch-icon.png">
		<base href="<?php echo $base; ?>" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-title" content="<?php echo $title; ?>">
		<meta property="og:title" content="<?php echo $title; ?>" />
		<meta property="og:description" content="<?php echo $description; ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo $url; ?>" />
		<meta property="og:image" content="<?php echo $image; ?>" />

		<?php foreach ($links as $link) { ?>
		<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
		<?php } ?>
		<!-- {STYLES} -->
		<?php foreach ($styles as $style) { ?>
		<link href="<?php echo $style['href']; ?>?updated=1534467054" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
		<?php } ?>
		<!-- {SCRIPTS} -->
		<script src="catalog/view/javascript/common.js?v=0.0.2" type="text/javascript"></script>
		<?php foreach ($scripts as $script) { ?>
		<script src="<?php echo $script; ?>" type="text/javascript"></script>
		<?php } ?>
		<?php echo $google_analytics; ?>
		
		<script type="application/ld+json"> 
		{ 
			"@context": "http://schema.org", 
			"@type": "WebSite", 
			"name" : "Liv Style", 
			"alternateName" : "Liv Style", 
			"url": "https://www.livstyle.com.br/", 
			"potentialAction": { 
				"@type": "SearchAction", 
				"target": "https://www.livstyle.com.br/busca&description=true&search={search_term_string}", 
				"query-input": "required name=search_term_string" 
			} 
		} 
		</script>
	</head>

	<body class="<?php echo $class; ?> <?php echo $kuler->getBodyClass(); ?>">
		<?php
			$modules = Kuler::getInstance()->getModules('header_top');
			if ($modules) {
				echo '<div class="header-top"><div class="container">'.implode('', $modules).'</div></div>';
			}
		?>

		<nav class="topbar">
			<div class="container">
				<div class="top-whatsapp">

					<!--
					<a id="menu-fixed-whatsapp" href="https://api.whatsapp.com/send?phone=556298660525" title="Enviar Mensagem" target="_blank">
						<img src="/image/catalog/config/whatsapp.png"> <span>Varejo (11) 99010-9555</span>
					</a>

					-->
				</div>

				<div class="bag">
					<a class="icon-bag" href="javascript:void(0);"></a>
					<span class="total cart-product-total-number"><?php echo Kuler::getInstance()->cart->countProducts(); ?></span>
				</div>

				<div class="favorite">
					<a class="icon-favorite" href="<?php echo $wishlist; ?>"></a>
				</div>

				<div class="account">
					<a class="icon-account" href="/conta/acessar"></a>
				</div>

				<div class="row topbar-links">
					<!-- <div class="col-lg-7 col-md-7 topbar__left">
						<?php if ($logged) { ?>
							<a href="<?php echo $account; ?>"><?php echo $text_account; ?></a> |
							<a href="<?php echo $order; ?>"><?php echo $text_order; ?></a> |
							<a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a> |
							<a href="<?php echo $download; ?>"><?php echo $text_download; ?></a> |
							<a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a>
						<?php } else { ?> 
							<span><?php echo $kuler->language->get('text_welcome'); ?></span>
							<a href="/conta/cadastro"><?php echo $text_register; ?></a>
							<span> <?php echo $kuler->language->get('text_or'); ?></span>
							<a href="/conta/acessar"><?php echo $text_login; ?></a>
						<?php } ?>
					</div>
					<div class="col-lg-5 col-md-5 topbar__right">
						<a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a> |
						<a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a> |
						<a href="<?php echo $checkout ?>"><?php echo $text_checkout; ?></a>
					</div> -->
				</div>

				<header class="header box-search">
					<div class="container search-mobile">
						<div class="row">
							<div class="col-xs-12">
								<?php if ($kuler->getSkinOption('live_search_status')) { ?>
									<?php include(DIR_TEMPLATE . Kuler::getInstance()->getTheme() . '/template/common/_live_search.tpl');?>
								<?php } else { ?>
									<?php echo $search;?>
								<?php } ?>
							</div>
						</div>
					</div>
				</header>
			</div>
		</nav>

		<input id="menu-toogle" type="checkbox" class="menu-toogle">
		<nav class="menu-top">
			<div class="menu-content">
				<ul class="menu-header">
					<li class="menu-icon">
						<label class="menu-icon-label" for="menu-toogle">
							<span class="menu-icon-bread menu-icon-bread-top">
								<span class="menu-icon-bread-crust menu-icon-bread-crust-top"></span>
							</span>

							<span class="menu-icon-bread menu-icon-bread-bottom">
								<span class="menu-icon-bread-crust menu-icon-bread-crust-bottom"></span>
							</span>
						</label>

						<a class="ac-gn-menuanchor ac-gn-menuanchor-open" id="ac-gn-menuanchor-open" href="#"></a>
						<a class="ac-gn-menuanchor ac-gn-menuanchor-close" id="ac-gn-menuanchor-close" href="#"></a>
					</li>

					<li class="icon-ancora">
						<a class="ac-gn-link-ancora" href="/" id="ac-gn-firstfocus-small"></a>
					</li>

					<li class="icon-search">
						<a class="ac-gn-link-search" href="javascript:void(0);"></a>
					</li>

					<li class="icon-favorite">
						<a class="ac-gn-link-favorite" href="/conta"></a>
					</li>

					<li class="icon-account">
						<?php if ($logged) { ?>
							<a class="ac-gn-link-account" href="/conta"></a>
						<?php } else { ?>
							<a class="ac-gn-link-account" href="/conta/acessar"></a>
						<?php } ?>
					</li>

					<li class="icon-bag" id="ac-gn-bag-small">
						<span class="cart-product-total-number"><?php echo Kuler::getInstance()->cart->countProducts(); ?></span>
						<a class="ac-gn-link-bag" href="javascript:void(0);"></a>
						<span class="cart-content-caret"></span>
					</li>
				</ul>

				<ul class="menu-itens">
					<li class="icon-ancora">
						<a class="ac-gn-link-ancora" href="/"></a>
					</li>

					<?php
						$modules = Kuler::getInstance()->getModules('menu');
						if ($modules) {
							echo implode('', $modules);
						}
					?>

					<li>
						<a href="/ofertas" style="color:#AD2031; font-weight: 600;">PROMO</a>
					</li>
				</ul>

				<?php echo $cart; ?>
			</div>
		</nav>

		<header class="header box-search">
			<div class="container search-mobile">
				<div class="row">
					<div class="col-xs-12">
						<?php if ($kuler->getSkinOption('live_search_status')) { ?>
							<?php include(DIR_TEMPLATE . Kuler::getInstance()->getTheme() . '/template/common/_live_search.tpl');?>
						<?php } else { ?>
							<?php echo $search;?>
						<?php } ?>
					</div>
				</div>
			</div>
		</header>

		<?php
			function renderSubMenuRecursive($categories) {
				$html = '<ul class="sublevel">';

				foreach ($categories as $category) {
					$parent = !empty($category['children']) ? ' parent' : '';
					$active = !empty($category['active']) ? ' active' : '';
					$html .= sprintf("<li class=\"item$parent $active\"><a href=\"%s\">%s</a>", $category['href'], $category['name']);

					if (!empty($category['children'])) {
						$html .= '<span class="btn-expand-menu"></span>';
						$html .= renderSubMenuRecursive($category['children']);
					}

					$html .= '</li>';
				}
				$html .= '</ul>';

				return $html;
			}
		?>
		<div class="kl-main-content">
			<?php
				$modules = Kuler::getInstance()->getModules('promotion');
				if ($modules) {
					echo implode('', $modules);
				}
			?>
