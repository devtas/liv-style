			<?php
				$kuler = Kuler::getInstance();

				$modules = Kuler::getInstance()->getModules('footer_top');
				if ($modules) {
					echo implode('', $modules);
				}
			?>

			<footer class="footer">
				<div class="container">
					<div class="row">
						<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 footer__information">
							<?php if ($kuler->getSkinOption('show_information')) { ?>
								<?php if ($kuler->getSkinOption('show_information_title')) { ?>
									<h3 class="box-heading divide"><span>Redes Sociais</span></h3>
								<?php } ?>
								<p class="footer__information-text">
									<?php echo $kuler->translate($kuler->getSkinOption('information_content')); ?>
								</p>
							<?php } ?>

							<?php if ($kuler->getSkinOption('show_contact')) { ?>
								<?php if ($kuler->getSkinOption('show_contact_title')) { ?>
									<h3 class="box-heading">
										<span><?php echo $kuler->translate($kuler->getSkinOption('contact_title')); ?></span>
									</h3>
								<?php } ?>
								<ul class="contact__list list-10">
									<?php if (($skype1 = $kuler->getSkinOption('skype_1')) || ($skype2 = $kuler->getSkinOption('skype_2'))) { ?>
										<li class="contact__item contact__item--skype">
											<?php if ($skype1) { ?>
												<span><?php echo $skype1; ?></span>
											<?php } ?>
											<?php if ($skype2 = $kuler->getSkinOption('skype_2') && $skype2) { ?>
												<span><?php echo $skype2; ?></span>
											<?php } ?>
										</li>
									<?php } ?>

									<?php if (($email1 = $kuler->getSkinOption('email_1')) || ($email2 = $kuler->getSkinOption('email_2'))) { ?>
										<li class="contact__item contact__item--email">
											<?php if ($email1) { ?>
												<span><?php echo $email1; ?></span>
											<?php } ?>
											<?php if ($email2 = $kuler->getSkinOption('email_2') && $email2) { ?>
												<span><?php echo $email2; ?></span>
											<?php } ?>
										</li>
									<?php } ?>

									<?php if (($mobile1 = $kuler->getSkinOption('mobile_1')) || ($mobile2 = $kuler->getSkinOption('mobile_2'))) { ?>
										<li class="contact__item contact__item--mobile">
											<?php if ($mobile1) { ?>
												<span><?php echo $mobile1; ?></span>
											<?php } ?>
											<?php if ($mobile2 = $kuler->getSkinOption('mobile_2') && $mobile2) { ?>
												<span><?php echo $mobile2; ?></span>
											<?php } ?>
										</li>
									<?php } ?>

									<?php if (($phone1 = $kuler->getSkinOption('phone_1')) || ($phone2 = $kuler->getSkinOption('phone_2'))) { ?>
										<li class="contact__item contact__item--phone">
											<?php if ($phone1) { ?>
												<span><?php echo $phone1; ?></span>
											<?php } ?>
											<?php if ($phone2 = $kuler->getSkinOption('phone_2') && $phone2) { ?>
												<span><?php echo $phone2; ?></span>
											<?php } ?>
										</li>
									<?php } ?>

									<?php if (($fax1 = $kuler->getSkinOption('fax_1')) || ($fax2 = $kuler->getSkinOption('fax_2'))) { ?>
										<li class="contact__item contact__item--fax">
											<?php if ($fax1) { ?>
												<span><?php echo $fax1; ?></span>
											<?php } ?>
											<?php if ($fax2 = $kuler->getSkinOption('fax_2') && $fax2) { ?>
												<span><?php echo $fax2; ?></span>
											<?php } ?>
										</li>
									<?php } ?>
								</ul>
							<?php } ?>

							<div class="social-icons">
								<?php if ($kuler->getSkinOption('show_social_icons')) { ?>
									<?php if ($kuler->getSkinOption('show_social_icons_title')) { ?>
										<h3><span><?php echo $kuler->translate($kuler->getSkinOption('social_icon_title')); ?></span></h3>
									<?php } ?>
									<?php if ($social_icons = $kuler->getSocialIcons()) { ?>
										<ul class="social-icons__list">
											<?php foreach ($social_icons as $social_icon) { ?>
												<li>
													<a href="<?php echo $social_icon['link']; ?>" target="_blank" class="<?php echo $social_icon['class']; ?> social-icons__item"></a>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								<?php } ?>
							</div>
						</div> -->

						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<h3 class="box-heading">Institucional</h3>
							<ul class="list-unstyled list-10">
								<li><a href="/sobre">Quem Somos</a></li>
								<li><a href="/lojas">Encontre uma Loja</a></li>
								<li><a href="/atacado">Atacado</a></li>
								<li><a href="/contato">Contato</a></li>
							</ul>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<h3 class="box-heading">Ajuda e Suporte</h3>
							<ul class="list-unstyled list-10">
								<li><a href="/conta">Minha Conta</a></li>
								<li><a href="/devolucoes/cadastro">Trocas e Devoluções</a></li>
								<li><a href="/envio-e-entrega">Envio e Entrega</a></li>
								<li><a href="/politica-de-privacidade">Política de Privacidade</a></li>
							</ul>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<h3 class="box-heading">Atendimento</h3>
							<ul class="list-unstyled list-10">
								<li><span>Segunda a sexta, 8h às 18h</span></li>
								<li><span>Whatsapp: (11) 99010-9555</span></li>
								<li><span>contato@livstyle.com.br</span></li>
							</ul>
						</div>

						<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<h3 class="box-heading"><?php echo $text_account; ?></h3>
							<ul class="list-unstyled list-10">
								<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
								<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
								<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
								<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
								<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
							</ul>
						</div> -->

						<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<?php if ($informations) { ?>
								<h3 class="box-heading"><?php echo $text_information; ?></h3>
								<ul class="list-unstyled list-10">
									<?php foreach ($informations as $information) { ?>
									<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
									<?php } ?>
								</ul>
							<?php } ?>
						</div> -->

						<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<h3 class="box-heading"><?php echo $text_extra; ?></h3>
							<ul class="list-unstyled list-10">
								<li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
								<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
								<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
								<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a>
									<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
							</ul>
						</div> -->
					</div>
				</div>

				<div class="footer-contact">
					<h3 class="box-heading">Acompanhe a LIV Style</h3>

					<div class="social-icons">
						<?php if ($kuler->getSkinOption('show_social_icons')) { ?>
							<?php if ($kuler->getSkinOption('show_social_icons_title')) { ?>
								<h3><span><?php echo $kuler->translate($kuler->getSkinOption('social_icon_title')); ?></span></h3>
							<?php } ?>
							<?php if ($social_icons = $kuler->getSocialIcons()) { ?>
								<ul class="social-icons__list">
									<?php foreach ($social_icons as $social_icon) { ?>
										<li>
											<a href="<?php echo $social_icon['link']; ?>" target="_blank" class="<?php echo $social_icon['class']; ?> social-icons__item"></a>
										</li>
									<?php } ?>
								</ul>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</footer>

			<div id="powered">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 copyright">
							<img class="image-logo" src="/image/catalog/config/logo-livstyle-white.svg" width="50px" />
							<a href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/index.html#url=https://www.livstyle.com.br" target="_blank">
								<p class="security-1">Loja segura com criptografia (SSL)</p>
								<p class="security-2">Blindado contra roubo de informações e clonagem de cartão.</p>
							</a>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 copyright">
							<p class="payments">Pague com cartão de crédito, boleto ou transferência/depósito bancário</p>
							<p>
								<i class="icon-payments icon-payments-visa"></i>
								<i class="icon-payments icon-payments-mastercard"></i>
								<i class="icon-payments icon-payments-amex"></i>
								<i class="icon-payments icon-payments-diners"></i>
								<i class="icon-payments icon-payments-hipercard"></i>
								<i class="icon-payments icon-payments-hiper"></i>
								<i class="icon-payments icon-payments-elo"></i>
							</p>
						</div>
					</div>
				</div>
				
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12 copyright text-center">
							<p><a href="/sobre">Liv Style Ltda - 20.852.860/0001-60 - Rua Correia de Melo, 84, sala 206 - Bom Retiro, São Paulo - SP, 01123-020 - Copyright &copy; <?php echo date("Y"); ?></a></p>
						</div>
					</div>
				</div>
			</div>

			<?php
				$modules = Kuler::getInstance()->getModules('footer_bottom');
				if ($modules) {
					echo implode('', $modules);
				}
			?>
		</div>

	
		<section id="whatsapp">
            <a id="menu-fixed-whatsapp" href="https://api.whatsapp.com/send?phone=556298660525" title="Enviar Mensagem" target="_blank">
                <img src="/image/catalog/config/whatsapp.png">
            </a>
        </section>


		<?php if ($kuler->getSkinOption('login_popup')) { ?>
			<?php $kuler->loginPopupInit($data); ?>
			<div style="display: none">
				<div id="login-popup">
					<div class="col-sm-12">
						<h2><?php echo _t('text_returning_customer'); ?></h2>
						<form id="popup-login-form" novalidate>
							<div class="content">
								<p><?php echo _t('text_i_am_returning_customer'); ?></p>
								<div class="form-group required">
									<div class="field">
										<input type="text" class="fld fld-primary" name="email" required />
										<label><?php echo _t('entry_email'); ?></label>
									</div>
								</div>
								
								<div class="form-group required">
									<div class="field">
										<input type="password" class="fld fld-primary" name="password" required />
										<label><?php echo _t('entry_password'); ?></label>
									</div>
								</div>
								<a href="<?php echo $data['forgotten']; ?>"><?php echo _t('text_forgotten'); ?></a><br />
								<br />
								<input type="submit" value="<?php echo _t('button_login'); ?>" class="button" />
							</div>
						</form>
						<br>
					</div>

					<div class="col-sm-12">
						<h2><?php echo _t('text_new_customer'); ?></h2>
						<div class="content">
							<p><?php echo _t('text_register_account'); ?></p><br>
							<p><a href="/conta/cadastro" class="register">clique aqui para criar sua conta</a></p>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<!-- {BODY_SCRIPTS} -->

		
		<!-- {MODAL POPUP} 
		<a id="popup-access" href="/termos&amp;information_id=8" class="first-day-look"></a>

		-->

		<script type="text/javascript">
		/*
		var _smartsupp = _smartsupp || {};
		_smartsupp.key = '3735576bd7a62c69f55d3d0b3c86e4cb5f8652ac';
		window.smartsupp||(function(d) {
			var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
			s=d.getElementsByTagName('script')[0];c=d.createElement('script');
			c.type='text/javascript';c.charset='utf-8';c.async=true;
			c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
		})(document);
		*/
		</script> 
	</body>
</html>
