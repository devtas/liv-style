<?php
$kuler = Kuler::getInstance();
$theme = $kuler->getTheme();
$kuler->addScript(array(
  "catalog/view/theme/$theme/js/lib/jquery.elevatezoom.js",
  "catalog/view/theme/$theme/js/product.js"
), true);
$kuler->language->load('kuler/zorka');
global $config;
?>

<?php
	$url = "https://";
	if(!((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443)) {
		$url = "http://";
	}

	$urlAux = explode("/", $_SERVER['REQUEST_URI']);

	if(count($urlAux) === 3) {
		$url = $url.$_SERVER['HTTP_HOST']."/".$urlAux[2];
	}
	else {
		$url = $url.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	}

	$urlAux = explode("?", $url);

	if(count($urlAux) === 2) {
		$url = $urlAux[0];
	}
?>



<?php
	$img = str_replace("540x540","270x270", $thumb);
	echo str_replace("https://www.livstyle.com/image/catalog/config/image.png", $img, $header);

	$seoQtdReviews = explode(" ", $reviews);
	$seoQtdReviews = $seoQtdReviews[0];
?>

<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Product",
        "name": "<?php echo $heading_title; ?>",
        "productID": "<?php echo $product_id; ?>",
        "image": "<?php echo $img; ?>",
        "description": "<?php echo strip_tags($description); ?>",
        "url" : "<?php echo $url; ?>",
        "brand": {
            "@type": "Thing",
            "name": "LIV Style"
        }, <?php if($rating > 0) { ?>
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "<?php echo $rating; ?>",
			"ratingCount": "<?php echo $seoQtdReviews; ?>"
        }, <?php } ?>
        "offers": {
            "@type": "Offer",
            "itemCondition": "http://schema.org/NewCondition",
            "availability": "http://schema.org/InStock",
            "seller": {
                "@type": "Organization",
                "name": "LIV Style"
            }
        }
    }
</script>

  <div class="container">
	<ul class="breadcrumb">
	  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	  <?php } ?>
	</ul>
	<div class="row"><?php echo $column_left; ?>
	  <?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-lg-6 col-sm-6'; ?>
	  <?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-lg-9 col-sm-9'; ?>
	  <?php } else { ?>
		<?php $class = 'col-lg-12 col-sm-12'; ?>
	  <?php } ?>
	  <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
		<div class="row">
		  <?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-lg-5 col-sm-5'; ?>
		  <?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-lg-5 col-sm-5'; ?>
		  <?php } else { ?>
			<?php $class = 'col-lg-5 col-sm-5'; ?>
		  <?php } ?>
		  <div class="<?php echo $class; ?>">
			<?php if ($thumb || $images) { ?>
			  <ul class="thumbnails">
				<?php if ($thumb) { ?>
				  <div class="thumbnails__big-image" style="max-width: <?php echo $config->get('config_image_thumb_width'); ?>px; max-height: <?php echo $config->get('config_image_thumb_height'); ?>px;">
					<a href="<?php echo $popup; ?>">
					  <img id="main-image" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>"/>
					</a>
				  </div>
				<?php } ?>
				<?php if ($images) { ?>
				  <div class="thumbnails__list-image owl-carousel" id="image-additional">
					<?php foreach ($images as $image) { ?>
					  <div>
						<a title="<?php echo $heading_title; ?>" class="product-image-link" href="<?php echo $image['popup']; ?>" data-image="<?php echo $image['popup']; ?>" data-zoom-image="<?php echo $image['popup']; ?>">
						  <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
						</a>
					  </div>
					<?php } ?>
				  </div>
				<?php } ?>
			  </ul>
			<?php } ?>
		  </div>
		  <?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-lg-7 col-sm-7'; ?>
		  <?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-lg-7 col-sm-7'; ?>
		  <?php } else { ?>
			<?php $class = 'col-lg-7 col-sm-7'; ?>
		  <?php } ?>
		  <div class="<?php echo $class; ?> product-info">
			<h1><?php echo $heading_title; ?></h1>
			<p class="model"><?php echo $text_model; ?> <?php echo $model; ?></p>
			<!-- <?php if ($review_status) { ?>
			  <div class="product-rating">
				<p>
				  <?php for ($i = 1; $i <= 5; $i++) { ?>
					<?php if ($rating < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					<?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					<?php } ?>
				  <?php } ?>
				  <a><?php echo $reviews; ?></a>
				</p>
			  </div>
			<?php } ?> -->
			<?php if ($special) { ?>
			  <div class="product-sale">-<?php echo $kuler->calculateSalePercent($special, $price); ?>%</div>
			<?php } ?>
			<?php if ($price) { ?>
			  <ul class="list-unstyled product-price">
				<?php if (!$special) { ?>
				  <li>
					<span><?php echo $price; ?></span>
				  </li>
				<?php } else { ?>
				  <li>À vista <span class="product-price--old"><?php echo $price; ?></span> <span><?php echo $special; ?></span> ou <span class="installs">3x R$<?php echo str_replace(".", ",", (round(floatval(str_replace("R$", "", $special))/3, 2))); ?> sem juros.</span></li>
				<?php } ?>
				<?php if ($points) { ?>
				  <li><?php echo $text_points; ?> <?php echo $points; ?></li>
				<?php } ?>
				<?php if ($discounts) { ?>
				  <li>
					<hr class="product-options">
				  </li>
				  <?php foreach ($discounts as $discount) { ?>
					<li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
				  <?php } ?>
				<?php } ?>
			  </ul>
			<?php } ?>
			<!-- <h3><?php echo $kuler->language->get('text_product_details'); ?></h3> -->
			<ul class="list-unstyled product-options">
			  <?php if ($manufacturer) { ?>
				<li>
				  <?php if ($kuler->getSkinOption('show_brand_logo')) { ?>
					<a href="<?php echo $manufacturers; ?>">
					  <img src="<?php echo $kuler->getManufacturerImage($product_id); ?>" alt="<?php echo $manufacturer; ?>" />
					</a>
				  <?php } else { ?>
					<?php echo $text_manufacturer; ?>
					<a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a>
				  <?php } ?>
				</li>
			  <?php } ?>

			  <?php if ($reward) { ?>
				<li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
				<?php } ?>
				<?php if ($stock !== "Em estoque") { ?>
				<li class="in-stock"><?php echo $stock; ?></li>
				<?php } ?>
			</ul>
			<div id="product">

			<?php if((strtolower($stock) != "esgotado") && (strtolower($stock) != "em breve") && (strtolower($stock) != "pré-venda")) { ?>
			  <?php if ($options) { ?>
				<hr class="product-options">
				<!-- <h3><?php echo $text_option; ?></h3> -->
				<?php foreach ($options as $option) { ?>
				  <?php if ($option['type'] == 'select') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
					  <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control select-primary">
						<option value=""><?php echo $text_select; ?></option>
						<?php foreach ($option['product_option_value'] as $option_value) { ?>
						  <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?>
						  </option>
						<?php } ?>
					  </select>
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'radio') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label"><?php echo $option['name']; ?>:</label>
					  <div id="input-option<?php echo $option['product_option_id']; ?>">
						<?php foreach ($option['product_option_value'] as $option_value) { ?>
						  <div class="radio radio-text">
							<label>
							  <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
							  <span><?php echo $option_value['name']; ?></span>
							  <?php if ($option_value['price']) { ?>
								(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							  <?php } ?>
							</label>
							</div>
						<?php } ?>
						<?php if ($option['name'] === "Tamanho") { ?>
							<a class="product-size-info product-size-popup" href="/termos&amp;information_id=7"> <img src="/image/catalog/config/icon-size-info-livstyle.svg"> Descubra seu tamanho</a>
						<?php } ?>
					  </div>
						<hr class="product-options">
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'checkbox') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label"><?php echo $option['name']; ?></label>
					  <div id="input-option<?php echo $option['product_option_id']; ?>">
						<?php foreach ($option['product_option_value'] as $option_value) { ?>
						  <div class="checkbox">
							<label>
							  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
							  <?php echo $option_value['name']; ?>
							  <?php if ($option_value['price']) { ?>
								(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							  <?php } ?>
							</label>
						  </div>
						<?php } ?>
					  </div>
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'image') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label"><?php echo $option['name']; ?>:</label>
					  <div id="input-option<?php echo $option['product_option_id']; ?>">
						<?php foreach ($option['product_option_value'] as $option_value) { ?>
						  <div class="radio radio-image">
							<label>
							  <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
							  <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
							  <?php if ($option_value['price']) { ?>
								(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							  <?php } ?>
							</label>
							</div>
						<?php } ?>
					  </div>
						<hr class="product-options">
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'text') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
					  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'textarea') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
					  <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'file') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label"><?php echo $option['name']; ?></label>
					  <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
					  <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'date') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
					  <div class="input-group date">
						<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
				<span class="input-group-btn">
				<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'datetime') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
					  <div class="input-group datetime">
						<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
				<span class="input-group-btn">
				<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
				</span></div>
					</div>
				  <?php } ?>
				  <?php if ($option['type'] == 'time') { ?>
					<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
					  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
					  <div class="input-group time">
						<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
				<span class="input-group-btn">
				<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
				</span></div>
					</div>
				  <?php } ?>
				<?php } ?>
			  <?php } ?>
			  <?php } ?>
			  <?php if ($recurrings) { ?>
				<hr class="product-options">
				<h3><?php echo $text_payment_recurring ?></h3>
				<div class="form-group required">
				  <select name="recurring_id" class="form-control">
					<option value=""><?php echo $text_select; ?></option>
					<?php foreach ($recurrings as $recurring) { ?>
					  <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
					<?php } ?>
				  </select>
				  <div class="help-block" id="recurring-description"></div>
				</div>
			  <?php } ?>
			  <?php if ($kuler->getSkinOption('show_custom_block')) { ?>
				<div class="custom-block"><?php echo $kuler->translate($kuler->getSkinOption('custom_block_content')); ?></div>
			  <?php } ?>

			<?php if((strtolower($stock) != "esgotado") && (strtolower($stock) != "em breve") && (strtolower($stock) != "pré-venda")) { ?>
				<?php if (!$kuler->getSkinOption('show_number_quantity')) { ?>
				<div class="form-group required">
					<!-- <label class="control-label" for="input-option-quantity">Quantidade</label>
					<select name="quantity" id="input-option-quantity" class="form-control select-primary">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select> -->
					<input type="hidden" name="quantity" size="2" value="1" />
					<input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
				</div>
				<?php } ?>
			  <div class="product-detail button-group">
				<div class="product-detail__group-buttons">
				  <button id="button-cart" class="product-detail-button product-detail-button--cart product-add-to-cart" type="button" data-loading-text="<?php echo $text_loading; ?>">
					<span>COMPRAR</span>
				  </button>
				  <!-- <button class="product-detail-button product-detail-button--compare" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');">
					<i class="pe-7s-photo-gallery"></i>
				  </button> -->
				</div>
				<button class="product-detail-button wishlist-product" type="button" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');">
					<i class="pe-7s-like"></i> <span>Adicionar à lista de desejos</span>
				  </button>
			  </div>
			<?php } else { ?>
					<p>Este produto estará disponível novamente em breve. <br><u><a href="/contato" target="_blank">Clique aqui para entrar na lista de espera da pré-venda.</a></u></p>
				<?php } ?>

			  <?php if ($minimum > 1) { ?>
				<div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
			  <?php } ?>
			</div>
		  </div>
		  <div class="col-lg-12">
			<div class="product-tabs">
			  <ul class="nav nav-tabs">
				<li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
				<?php if ($attribute_groups) { ?>
				  <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
				<?php } ?>
				<!-- <?php if ($review_status) { ?>
				  <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
				<?php } ?> -->
				<?php if ($kuler->getSkinOption('show_custom_tab_1')) { ?>
				  <li><a data-toggle="tab" href="#tab-custom-tab-1"><?php echo $kuler->translate($kuler->getSkinOption('custom_tab_1_title')); ?></a></li>
				<?php } ?>
				<?php if ($kuler->getSkinOption('show_custom_tab_2')) { ?>
				  <li><a data-toggle="tab" href="#tab-custom-tab-2"><?php echo $kuler->translate($kuler->getSkinOption('custom_tab_2_title')); ?></a></li>
				<?php } ?>
			  </ul>
			  <div class="tab-content">
				<div class="tab-pane active" id="tab-description">
					<div class="description-product">
						<?php echo $description; ?>
					</div>
					<?php if ($tags) { ?>
						<p class="tag"><span class="tag__title"><?php echo $text_tags; ?></span>
						  <?php for ($i = 0; $i < count($tags); $i++) { ?>
							<?php if ($i < (count($tags) - 1)) { ?>
							  <a class="tag__name" href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
							<?php } else { ?>
							  <a class="tag__name" href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
							<?php } ?>
						  <?php } ?>
						</p>
					<?php } ?>
				</div>
				<?php if ($attribute_groups) { ?>
				  <div class="tab-pane" id="tab-specification">
					  <?php foreach ($attribute_groups as $attribute_group) { ?>

					<table class="table table-bordered">
						<thead>
						<tr>
						  <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
						  <tr>
							<td><?php echo $attribute['name']; ?></td>
							<td><?php echo $attribute['text']; ?></td>
						  </tr>
						<?php } ?>
						</tbody>

					</table>
					  <?php } ?>
				  </div>
				<?php } ?>
				<?php if ($review_status) { ?>
				  <div class="tab-pane" id="tab-review">
					<form class="form-horizontal">
					  <div id="review"></div>
					  <h2><?php echo $text_write; ?></h2>
					  <div class="form-group required">
						<div class="col-lg-4 col-sm-8">
						  <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
						  <input type="text" name="name" value="" id="input-name" class="form-control" />
						</div>
					  </div>
					  <div class="form-group required">
						<div class="col-lg-12 col-sm-12">
						  <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
						  <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
						  <div class="help-block"><?php echo $text_note; ?></div>
						</div>
					  </div>
					  <div class="form-group required">
						<div class="col-lg-12 col-sm-12">
						  <label class="control-label"><?php echo $entry_rating; ?></label>
						  <p><?php echo $entry_bad; ?>&nbsp;
						  <input type="radio" name="rating" value="1" />
						  &nbsp;
						  <input type="radio" name="rating" value="2" />
						  &nbsp;
						  <input type="radio" name="rating" value="3" />
						  &nbsp;
						  <input type="radio" name="rating" value="4" />
						  &nbsp;
						  <input type="radio" name="rating" value="5" />
						  &nbsp;<?php echo $entry_good; ?></p></div>
					  </div>
					  <div class="form-group required">
						<div class="col-lg-4 col-sm-8">
						  <label class="control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>
						  <input type="text" name="captcha" value="" id="input-captcha" class="form-control" />
						</div>
					  </div>
					  <div class="form-group">
						<div class="col-lg-12 col-sm-12">
							<img src="/captcha" alt="" id="captcha" />
						</div>
					  </div>
					  <div class="buttons">
						<button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_comment; ?></button>
					  </div>
					</form>
				  </div>
				<?php } ?>
				<?php if ($kuler->getSkinOption('show_custom_tab_1')) { ?>
				  <div id="tab-custom-tab-1" class="tab-pane">
					<?php echo $kuler->translate($kuler->getSkinOption('custom_tab_1_content')); ?>
				  </div>
				<?php } ?>
				<?php if ($kuler->getSkinOption('show_custom_tab_2')) { ?>
				  <div id="tab-custom-tab-2" class="tab-pane">
					<?php echo $kuler->translate($kuler->getSkinOption('custom_tab_2_content')); ?>
				  </div>
				<?php } ?>
			  </div>
			</div>
		  </div>
		</div>
		<?php if ($products && $kuler->getSkinOption('show_related_products')) { ?>
		  <div class="product-related">
			<div class="box-heading">
			  <span><?php echo $kuler->language->get('text_related_products'); ?></span>
			</div>
			<div id="product-related" class="row owl-carousel-related-product" style="margin-left: 0px;">
			  <?php foreach ($products as $product) { ?>
				<div class="product-layout product-grid col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php if (isset($product['date_end']) && $product['date_end']) echo ' has-deal'; ?>">
				  <?php if ($product['thumb']) { ?>
					<div class="product-thumb">
					  <div class="product-thumb__primary">
						<a href="<?php echo $product['href']; ?>">
						  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
						</a>
					  </div>
					  <?php if ($images = $kuler->getProductImages($product['product_id'])) { ?>
						<?php if(!$kuler->mobile->isMobile() && $kuler->getSkinOption('enable_swap_image')){ ?>
						  <?php $size = $kuler->getImageSizeByPath($product['thumb']); ?>
						  <div class="product-thumb__secondary hidden-xs hidden-sm hidden-md">
							<a href="<?php echo $product['href']; ?>">
							  <img src="<?php echo $kuler->resizeImage($images[0], $size['width'], $size['height']); ?>" alt="<?php echo $product['name']; ?>"/>
							</a>
						  </div>
						<?php } ?>
					  <?php } //end swap image ?>
					  <?php if (Kuler::getInstance()->getSkinOption('show_quick_view')) { ?>
						<button class="product-detail-button product-detail-button--quick-view">
						  <a href="<?php echo Kuler::getInstance()->getQuickViewUrl($product); ?>" data-toggle="tooltip" title="<?php echo $kuler->translate($kuler->getSkinOption('view_button_text')) ?>">
							<?php echo ($kuler->translate($kuler->getSkinOption('view_button_text'))) ? $kuler->translate($kuler->getSkinOption('view_button_text')) : '<i class="pe-7s-search"></i>';?>
						  </a>
						</button>
					  <?php } ?>
					  <?php if ($product['special']) { ?>
						<!-- <div class="product-sale">
						  <span>-<?php echo $kuler->calculateSalePercent($product['special'], $product['price']); ?>%</span>
						</div> -->
					  <?php } //end special ?>
					  <?php if(isset($setting['deal_date']) && $setting['deal_date']) { ?>
						<?php if(isset($product['date_end'])) { ?>
						  <?php
						  $parts = array('0000', '00', '00');

						  if ($product['date_end']) {
							$parts = explode('-', $product['date_end']);
						  }
						  ?>
						  <div class="product-deal-countdown" data-is-deal="<?php echo $product['date_end'] ? 'true' : 'false' ?>" data-product-id="<?php echo $product['product_id'] ?>" data-date-end="<?php echo $product['date_end'] ?>" data-year="<?php echo $parts[0] ?>" data-month="<?php echo $parts[1] ?>" data-day="<?php echo $parts[2] ?>"></div>
						<?php }  ?>
					  <?php } //end deal date ?>
					</div><!--/.produc-thumb-->
				  <?php } else { ?>
					<div class="product-thumb product-thumb--no-image">
					  <a href="<?php echo $product['href']; ?>">
						<img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" />
					  </a>
					</div><!--/.product-thumb--no-image-->
				  <?php } //end if product thumb ?>
				  <h4 class="product-name text-center">
					<a href="<?php echo $product['href']; ?>">
					  <?php echo $product['name']; ?>
					</a>
				  </h4>
				  <p class="product-price text-center">
					<?php if (!$product['special']) { ?>
					  <?php echo $product['price']; ?>
						<p class="installments text-center">3x R$<?php echo str_replace(".", ",", (round(floatval(str_replace("R$", "", $product['price']))/3, 2))); ?></p>
					<?php } else { ?>
					  <span class="product-price--old"><?php echo $product['price']; ?></span>
					  <span class="product-price--new"><?php echo $product['special']; ?></span>
						<p class="installments text-center">3x R$<?php echo str_replace(".", ",", (round(floatval(str_replace("R$", "", $product['special']))/3, 2))); ?></p>
					<?php } ?>
				  </p>
				  <div class="product-description hidden">
					<?php echo $product['description']; ?>
				  </div>
				  <div class="product-detail button-group text-center">
					<div class="product-detail__group-buttons">
					  <button class="product-detail-button product-detail-button--cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
						<span><?php echo $button_cart; ?></span>
					  </button>
					  <!-- <button class="product-detail-button product-detail-button--wishlist" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
						<i class="pe-7s-like"></i>
					  </button>
					  <button class="product-detail-button product-detail-button--compare" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
						<i class="pe-7s-photo-gallery"></i>
					  </button> -->
					</div>
				  </div>
				</div>
			  <?php } ?>
			</div>
		  </div>
		<?php } ?>
		<?php echo $content_bottom; ?></div>
	  <?php echo $column_right; ?></div>
  </div>

	<script>
		$(document).ready(function() {
			$('.owl-carousel-related-product').owlCarousel({
				loop: false,
				margin: 10,
				responsive:{
					0:{
						items:2
					},
					480:{
						items:2
					},
					992:{
						items:3
					},
					1000:{
						items:4
					}
				}
			});

			$('.owl-carousel').owlCarousel({
				loop: false,
				margin: 2,
				responsive:{
					0:{
						items:3
					},
					480:{
						items:5
					},
					768:{
						items:4
					},
					1000:{
						items:5
					}
				}
			});
		});
	</script>
  <script type="text/javascript">
  	$('#button-cart').on('click', function(){
  		$.ajax({
	        url: 'carrinho/adicionar',
	        type: 'post',
	        data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
	        dataType: 'json',
	        beforeSend: function() {
	          $('#button-cart').button('loading');
	        },
	        complete: function() {
	          $('#button-cart').button('reset');
	        },
	        success: function(json) {
	          $('.alert, .text-danger').remove();
	          $('.form-group').removeClass('has-error');

	          if (json['error']) {
	            if (json['error']['option']) {
	              for (i in json['error']['option']) {
	                var element = $('#input-option' + i.replace('_', '-'));

	                if (element.parent().hasClass('input-group')) {
	                  element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
	                } else {
	                  element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
	                }
	              }
	            }

	            if (json['error']['recurring']) {
	              $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
	            }

	            // Highlight any found errors
	            $('.text-danger').parent().addClass('has-error');
	          }

	          if (json['success']) {
	            $('.breadcrumb').after('<div id="f-modal" class="fadeModal"><div class="liv-alert">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
				$('.close, .black').click(function() {
					$('#f-modal').remove();
				});

	            ga('send', 'event', 'Carrinho','Adicionar Produto ao Carrinho','');

	            console.log("ga_sent")
				
	            $('#cart-total').html(json['total']);

	            $('#cart').load('/carrinho/info #cart');
	            $('.cart-product-total-number').load('/carrinho/quantidade');
	          }
	          closeAlert(50000);
	        }
	    });
  	});
  </script>
  <script type="text/javascript"><!--
	$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	  $.ajax({
		url: '/produto/descricao',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
		  $('#recurring-description').html('');
		},
		success: function(json) {
		  $('.alert, .text-danger').remove();

		  if (json['success']) {
			$('#recurring-description').html(json['success']);
		  }
		}
	  });
	});
	//--></script>
  <script type="text/javascript"><!--
	$('.date').datetimepicker({
	  pickTime: false
	});

	$('.datetime').datetimepicker({
	  pickDate: true,
	  pickTime: true
	});

	$('.time').datetimepicker({
	  pickDate: false
	});

	$('button[id^=\'button-upload\']').on('click', function() {
	  var node = this;

	  $('#form-upload').remove();

	  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	  $('#form-upload input[name=\'file\']').trigger('click');

	  $('#form-upload input[name=\'file\']').on('change', function() {
		$.ajax({
		  url: 'index.php?route=tool/upload',
		  type: 'post',
		  dataType: 'json',
		  data: new FormData($(this).parent()[0]),
		  cache: false,
		  contentType: false,
		  processData: false,
		  beforeSend: function() {
			$(node).button('loading');
		  },
		  complete: function() {
			$(node).button('reset');
		  },
		  success: function(json) {
			$('.text-danger').remove();

			if (json['error']) {
			  $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
			}

			if (json['success']) {
			  alert(json['success']);

			  $(node).parent().find('input').attr('value', json['code']);
			}
		  },
		  error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		  }
		});
	  });
	});
	//--></script>
  <script type="text/javascript"><!--
	$('#review').delegate('.pagination a', 'click', function(e) {
	  e.preventDefault();

	  $('#review').fadeOut('slow');

	  $('#review').load(this.href);

	  $('#review').fadeIn('slow');
	});

	$('#review').load('review&product_id=<?php echo $product_id; ?>');

	$('#button-review').on('click', function() {
	  $.ajax({
		url: 'review/adicionar&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
		  $('#button-review').button('loading');
		},
		complete: function() {
		  $('#button-review').button('reset');
		  $('#captcha').attr('src', 'captcha#'+new Date().getTime());
		  $('input[name=\'captcha\']').val('');
		},
		success: function(json) {
		  $('.alert-success, .alert-danger').remove();

		  if (json['error']) {
			$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		  }

		  if (json['success']) {
			$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

			$('input[name=\'name\']').val('');
			$('textarea[name=\'text\']').val('');
			$('input[name=\'rating\']:checked').prop('checked', false);
			$('input[name=\'captcha\']').val('');
		  }
		}
	  });
	});
	//-->

	$('#qty-dec').click(function(){
		if($('input[name=\'quantity\']').val() > 1) {
			var qty = parseInt($('input[name=\'quantity\']').val());
			$('input[name=\'quantity\']').val(qty-1);
		}
	});

	$('#qty-inc').click(function(){
		if($('input[name=\'quantity\']').val() < 99) {
			var qty = parseInt($('input[name=\'quantity\']').val());
			$('input[name=\'quantity\']').val(qty+1);
		}
	});
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6&appId=1038833542860959";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));

	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>

<?php echo $footer; ?>
