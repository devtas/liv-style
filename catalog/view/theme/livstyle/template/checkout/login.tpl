<div class="row">
  <div class="col-sm-6">
    <h2><?php echo $text_new_customer; ?></h2>
    <p><?php echo $text_checkout; ?></p>
    <div class="radio">
      <label>
        <?php if ($account == 'register') { ?>
        <input type="radio" name="account" value="register" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="register" />
        <?php } ?>
        <?php echo $text_register; ?></label>
    </div>
    <?php if ($checkout_guest) { ?>
    <div class="radio">
      <label>
        <?php if ($account == 'guest') { ?>
        <input type="radio" name="account" value="guest" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="guest" />
        <?php } ?>
        <?php echo $text_guest; ?></label>
    </div>
    <?php } ?>
    <p><?php echo $text_register_account; ?></p>
    <input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
  <div class="col-sm-6">
    <h2><?php echo $text_returning_customer; ?></h2>
    <p><?php echo $text_i_am_returning_customer; ?></p>
    <div class="form-group">
      <div class="field">
        <input type="text" name="email" value="" id="input-email" class="fld fld-primary" required=""/>
        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
      </div>
    </div>
    <div class="form-group field">
      <input type="password" name="password" value="" id="input-password" class="form-control fld fld-primary" required="" />
      <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
      <a style="padding-top:5px" href="<?php echo $forgotten; ?>" class="pull-right"><?php echo $text_forgotten; ?></a></div>
    <input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<script>
$('.field > .fld').focus(function() {
		$(this).addClass('fld-active');
	});

	$('.field > .fld').blur(function() {
		if($(this).val().length === 0) {
			$(this).removeClass('fld-active');
		}
	});

	$('.field > .fld').on('invalid', function(event) {
		console.log(event.type);
	});

	$(document).on('change', '.field > .fld', function() {
		if($(this).val().length > 0) {
			$(this).addClass('fld-active');
		}
		else {
			$(this).removeClass('fld-active');
		}
	});
</script>