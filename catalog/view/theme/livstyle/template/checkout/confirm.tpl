<?php if (!isset($redirect)) { ?>
<div class="col-sm-12">

  <table class="table table-bordered table-hover tablesaw tablesaw-stack" data-tablesaw-mode="stack">
  <caption class="text-center"></caption>
  <thead>
    <tr>
      <th class="text-left"><?php echo $column_name; ?></th>
      <th class="text-center">Referência</th>
      <th class="text-center"><?php echo $column_quantity; ?></th>
      <th class="text-center"><?php echo $column_price; ?></th>
      <th class="text-center"><?php echo $column_total; ?></th>
    </tr>
  </thead>
  <tbody>
      <?php foreach ($products as $product) { ?>
        <tr>
          <td class="text-left">
            <b class="tablesaw-cell-label"><?php echo $column_name; ?></b><span class="tablesaw-cell-content">
            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php foreach ($product['option'] as $option) { ?>
              <br>
              <small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?>
            <?php if($product['recurring']) { ?>
              <br>
              <span class="label label-info"><?php echo $text_recurring; ?></span>
              <small><?php echo $product['recurring']; ?></small>
            <?php } ?>
          </td>
          <td class="text-center"><b class="tablesaw-cell-label">Referência</b><span class="tablesaw-cell-content"></span><?php echo $product['model']; ?></td>
          <td class="text-center"><b class="tablesaw-cell-label"><?php echo $column_quantity; ?></b><span class="tablesaw-cell-content"></span><?php echo $product['quantity']; ?></td>
          <td class="text-center"><b class="tablesaw-cell-label"><?php echo $column_price; ?></b><span class="tablesaw-cell-content"></span><?php echo $product['price']; ?></td>
          <td class="text-center"><b class="tablesaw-cell-label"><?php echo $column_total; ?></b><span class="tablesaw-cell-content"></span><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td>1</td>
          <td><?php echo $voucher['amount']; ?></td>
          <td><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
  </tbody>
</table>
  <div class="col-sm-5 col-sm-offset-7 pull-right" style="position:relative;left:14px">
    <table class="table table-resume">
      <tbody>
        <?php foreach ($totals as $total) { ?>
          <tr>
            <td class="text-uppercase text-right">
              <strong style="font-weight:bold;"><?php echo $total['title']; ?></strong>
            </td>
            <td class="text-uppercase text-right product-price--black">
              <?php echo $total['text']; ?>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
  <?php
  $inline_payments = array('bank_transfer', 'cheque');
  if (!Kuler::getInstance()->getSkinOption('enable_one_page_checkout') || (!empty(Kuler::getInstance()->session->data['payment_method']) && !empty(Kuler::getInstance()->session->data['payment_method']['code']) && in_array(Kuler::getInstance()->session->data['payment_method']['code'], $inline_payments))) {  ?>
    <div class="payment"><?php echo $payment; ?></div>
  <?php } ?>
  <?php } else { ?>
  <script type="text/javascript"><!--
  location = '<?php echo $redirect; ?>';
  //--></script>
  <?php } ?>
