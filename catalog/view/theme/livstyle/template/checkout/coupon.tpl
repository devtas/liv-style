<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
  <div class="input-group">
    <input  type="text"
            name="coupon"
            value="<?php echo $coupon; ?>"
            placeholder="<?php echo $entry_coupon; ?>"
            id="input-coupon"
              class="form-control" />

    <span class="input-group-btn">
      <input  type="button"
              value="<?php echo $button_coupon; ?>"
              id="button-coupon"
              data-loading-text="<?php echo $text_loading; ?>"
              class="btn btn-primary input-lg" />
    </span>
  </div>
</div>

<script type="text/javascript">
$('#button-coupon').on('click', function () {
  $.ajax({
    url: '/cupom/cupom'
    , type: 'post'
    , data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val())
    , dataType: 'json'
    , beforeSend: function () {
      $('#button-coupon').button('loading');
    }
    , complete: function () {
      $('#button-coupon').button('reset');
    }
    , success: function (json) {
      $('.alert').remove();
      if (json['error']) {
        $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
      if (json['redirect']) {
        location = json['redirect'];
      }
    }
  });
});
</script>
