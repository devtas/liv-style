<?php
	$kuler->addScript('catalog/view/theme/' . $kuler->getTheme() . '/js/one_page_checkout.js', true);
?>
<script>
	Kuler.one_page_checkout_methods_url = <?php echo json_encode($one_page_checkout_methods_url); ?>;
	Kuler.shipping_required = <?php echo json_encode($shipping_required); ?>;
</script>

<div id="one-page-checkout">
	<?php if (!$is_logged) { ?>
		<a id="login">
			<?php echo _t('text_already_registered_click_here_to_login'); ?>
		</a>
	<?php } ?>

	<div class="checkout-info row">
		<form id="checkout-form" novalidate>
			<input type="hidden" name="address_id" value="<?php echo $address_id; ?>" />
			<div id="order-account" class="customer-info col-md-5">
				<div class="row">
					<div class="col-sm-6">
						<fieldset>
							<legend><?php echo _t('text_your_details'); ?></legend>
							<div class="form-group" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
								<label class="control-label">
									<?php echo _t('entry_customer_group'); ?>
								</label>
								<?php foreach ($customer_groups as $customer_group) { ?>
									<?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
										<div class="radio">
											<label>
												<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
												<?php echo $customer_group['name']; ?>
											</label>
										</div>
									<?php } else { ?>
										<div class="radio">
											<label>
												<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" />
												<?php echo $customer_group['name']; ?>
											</label>
										</div>
									<?php } ?>
								<?php } ?>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-firstname" value="<?php echo $first_name; ?>" name="firstname" required />
								<label><?php echo _t('entry_firstname'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-lastname" value="<?php echo $last_name; ?>" name="lastname" required />
								<label><?php echo _t('entry_lastname'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-fax" value="<?php echo $fax; ?>" name="fax" required />
								<label><?php echo _t('entry_fax'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-email" value="<?php echo $email; ?>" name="email" required />
								<label><?php echo _t('entry_email'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-telephone" value="<?php echo $telephone; ?>" name="telephone" required />
								<label><?php echo _t('entry_telephone'); ?></label>
							</div>

							<?php foreach ($custom_fields as $custom_field) { ?>
								<?php if ($custom_field['location'] == 'account') { ?>
									<?php if ($custom_field['type'] == 'select') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group field custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label" for="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php echo $custom_field['name']; ?>
											</label>
											<select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control select-secondary">
												<option value="">
													<?php echo _t('text_select'); ?>
												</option>
												<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
													<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>">
														<?php echo $custom_field_value['name']; ?>
													</option>
												<?php } ?>
											</select>
										</div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'radio') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label">
												<?php echo $custom_field['name']; ?>
											</label>
											<div id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
													<div class="radio">
														<label>
															<input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
															<?php echo $custom_field_value['name']; ?>
														</label>
													</div>
												<?php } ?>
											</div>
										</div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'checkbox') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label">
												<?php echo $custom_field['name']; ?>
											</label>
											<div id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
													<div class="checkbox">
														<label>
															<input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
															<?php echo $custom_field_value['name']; ?>
														</label>
													</div>
												<?php } ?>
											</div>
										</div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'text') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label" for="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php echo $custom_field['name']; ?>
											</label>
											<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
										</div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'textarea') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label" for="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php echo $custom_field['name']; ?>
											</label>
											<textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
												<?php echo $custom_field['value']; ?>
											</textarea>
										</div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'file') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label">
												<?php echo $custom_field['name']; ?>
											</label>
											<br />
											<button type="button" id="button-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo _t('text_loading'); ?>" class="btn btn-default"><i class="fa fa-upload"></i>
												<?php echo $button_upload; ?>
											</button>
											<input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="" /> </div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'date') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label" for="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php echo $custom_field['name']; ?>
											</label>
											<div class="input-group date">
												<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD" id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
												<span class="input-group-btn">
													<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'time') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label" for="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php echo $custom_field['name']; ?>
											</label>
											<div class="input-group time">
												<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="HH:mm" id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
												<span class="input-group-btn">
													<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</div>
									<?php } ?>

									<?php if ($custom_field['type'] == 'datetime') { ?>
										<div id="payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
											<label class="control-label" for="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>">
												<?php echo $custom_field['name']; ?>
											</label>
											<div class="input-group datetime">
												<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-order-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
												<span class="input-group-btn">
													<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						</fieldset>
					</div>
					<div class="col-sm-6">
						<fieldset id="address">
							<legend><?php echo _t('text_your_address'); ?></legend>
							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-postcode" value="<?php echo $postcode; ?>" name="postcode" required />
								<label><?php echo _t('entry_postcode'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-address-1" value="<?php echo $address_1; ?>" name="address_1" required />
								<label><?php echo _t('entry_address_1'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-company" value="<?php echo $company; ?>" name="company" required />
								<label><?php echo _t('entry_company'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-address-2" value="<?php echo $address_2; ?>" name="address_2" required />
								<label><?php echo _t('entry_address_2'); ?></label>
							</div>

							<div class="field">
								<input type="text" class="fld fld-primary" id="input-order-city" value="<?php echo $city; ?>" name="city" required />
								<label><?php echo _t('entry_city'); ?></label>
							</div>


							<div class="form-group field required">
								<select name="zone_id" id="input-order-zone" class="form-control select-secondary" data-value="<?php echo $zone_id; ?>"></select>
								<label>Estado</label>
							</div>
							<div class="form-group field required">
								<select name="country_id" id="input-order-country" class="form-control country-selector select-secondary" data-post-code-required="#input-order-postcode" data-zone="#input-order-zone">
									<?php foreach ($countries as $country) { ?>
										<?php if ($country['country_id'] == $country_id) { ?>
											<option value="<?php echo $country['country_id']; ?>" selected="selected">
												<?php echo $country['name']; ?>
											</option>
											<?php } else { ?>
												<option value="<?php echo $country['country_id']; ?>">
													<?php echo $country['name']; ?>
												</option>
												<?php } ?>
											<?php } ?>
								</select>
								<label>País</label>
							</div>
						</fieldset>
					</div>

					<div class="col-sm-6">
						<fieldset class="checkbox" style="font-size: 14px;">
							<?php if ($shipping_required) { ?>
								<label>
									<input type="checkbox" name="shipping_address_same" value="1" class="toggle-option" data-no-order-update data-target="#shipping-address-selector" data-reserve="true" checked="checked" />
									<?php echo _t('text_my_delivery_address_and_personal_details_are_the_same'); ?>
								</label>

								<div id="shipping-address-selector" class="field" style="margin-top: 20px;">
									<select name="shipping_address_id" id="input-order-shipping-address-id" class="form-control address-selector select-secondary" data-type="shipping">
										<option value="0">
											<?php echo _t('text_please_select'); ?>
										</option>

										<?php foreach ($addresses as $address) { ?>
											<option value="<?php echo $address['address_id']; ?>">
												<?php echo $address['firstname']; ?>
												<?php echo $address['lastname']; ?>,
												<?php echo $address['address_1']; ?>,
												<?php echo $address['city']; ?>,
												<?php echo $address['zone']; ?>,
												<?php echo $address['country']; ?>
											</option>
										<?php } ?>

										<option value="new">
											<?php echo _t('text_i_want_to_use_a_new_address'); ?>
										</option>
									</select>
									<label>Endereço</label>
								</div>
							<?php } ?>
						</fieldset>


						<fieldset class="checkbox" style="font-size: 14px;">
							<label>
								<input type="checkbox" name="payment_address_same" value="1" class="toggle-option" data-no-order-update data-target="#payment-address-selector" data-reserve="true" checked="checked" />
								<?php echo _t('text_my_payment_address_and_personal_details_are_the_same'); ?>
							</label>
							<div id="payment-address-selector" class="field" style="margin-top: 20px;">
								<select name="payment_address_id" id="input-order-payment-address-id" class="form-control address-selector select-secondary" data-type="payment">
									<option value="0">
										<?php echo _t('text_please_select'); ?>
									</option>
									<?php foreach ($addresses as $address) { ?>
										<option value="<?php echo $address['address_id']; ?>">
											<?php echo $address['firstname']; ?>
											<?php echo $address['lastname']; ?>,
											<?php echo $address['address_1']; ?>,
											<?php echo $address['city']; ?>,
											<?php echo $address['zone']; ?>,
											<?php echo $address['country']; ?>
										</option>
									<?php } ?>
									<option value="new">
										<?php echo _t('text_i_want_to_use_a_new_address'); ?>
									</option>
								</select>
								<label>Endereço</label>
							</div>
						</fieldset>

					<?php if (!$is_logged) { ?>
						<p style="font-size: 14px;">
							<label>
								<input type="checkbox" name="create_new_account" class="toggle-option" data-target="#password-container" value="1" />
								<?php echo _t('text_create_new_account'); ?>
							</label>

							<fieldset id="password-container">
								<div class="form-group required">
									<div class="field col-lg-12">
										<input type="password" class="fld fld-primary" id="input-order-password" name="password" required />
										<label><?php echo _t('entry_password'); ?></label>
									</div>
								</div>

								<div class="form-group required">
									<div class="field col-lg-12">
										<input type="password" class="fld fld-primary" id="input-order-confirm" name="confirm" required />
										<label><?php echo _t('entry_confirm'); ?></label>
									</div>
								</div>

								<div class="checkbox" style="font-size: 14px;">
									<label>
										<input type="checkbox" name="newsletter" value="1" />
										<?php echo _t('text_subscribe_newsletter'); ?>
									</label>
								</div>
								<div class="checkbox" style="font-size: 14px;">
									<label>
										<input type="checkbox" name="register_agree" id="input-order-register-agree" value="1" />
										<?php echo $text_agree_register; ?>
									</label>
								</div>
								<div class="buttons checkout-buttons">
									<div class="pull-right">
										<input type="submit" id="confirm-order" class="button" value="<?php echo _t('button_confirm'); ?>" />
									</div>
								</div>
							</fieldset>
						</p>
					<?php } ?>
				</div>
				</div>
			</div>
			<div class="order-info col-md-7">
				<div class="method row">
					<?php if ($shipping_required) { ?>
						<div class="shipping-method col-md-6">
							<h2 id="input-order-shipping-method"><?php echo _t('text_opcheckout_shipping_method'); ?></h2>
							<div id="shipping-method-content"></div>
						</div>
					<?php } ?>
					<div class="payment-method col-md-6">
						<h2><?php echo _t('text_opcheckout_payment_method'); ?></h2>
						<div id="payment-method-content"></div>
					</div>
				</div>
				<div class="order-total">
					<h2 id="input-order-payment-method"><?php echo _t('text_opcheckout_confirm'); ?></h2>
					<div id="order-total-content" class="table-responsive"></div>
				</div>
				<p>
					<label>
						<input type="checkbox" value="1" class="toggle-option" data-target="#coupon-container" />
						<?php echo _t('text_use_coupon'); ?>
					</label>
					<div id="coupon-container">
						<div class="col-lg-6 col-md-6 col-sm-10 col-xs-8">
							<input type="text" id="coupon" class="form-control" placeholder="<?php echo _t('entry_coupon'); ?>" value="<?php echo $coupon; ?>" />
						</div>
						<input type="button" id="apply-coupon" class="button" value="<?php echo _t('button_coupon'); ?>" />
					</div>
				</p>
				<p>
					<label id="input-order-order-agree">
						<input type="checkbox" name="order_agree" value="1" />
						Eu li e concordo com o contrato de <a href="/termos&information_id=5" class="agree"><b>Termos e Condições</b></a>
					</label>
				</p>
				<div class="buttons checkout-buttons">
					<div class="pull-right">
						<input type="submit" id="confirm-order" class="button" value="<?php echo _t('button_confirm'); ?>" />
					</div>
					<div class="pull-right" data-toggle="tooltip" title="loja segura com criptografia (SSL) blindada contra roubo de informações e clonagem de cartão.">
						<p><i class="fa fa-lock" aria-hidden="true"></i> Loja 100% Segura</p>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div style="display: none;">
	<div id="address-form-container" class="popup-container mfp-container-register">
		<h2 data-payment-title="<?php echo _t('text_payment_address'); ?>" data-shipping-title="<?php echo _t('text_shipping_title'); ?>"></h2>
		<form id="address-form" novalidate>
			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-firstname" value="" name="firstname" required />
					<label><?php echo _t('entry_firstname'); ?></label>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-lastname" value="" name="lastname" required />
					<label><?php echo _t('entry_lastname'); ?></label>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-fax" value="<?php echo $fax; ?>" name="fax" required />
    				<label><?php echo _t('entry_fax'); ?></label>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-postcode" value="" name="postcode" required />
					<label><?php echo _t('entry_postcode'); ?></label>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-address-1" value="" name="address_1" required />
					<label><?php echo _t('entry_address_1'); ?></label>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-company" value="" name="company" required />
					<label><?php echo _t('entry_company'); ?></label>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-address-2" value="" name="address_2" required />
					<label><?php echo _t('entry_address_2'); ?></label>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="field">
					<input type="text" class="fld fld-primary" id="input-new-city" value="" name="city" required />
					<label><?php echo _t('entry_city'); ?></label>
				</div>
			</div>

			<div class="form-group required">
				<div class="col-sm-12 field">
					<select name="zone_id" id="input-new-zone" class="form-control select-secondary"></select>
					<label>Estado</label>
				</div>
			</div>

			<div class="form-group required">
				<div class="col-sm-12 field">
					<select name="country_id" id="input-new-country" class="form-control country-selector select-secondary" data-post-code-required="#input-new-postcode" data-zone="#input-new-zone">
						<?php foreach ($countries as $country) { ?>
							<option value="<?php echo $country['country_id']; ?>">
								<?php echo $country['name']; ?>
							</option>
						<?php } ?>
					</select>
					<label>País</label>
				</div>
			</div>

			<?php foreach ($custom_fields as $custom_field) { ?>
				<?php if ($custom_field['location'] == 'address') { ?>
					<?php if ($custom_field['type'] == 'select') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label" for="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12 field">
								<select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control select-secondary">
									<option value="">
										<?php echo _t('text_select'); ?>
									</option>
									<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
										<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>">
											<?php echo $custom_field_value['name']; ?>
										</option>
									<?php } ?>
								</select>
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'radio') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<div id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
									<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
										<div class="radio">
											<label>
												<input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
												<?php echo $custom_field_value['name']; ?>
											</label>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'checkbox') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<div id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
									<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
												<?php echo $custom_field_value['name']; ?>
											</label>
										</div>
										<?php } ?>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'text') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label" for="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'textarea') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label" for="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
									<?php echo $custom_field['value']; ?>
								</textarea>
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'file') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<button type="button" id="button-payment-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo _t('text_loading'); ?>" class="btn btn-default"><i class="fa fa-upload"></i>
									<?php echo $button_upload; ?>
								</button>
								<input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="" />
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'date') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label" for="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<div class="input-group date">
									<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD" id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'time') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label" for="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<div class="input-group time">
									<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="HH:mm" id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if ($custom_field['type'] == 'datetime') { ?>
						<div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="col-sm-12 control-label" for="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>">
								<?php echo $custom_field['name']; ?>
							</label>
							<div class="col-sm-12">
								<div class="input-group datetime">
									<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-new-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			<?php } ?>

			<div class="text-center">
				<button type="submit" class="btn btn-primary">
					<?php echo _t('button_continue'); ?>
				</button>
			</div>
		</form>
	</div>
	<div id="payment-form" class="popup-container"></div>
</div>

<script>
	Kuler.one_page_checkout_login_url = <?php echo json_encode($login_url); ?>;
	Kuler.order_confirm_url = <?php echo json_encode($order_confirm_url); ?>;
</script>

<link href="catalog/view/javascript/rede_rest/css/skeleton/skeleton.css" rel="stylesheet" type="text/css" />

<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'conta/pais&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value="">Estado</option>';

			if (json['zone']) {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

				html += '>' + json['zone'][i]['name'] + '</option>';
			}
			} else {
				html += '<option value="0" selected="selected">Estado</option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#address-form select[name=\'country_id\']').trigger('change');
//--></script>
