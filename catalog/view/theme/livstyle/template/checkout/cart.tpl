<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li>
				<a href="<?php echo $breadcrumb['href']; ?>">
					<?php echo $breadcrumb['text']; ?>
				</a>
			</li>
		<?php } ?>
	</ul>
	<?php if ($attention) { ?>
		<div class="alert alert-info"><i class="fa fa-info-circle"></i>
			<?php echo $attention; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>

	<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i>
			<?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>

	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
			<?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>

	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-lg-6 col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-lg-9 col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-lg-12 col-sm-12'; ?>
		<?php } ?>

		<div id="content" class="<?php echo $class; ?>">
			<?php echo $content_top; ?>

			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<div id="cart-table" class="table-responsive">
					<table class="table table-bordered tablesaw tablesaw-stack" data-tablesaw-mode="stack">
						<thead>
							<tr>
								<td class="table-title text-uppercase text-left col-lg-2 col-md-2 col-sm-2 col-xs-2">
									<?php echo $column_image; ?>
								</td>
								<td class="table-title text-uppercase text-left">
									<?php echo $column_name; ?>
								</td>
								<td class="table-title text-uppercase text-center">
									<?php echo $column_quantity; ?>
								</td>
								<td class="table-title text-uppercase text-center">
									<?php echo $column_price; ?>
								</td>
								<td class="table-title text-uppercase text-center hidden-xs hidden-sm">
									<?php echo $column_total; ?>
								</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($products as $key => $product) { ?>
								<tr>
									<td class="text-left content-center">
										<?php if ($product['thumb']) { ?> <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
											<?php } ?>
									</td>
									<td class="cart-table-uppercase text-left content-center">
										<a class="text-uppercase product-title" href="<?php echo $product['href']; ?>">
											<?php echo $product['name']; ?>
										</a>

										<?php if (!$product['stock']) { ?>
											<span class="text-danger">***</span>
										<?php } ?>

										<?php if ($product['option']) { ?>
											<br /><small class="text-uppercase reference-title"><?php echo  $column_model .": ".$product['model']; ?></small>
											<?php foreach ($product['option'] as $option) { ?>
												<br /> <small class="mt-10"><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                      <?php } ?>
                      <br /> <small class="product-remove mt-10"><a onclick="cart.remove('<?php echo $product['key']; ?>');">Remover produto do carrinho</a></small>
										<?php } ?>

										<?php if ($product['reward']) { ?>
											<br /> <small><?php echo $product['reward']; ?></small>
										<?php } ?>

										<?php if ($product['recurring']) { ?>
											<br /> <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
										<?php } ?>
									</td>
									<td class="text-center align-group-btn content-center">
										<div class="input-group btn-block" style="width: 60%;margin: 0 auto;">
												<span class="input-group-btn">
													<button data-toggle="tooltip" data-ref="quantity-value-<?php echo $key; ?>" onclick="cart.increase(this)" title="+" class="btn btn-primary btn-plus"><i class="fa fa-plus"></i></button>
												</span>
												<input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control quantity-value quantity-value-<?php echo $key; ?>" readonly/>
												<span class="input-group-btn">
													<button data-toggle="tooltip" data-ref="quantity-value-<?php echo $key; ?>" onclick="cart.decrease(this)" title="-" class="btn btn-primary"><i class="fa fa-minus"></i></button>
												</span>
										</div>
									</td>
									<td class="text-center content-center">
										<span class="subtotal">Subtotal</span>
										<b><?php echo $product['price']; ?></b>
									</td>
									<td class="text-center hidden-xs hidden-sm">
										<b><?php echo $product['total']; ?></b>
									</td>
								</tr>
							<?php } ?>

							<?php foreach ($vouchers as $vouchers) { ?>
								<tr>
									<td></td>
									<td class="text-left">
										<?php echo $vouchers['description']; ?>
									</td>
									<td class="text-left"></td>
									<td class="text-left">
										<div class="input-group btn-block" style="max-width: 200px;">
											<input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" /> <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span>
										</div>
									</td>
									<td class="text-right">
										<?php echo $vouchers['amount']; ?>
									</td>
									<td class="text-right">
										<?php echo $vouchers['amount']; ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
			<br />
			<div class="row clearfix">
				<?php if ($coupon || $voucher || $reward) { ?>
					<?php echo $coupon; ?>
					<!-- <?php echo $voucher; ?> -->
					<?php echo $reward; ?>
				<?php } ?>
				<div class="col-sm-4 col-sm-offset-8 pull-right">
					<table class="table table-resume">
						<?php foreach ($totals as $total) { ?>
							<tr>
								<td class="text-uppercase text-right">
									<strong><?php echo $total['title']; ?></strong>
								</td>
								<td class="text-uppercase text-right product-price--black">
									<?php echo $total['text']; ?>
								</td>
							</tr>
						<?php } ?>
					</table>
				</div>
			</div>
			<div class="buttons cart-button-bottom">
				<div class="pull-left">
					<a href="<?php echo $continue; ?>" class="btn btn-default input-lg">
						<?php echo $button_shopping; ?>
					</a>
				</div>
				<div class="pull-right">
					<a href="<?php echo $checkout; ?>" class="btn btn-primary input-lg">
						<?php echo $button_checkout; ?>
					</a>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>
<?php echo $footer; ?>
