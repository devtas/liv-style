<?php
$kuler = Kuler::getInstance();
?>
<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <div class="row"><?php echo $column_left; ?>
	  <?php if ($column_left && $column_right) { ?>
		  <?php $class = 'col-lg-6 col-sm-6'; ?>
	  <?php } elseif ($column_left || $column_right) { ?>
		  <?php $class = 'col-lg-9 col-sm-9'; ?>
	  <?php } else { ?>
		  <?php $class = 'col-lg-12 col-sm-12'; ?>
	  <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <div class="row" style="margin: 50px; text-align: center;">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <h3>WhatsApp</h3>
          <p><strong>Atacado (SP)</strong><br><span>(11) 94585-2940</span></p>
          <p><strong>Atacado (GYN)</strong><br><span>(62) 9633-0525</span></p>
          <p><strong>Varejo</strong><br><span>(11) 99010-9555</span></p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <h3>E-mail</h3>
          <p><span>contato@livstyle.com.br</span></p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <h3>Showroom para Lojistas</h3>
          <p><strong>São Paulo/SP - Bom Retiro</strong><br><span>Rua Correia de Melo, 84, Sala 202</span></p>
          <p><strong>Goiânia/GO</strong><br><span>Av. 136, 761, Sala 182A</span></p>
        </div>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
            <div class="col-sm-10">
              <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
              <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>
            <div class="col-sm-10">
              <input type="text" name="captcha" id="input-captcha" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-10 pull-right">
              <img src="index.php?route=tool/captcha" alt="" />
              <?php if ($error_captcha) { ?>
                <div class="text-danger"><?php echo $error_captcha; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons">
          <div class="pull-right">
            <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
