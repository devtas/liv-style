

<?php echo $header; ?>

<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        <div class="col-md-12">
          <img src="/image/catalog/config/mail/mail-success.png" class="pull-left title-icon" />
          <h1><?php echo $heading_title; ?></h1>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <?php echo $text_message; ?>

          <?php if ($plp) { ?>
            <!-- <br><br><h3>Entrega</h3> -->
            <p>Para rastrear a sua entrega, utilize o código: <?php echo $plp; ?></p>
          <?php } ?>
        </div>
        <div class="col-md-2"></div>

        <?php if (isset($items)): ?>
          <div class="col-md-4">
            <div class="order_items">
              <h3><?php echo $items_title; ?></h3>
              <div class="items">
                <?php foreach ($items as $key => $item): ?>
                  <div class="item">
                    <div class="image">
                      <img src="/image/<?php echo $item['image']; ?>" height="45" alt="<?php echo $item['name']; ?>">
                    </div>
                    <div class="text">
                      <p class="pull-right"><b>Total: <?php echo $item['total_format']; ?></b></p>
                      x<?php echo $item['quantity']; ?> <b><?php echo $item['name']; ?></b><br />
                      <p><?php echo $item['price_format']; ?></p>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
              <BR />
              <BR />
              <div class="item">
                <p class="pull-right"><?php echo $order_totals['value_format']; ?></p>
                SUBTOTAL
              </div>
            </div>
          </div>
        <?php endif; ?>

      </div>      

      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
    fbq('track', 'Purchase', {
      content_type: 'product',
      currency: "BRL",
      value: <?php echo $order_totals['value']; ?>
    });
</script>