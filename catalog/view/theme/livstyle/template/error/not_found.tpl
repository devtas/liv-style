<?php echo $header; ?>
<div class="container-fluid container404">
  <div class="container">
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>

    <div class="row"><?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="number404">404</div>
        <h1><?php echo $heading_title; ?></h1>
        <!-- <p><?php echo $text_error; ?></p> -->
        <div class="buttons">
          <a href="/lancamento" class="btn btn-primary">Veja os lançamentos</a>
        </div>
        <?php echo $content_bottom; ?></div>
      <?php echo $column_right; ?>
    </div>
  </div>
</div>

<style type="text/css">
  .container404 {
    background-color: #B8DEE9;
    background-size: cover;
    min-height: 600px;
  }
  .container404 h1 {
    font-size: 40px;
    font-weight: 300;
    margin: 0 50px 50px 0;
    text-transform: uppercase;
    color: #fff;
  }
  .container404 .number404 {
    font-size: 200px;
    font-weight: 300;
    margin: 100px 0 0;
    color: #ffffff;
    letter-spacing: -.7rem;
    line-height: 1;
  }

  @media screen and (max-width: 768px) {
    .container404 .number404 {
      font-size: 150px;
      margin-top: 50px;
      text-align: center;
    }
    .container404 h1 {
      margin-right: 0;
      text-align: center;
    }
    .container404 .buttons {
      text-align: center;
    }
  }
</style>

<?php echo $footer; ?>