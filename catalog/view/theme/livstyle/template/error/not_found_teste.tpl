<style>
  .payment-rede_rest_credito-comprovante #cupom.panel-info {
    border: none;
  }
  .payment-rede_rest_credito-comprovante #cupom.panel {
    border: none;
    box-shadow: none;
    -webkit-box-shadow: none;
  }

  .payment-rede_rest_credito-comprovante #cupom.panel-info>.panel-heading {
    color: #000;
    text-transform: uppercase;
    background-color: inherit;
    border: none;
  }
</style>

<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="row">
    <div id="content" class="col-sm-12">
      <div class="row">
        <div class="col-sm-4">
          <div id="cupom" class="panel panel-info">
            <div class="panel-heading">
              Comprovante
              </div>
            <div class="panel-body">
               Compra realizada com Cartão de Crédito.<br />
               Thiago de Andrade da Silva<br />
               418.272.248-5<br />
               4042 8762 0908 4201<br />

              <p>Para rastrear a sua entrega, utilize o código: LV020230192BR</p>
            </div>
          </div>
          <div class="buttons" style="margin-left: 14px;">
            <a href="javascript:window.print()" target="_blank" class="btn btn-primary">Imprimir</a>
            <!--a href="/comprovante/imprimir" target="_blank" class="btn btn-primary">Imprimir</a-->
          </div>
        </div>
        <div class="col-sm-8">
          <div id="cupom" class="panel panel-info">
            <div class="panel-body">
              <img src="/image/catalog/config/mail/mail-success.png" class="pull-left title-icon" /> <h2>COMPRA REALIZADA COM SUCESSO!</h2>
              <br/>
              Seu pagamento foi aprovado e recebemos o seu pedido com sucesso!
              <br/>
              <br/>
              Agora nossa equipe irá separar o seu produto com muito carinho, para que ele chegue até você o mais rápido possível.
              <br/>
              <br/>
              Temos um enorme prazer de servir você, e desejamos que nossa marca possa fazer parte de momentos incríveis em sua vida.
              <br/>
              <br/>
              Agora enviaremos um email com todos os detalhes de sua compra. Em caso de qualquer dúvida, basta enviar um email para contato@livstyle.com.br ou pelo Whatsapp:(11) 99010-9555 que nossa equipe está pronta para lhe atender.
              <br/>
              <br/>
              <strong>Com amor,</strong>
              <br/>
              <strong>LIVSTYLE</strong>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8"></div>
        <div class="col-sm-4">
            <div class="order_items">
              <h3>Produtos</h3>
              <div class="items">
                  <div class="item">
                    <div class="image">
                      <img src="https://www.livstyle.com.br/image/cache/catalog/products/1.%20JANEIRO%202019/4.%20CALCA%20ALEXIS/CAL%C3%87A%20ALEXIS%20PRETA%202-308x460.jpg" height="45" alt="Product">
                    </div>
                    <div class="text">
                      <p class="pull-right"><b>Total: R$ 99,98</b></p>
                      x2 <b>Produto Teste</b><br />
                      <p>R$ 49,99</p>
                    </div>
                  </div>
                  <div class="item">
                    <div class="image">
                      <img src="https://www.livstyle.com.br/image/cache/catalog/products/1.%20JANEIRO%202019/4.%20CALCA%20ALEXIS/CAL%C3%87A%20ALEXIS%20PRETA%202-308x460.jpg" height="45" alt="Product">
                    </div>
                    <div class="text">
                      <p class="pull-right"><b>Total: R$ 99,98</b></p>
                      x2 <b>Produto Teste</b><br />
                      <p>R$ 49,99</p>
                    </div>
                  </div>
              </div>
              <BR />
              <BR />
              <div class="item">
                <p class="pull-right">R$ 99,98</p>
                SUBTOTAL
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>



</div>
<?php echo $footer; ?>