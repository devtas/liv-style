<?php
	$imgUrl = "https://www.livstyle.com.br/image/catalog/config/mail/"; // MUDAR

	$imgAnalysis = $imgUrl."mail-analysis.png";
	$imgSuccess = $imgUrl."mail-success.png";
	$imgError = $imgUrl."mail-error.png";
	$imgOrder = $imgUrl."mail-order.png";
	$imgProfile = $imgUrl."mail-profile.png";
	$imgProducts = $imgUrl."mail-cart.png";
	$imgPayment = $imgUrl."mail-payment.png";
	$imgShipping = $imgUrl."mail-shipping.png";
	$imgMoney = $imgUrl."mail-money.png";
	$logoVertical = $imgUrl."mail-primary-logo.png";
	$logoHorizontal = $imgUrl."mail-secondary-logo.png";
	$imgLineVertical = $imgUrl."mail-line-vertical.gif";
	$imgLineHorizontal = $imgUrl."mail-line-horizontal.gif";
	$imgPixel = $imgUrl."mail-pixel.png";
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
</head>

<body style="font-family: Arial, sans-serif; background-color: #fff;">
	<table border="0" align="center" cellpadding="0" cellspacing="10">
		<tbody>
			<tr>
				<td style="border:1px solid #ededed">
					<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
						<tbody>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<table width="550" border="0" align="center" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td>
													<table width="550" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td>
																	<span>
																		<img src="<?php echo $logoHorizontal;?>">
																	</span>
																</td>
																<td width="220" align="right">
																	<font size="2" style="font-size:9px"><?php echo $date_added; ?></font>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr height="1">
												<td bgcolor="#dddddd">
													<img src="<?php echo $imgPixel; ?>" width="1" height="1" style="display:block">
												</td>
											</tr>
											<tr>
												<td align="center">&nbsp;</td>
											</tr>
											<tr>
												<td align="center">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td width="1">
																	<img src="<?php echo $imgLineVertical; ?>" width="1" height="36" style="width:1px;min-height:36px">
																</td>
																<td>
																	<img src="<?php echo $imgLineHorizontal; ?>">
																</td>
																<td width="36">
																	<?php if ($order_status == 'Pedido cancelado' || $order_status == 'Pagamento negado' || $order_status == 'Pagamento cancelado'): ?>
																		<img src="<?php echo $imgError; ?>" width="36" height="36">
																	<?php else: ?>
																		<img src="<?php echo $imgSuccess; ?>" width="36" height="36">
																	<?php endif; ?>
																</td>
																<td>
																	<img src="<?php echo $imgLineHorizontal; ?>">
																</td>
																<td width="1">
																	<img src="<?php echo $imgLineVertical; ?>" width="1" height="36" style="width:1px;min-height:36px">
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" style="border-left:1px solid #ededed;border-right:1px solid #ededed;border-bottom:1px solid #ededed;border-collapse:collapse">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td>
																	<h2 style="color:#000;font-size:14px;font-weight:bold">
																		<br>Olá ;-)
																	</h2>
																	<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px">
																		<?php echo $text_greeting;?><strong><?php echo $date_added; ?></strong>
																		<?php echo ($order_status == 'Pagamento negado') ? ' e o pagamento foi negado.' : '.' ?>
																	</p>
																	<?php if ($customer_id && (strlen($text_link) > 0)) { ?>
																		<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px">
																			<?php echo $text_link; ?> <a href="<?php echo $link; ?>" target="_blank"><?php echo $link; ?></a>
																		</p>
																	<?php } ?>
																	<hr>
																	<h2 style="margin:15px 0px 15px 0px;color:#000;font-size:14px;font-weight:bold">Confira os dados do pedido:</h2>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
											</tr>

											<tr>
												<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td>
																	<table width="200" border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td valign="top" width="35">
																					<img src="<?php echo $imgProfile; ?>" style="display:block">
																				</td>
																				<td valign="top">
																					<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
																						<strong>Informações:</strong><br>
																						<?php echo $email; ?><br>
																						<?php echo $telephone; ?><br>
																					</font>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>

											<tr>
												<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td width="165">
																	<table width="165" border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td valign="top" width="35">
																					<img src="<?php echo $imgOrder; ?>" style="display:block">
																				</td>
																				<td valign="top">
																					<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
																						<strong>Número do pedido:</strong><br><?php echo $order_id; ?>
																					</font>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
																<td width="165">
																	<table width="165" border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td valign="top" width="35">
																					<img src="<?php echo $imgOrder; ?>" style="display:block">
																				</td>
																				<td valign="top">
																					<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
																						<strong>Status do pedido:</strong><br><?php echo $order_status; ?>
																					</font>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
											</tr>
											<tr>
												<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td width="165" valign="top">
																	<table width="165" border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td valign="top" width="35">
																					<span>
																						<img src="<?php echo $imgPayment; ?>" style="display:block">
																					</span>
																				</td>
																				<td valign="top">
																					<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
																						<strong>Pagamento:</strong><br><?php echo $payment_method; ?>
																					</font>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
																<?php if ($shipping_method) { ?>
																	<td width="170">
																		<table width="170" border="0" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td valign="top" width="35">
																						<span>
																							<img src="<?php echo $imgShipping; ?>" style="display:block">
																						</span>
																					</td>
																					<td valign="top">
																						<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758"><strong>Entrega:</strong><br>
																							<?php
																							if(strtolower($shipping_method) === "frete convencional") {
																								echo "Convencional";
																							}
																							else if(strtolower($shipping_method) === "frete expresso") {
																								echo "Expresso";
																							}
																							else
																							{
																								echo $shipping_method;
																							}
																							?>
																						</font>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																<?php } ?>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
											</tr>
											<tr>
												<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td valign="top" width="35">
																	<span>
																		<img src="<?php echo $imgProducts; ?>" style="display:block">
																	</span>
																</td>
																<td valign="top">
																	<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758"><strong>Itens do pedido:</strong></font><br>
																<table>
																	<tbody>
																		<?php foreach ($products as $product) { ?>
																		<tr>
																			<td>
																				<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
																					<?php echo $product['quantity']."x"; ?>
																				</font>
																			</td>
																			<td>
																				<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
																					<?php
																					echo $product['name'];

																					if(!empty($product['option'])) {
																						foreach ($product['option'] as $option) {
																							echo " - ".$option['name'].": ".$option['value'];
																						}
																						echo " -";
																					}
																					echo " (".$product['model'].") - ".$product['total'];
																					?>
																				</font>
																			</td>
																		</tr>
																		<?php } ?>
																	</tbody>
																</table>
													</font>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<?php foreach ($vouchers as $voucher) { ?>
							<tr>
								<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
									<table width="500" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td width="200" valign="top">
													<table width="200" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td valign="top" width="35">
																	<span>
																		<img src="<?php echo $imgMoney; ?>" style="display:block">
																	</span>
																</td>
																<td valign="top">
																	<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758"><strong>
																		<?php echo $voucher['description']; ?>:</strong><br><?php echo $voucher['amount']; ?>
																	</font>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<?php } ?>
							<?php foreach ($totals as $total) { ?>
							<tr>
								<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
									<table width="500" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td width="200" valign="top">
													<table width="200" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td valign="top" width="35">
																	<span>
																		<img src="<?php echo $imgMoney; ?>" style="display:block">
																	</span>
																</td>
																<td valign="top">
																	<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758"><strong>
																	<?php
																	if(strtolower($total['title']) === "frete convencional") {
																		echo "Frete";
																	}
																	else
																	{
																		echo $total['title'];
																	}?>:
																	</strong><br><?php echo $total['text']; ?></font>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<?php } ?>
								<tr>
									<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
								</tr>
								<?php if ($shipping_address) { ?>
								<tr>
									<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
										<table width="500" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td valign="top" width="35">
														<span>
															<img src="<?php echo $imgShipping; ?>" style="display:block">
														</span>
													</td>
													<td valign="top">
														<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
															<strong>Endereço:</strong><br><?php echo $shipping_address; ?><br>
														</font>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<?php } ?>
								<?php if ($comment) { ?>
								<tr>
									<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
								</tr>
								<tr>
									<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
										<table width="500" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td valign="top" width="35">
														<span>
															<img src="<?php echo $imgShipping; ?>" style="display:block">
														</span>
													</td>
													<td valign="top">
														<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
															<strong>Observação:</strong><br>
															<?php echo $comment; ?><br>
														</font>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<?php } ?>
								<?php if ($download) { ?>
								<tr>
									<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
								</tr>
								<tr>
									<td align="center" bgcolor="#f1f1f1" style="padding-left:20px">
										<table width="500" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td valign="top" width="35">
														<span>
															<img src="<?php echo $imgShipping; ?>" style="display:block">
														</span>
													</td>
													<td valign="top">
														<font size="2" face="Arial, Helvetica, sans-serif" style="font-size:12px;color:#575758">
															<strong><?php echo $download; ?>:</strong><br><?php echo $download; ?><br>
														</font>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<?php } ?>
								<tr>
									<td align="center" bgcolor="#f1f1f1">&nbsp;</td>
								</tr>
								<tr>
									<td align="center" style="border:1px solid #ededed;border-collapse:collapse">
										<table width="500" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td align="center">&nbsp;</td>
												</tr>
												<tr>
													<td style="padding-right:20px">
														<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px">Se ocorrer algum imprevisto com a sua compra, não se preocupe! Entre em contato com a LIV Style e nós resolveremos o seu problema.</p>

														<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px"> Em caso de dúvidas sobre o pedido, envie um e-mail para <a href="mailto:contato@livstyle.com.br" target="_blank">contato@livstyle.com.br</a></p>
													</td>
												</tr>
												<tr>
													<td align="center">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td align="center">&nbsp;</td>
								</tr>
								<tr>
									<td align="center"> <img src="<?php echo $logoVertical; ?>" height="70"> </td>
								</tr>
								<tr>
									<td align="center">&nbsp;</td>
								</tr>
								<tr style="text-align: center;">
									<td>
										<p style="color: #b9b8b8;font-size: 10px;">Copyright © <?php echo date("Y"); ?> LIV Style - Todos os direitos reservados.</p>
										<p style="color: #b9b8b8;font-size: 10px;">Em caso de dúvidas, envie um e-mail para <a href="mailto:contato@livstyle.com.br" target="_blank"  style="color: #b9b8b8;font-size: 10px;">contato@livstyle.com.br</a>.<br>Confira nossa política de privacidade em <a href="https://www.livstyle.com.br/politica-de-privacidade" target="_blank" style="color: #b9b8b8;font-size: 10px;">www.livstyle.com.br/politica-de-privacidade</a></p>
									</td>
								</tr>
								<tr>
									<td align="center">&nbsp;</td>
								</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	</td>
	</tr>
	</tbody>
	</table>
</body>
</html>
