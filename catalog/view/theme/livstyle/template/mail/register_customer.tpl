<?php
	$imgUrl = "https://www.livstyle.com.br/image/catalog/config/mail/"; // MUDAR

	$imgAnalysis = $imgUrl."mail-analysis.png";
	$imgSuccess = $imgUrl."mail-success.png";
	$imgOrder = $imgUrl."mail-order.png";
	$imgProducts = $imgUrl."mail-cart.png";
	$imgPayment = $imgUrl."mail-payment.png";
	$imgShipping = $imgUrl."mail-shipping.png";
	$imgMoney = $imgUrl."mail-money.png";
	$logoVertical = $imgUrl."mail-primary-logo.png";
	$logoHorizontal = $imgUrl."mail-secondary-logo.png";
	$imgLineVertical = $imgUrl."mail-line-vertical.gif";
	$imgLineHorizontal = $imgUrl."mail-line-horizontal.gif";
	$imgPixel = $imgUrl."mail-pixel.png";
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
</head>

<body style="font-family: Arial, sans-serif; background-color: #fff;">
	<table border="0" align="center" cellpadding="0" cellspacing="10">
		<tbody>
			<tr>
				<td style="border:1px solid #ededed">
					<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
						<tbody>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<table width="550" border="0" align="center" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td>
													<table width="550" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td>
																	<span>
																		<img src="<?php echo $logoHorizontal;?>">
																	</span>
																</td>
																<td width="220" align="right">
																	<font size="2" style="font-size:9px"><?php echo $date_added; ?></font>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr height="1">
												<td bgcolor="#dddddd">
													<img src="<?php echo $imgPixel; ?>" width="1" height="1" style="display:block">
												</td>
											</tr>
											<tr>
												<td align="center">&nbsp;</td>
											</tr>
											<tr>
												<td align="center">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td width="1">
																	<img src="<?php echo $imgLineVertical; ?>" width="1" height="36" style="width:1px;min-height:36px">
																</td>
																<td>
																	<img src="<?php echo $imgLineHorizontal; ?>">
																</td>
																<td width="36">
																	<img src="<?php echo $imgSuccess; ?>" width="36" height="36">
																</td>
																<td>
																	<img src="<?php echo $imgLineHorizontal; ?>">
																</td>
																<td width="1">
																	<img src="<?php echo $imgLineVertical; ?>" width="1" height="36" style="width:1px;min-height:36px">
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" style="border-left:1px solid #ededed;border-right:1px solid #ededed;border-bottom:1px solid #ededed;border-collapse:collapse">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td>
																	<h2 style="color:#000;font-size:14px;font-weight:bold">
																		<br>Olá ;-)
																	</h2>
																	<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px">
																		<?php echo $text_1;?>
																	</p>
																	
																	<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px">
																		<?php echo $text_2; ?>
																	</p>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">&nbsp;</td>
											</tr>
											<tr>
												<td align="center" style="border:1px solid #ededed;border-collapse:collapse">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td align="center">&nbsp;</td>
															</tr>
															<tr>
																<td style="padding-right:20px">
																	<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px">Se ocorrer algum imprevisto com a sua compra, não se preocupe! Entre em contato com a LIV Style e nós resolveremos o seu problema.</p>

																	<p style="color:#575758;font-family:Arial,sans-serif;font-size:12px"> Em caso de dúvidas sobre o pedido, envie um e-mail para <a href="mailto:contato@livstyle.com.br" target="_blank">contato@livstyle.com.br</a></p>
																</td>
															</tr>
															<tr>
																<td align="center">&nbsp;</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">&nbsp;</td>
											</tr>
											<tr>
												<td align="center"> <img src="<?php echo $logoVertical; ?>" height="70"> </td>
											</tr>
											<tr>
												<td align="center">&nbsp;</td>
											</tr>
											<tr style="text-align: center;">
												<td>
													<p style="color: #b9b8b8;font-size: 10px;">Copyright © <?php echo date("Y"); ?> LIV Style - Todos os direitos reservados.</p>
													<p style="color: #b9b8b8;font-size: 10px;">Em caso de dúvidas, envie um e-mail para <a href="mailto:contato@livstyle.com.br" target="_blank"  style="color: #b9b8b8;font-size: 10px;">contato@livstyle.com.br</a>.<br>Confira nossa política de privacidade em <a href="https://www.livstyle.com.br/politica-de-privacidade" target="_blank" style="color: #b9b8b8;font-size: 10px;">www.livstyle.com.br/politica-de-privacidade</a></p>
												</td>
											</tr>
											<tr>
												<td align="center">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
