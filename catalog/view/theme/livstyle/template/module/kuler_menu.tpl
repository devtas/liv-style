<?php $kuler = Kuler::getInstance(); ?>

<?php foreach ($items as $item_index => $item) { ?>
	<li>
		<a href="<?php echo $item['href']; ?>"><?php echo $item['title']; ?></a>
	</li>

	<?php if (!empty($item['children'])) { ?>
		<?php foreach ($item['children'] as $item2) { ?>

		<li>
			<a href="<?php echo $item2['href']; ?>"><?php echo $item2['title']; ?></a>
		</li>

        <?php } ?>
	<?php } ?>
<?php } ?>
