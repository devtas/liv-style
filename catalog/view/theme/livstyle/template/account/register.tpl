<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
   <?php if ($column_left && $column_right) { ?>
   <?php $class = 'col-lg-6 col-sm-6'; ?>
   <?php } elseif ($column_left || $column_right) { ?>
   <?php $class = 'col-lg-9 col-sm-9'; ?>
   <?php } else { ?>
   <?php $class = 'col-lg-12 col-sm-12'; ?>
   <?php } ?>
   <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    <h1><?php echo $heading_title; ?></h1>
    <p><?php echo $text_account_already; ?></p>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" novalidate>
      <fieldset id="account">
        <legend><?php echo $text_your_details; ?></legend>
        <div class="form-group required" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
          <label class="col-sm-2 control-label"><?php echo $entry_customer_group; ?></label>
          <div class="col-sm-10">
            <?php foreach ($customer_groups as $customer_group) { ?>
            <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                <?php echo $customer_group['name']; ?></label>
              </div>
              <?php } else { ?>
              <div class="radio">
                <label>
                  <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" />
                  <?php echo $customer_group['name']; ?></label>
                </div>
                <?php } ?>
                <?php } ?>
              </div>
            </div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-firstname" value="<?php echo $firstname; ?>" name="firstname" required />
					<?php if ($error_firstname) { ?>
						<div class="text-danger"><?php echo $error_firstname; ?></div>
					<?php } ?>
					<label><?php echo $entry_firstname; ?></label>
				</div>
            </div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-lastname" value="<?php echo $lastname; ?>" name="lastname" required />
					<?php if ($error_lastname) { ?>
						<div class="text-danger"><?php echo $error_lastname; ?></div>
					<?php } ?>
					<label><?php echo $entry_lastname; ?></label>
				</div>
            </div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="tel" class="fld fld-primary" id="input-fax" value="<?php echo $fax; ?>" name="fax" required />
					<?php if ($error_fax) { ?>
						<div class="text-danger"><?php echo $error_fax; ?></div>
					<?php } ?>
					<label><?php echo $entry_fax; ?></label>
				</div>
            </div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-email" value="<?php echo $email; ?>" name="email" required />
					<?php if ($error_email) { ?>
						<div class="text-danger"><?php echo $error_email; ?></div>
					<?php } ?>
					<label><?php echo $entry_email; ?></label>
				</div>
            </div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="tel" class="fld fld-primary" id="input-telephone" value="<?php echo $telephone; ?>" name="telephone" required />
					<?php if ($error_telephone) { ?>
						<div class="text-danger"><?php echo $error_telephone; ?></div>
					<?php } ?>
					<label><?php echo $entry_telephone; ?></label>
				</div>
            </div>

            <?php foreach ($custom_fields as $custom_field) { ?>
            <?php if ($custom_field['location'] == 'account') { ?>
            <?php if ($custom_field['type'] == 'select') { ?>
            <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
              <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
              <div class="col-sm-10">
                <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
                  <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
                  <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($custom_field['type'] == 'radio') { ?>
            <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
              <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
              <div class="col-sm-10">
                <div>
                  <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                  <div class="radio">
                    <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
                    <label>
                      <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                      <?php echo $custom_field_value['name']; ?></label>
                      <?php } else { ?>
                      <label>
                        <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                        <?php echo $custom_field_value['name']; ?></label>
                        <?php } ?>
                      </div>
                      <?php } ?>
                    </div>
                    <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                    <?php } ?>
                  </div>
                </div>
                <?php } ?>
                <?php if ($custom_field['type'] == 'checkbox') { ?>
                <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                  <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                  <div class="col-sm-10">
                    <div>
                      <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                      <div class="checkbox">
                        <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && in_array($register_custom_field_value['custom_field_value_id'], $register_custom_field[$custom_field['custom_field_id']])) { ?>
                        <label>
                          <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                          <?php echo $custom_field_value['name']; ?></label>
                          <?php } else { ?>
                          <label>
                            <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                            <?php echo $custom_field_value['name']; ?></label>
                            <?php } ?>
                          </div>
                          <?php } ?>
                        </div>
                        <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                        <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($custom_field['type'] == 'text') { ?>
                    <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                      <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                      <div class="col-sm-10">
                        <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                        <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                        <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($custom_field['type'] == 'textarea') { ?>
                    <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                      <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                      <div class="col-sm-10">
                        <textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                        <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                        <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($custom_field['type'] == 'file') { ?>
                    <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                      <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                      <div class="col-sm-10">
                        <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                        <input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : ''); ?>" />
                        <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                        <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($custom_field['type'] == 'date') { ?>
                    <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                      <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                      <div class="col-sm-10">
                        <div class="input-group date">
                          <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                          </span></div>
                          <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                          <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($custom_field['type'] == 'time') { ?>
                      <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                        <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                        <div class="col-sm-10">
                          <div class="input-group time">
                            <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                            <?php } ?>
                          </div>
                        </div>
                        <?php } ?>
                        <?php if ($custom_field['type'] == 'datetime') { ?>
                        <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                          <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                          <div class="col-sm-10">
                            <div class="input-group datetime">
                              <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                              <span class="input-group-btn">
                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                              </span></div>
                              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                              <?php } ?>
                            </div>
                          </div>
                          <?php } ?>
                          <?php } ?>
                          <?php } ?>
                        </fieldset>
                        <fieldset id="address">
                        	<legend><?php echo $text_your_address; ?></legend>
							<div class="form-group required">
								<div class="field col-lg-12">
									<input type="tel" class="fld fld-primary" id="input-postcode" value="<?php echo $postcode; ?>" name="postcode" required />
									<?php if ($error_postcode) { ?>
										<div class="text-danger"><?php echo $error_postcode; ?></div>
									<?php } ?>
									<label><?php echo $entry_postcode; ?></label>
								</div>
							</div>

							<div class="form-group required">
								<div class="field col-lg-12">
									<input type="text" class="fld fld-primary" id="input-address-1" value="<?php echo $address_1; ?>" name="address_1" required />
									<?php if ($error_address_1) { ?>
										<div class="text-danger"><?php echo $error_address_1; ?></div>
									<?php } ?>
									<label><?php echo $entry_address_1; ?></label>
								</div>
							</div>

							<div class="form-group required">
								<div class="field col-lg-12">
									<input type="text" class="fld fld-primary" id="input-company" value="<?php echo $company; ?>" name="company" required />
									<label><?php echo $entry_company; ?></label>
								</div>
							</div>

							<div class="form-group required">
								<div class="field col-lg-12">
									<input type="text" class="fld fld-primary" id="input-address-2" value="<?php echo $address_2; ?>" name="address_2" required />
									<label><?php echo $entry_address_2; ?></label>
								</div>
							</div>

							<div class="form-group required">
								<div class="field col-lg-12">
									<input type="text" class="fld fld-primary" id="input-city" value="<?php echo $city; ?>" name="city" required />
									<?php if ($error_city) { ?>
										<div class="text-danger"><?php echo $error_city; ?></div>
									<?php } ?>
									<label><?php echo $entry_city; ?></label>
								</div>
							</div>

							<div class="form-group required">
								<div class="col-lg-12 field">
									<select name="zone_id" id="input-zone" class="form-control select-secondary" data-value="<?php echo $zone_id; ?>"></select>
                  <label>Estado</label>
									<?php if ($error_zone) { ?>
										<div class="text-danger"><?php echo $error_zone; ?></div>
									<?php } ?>
								</div>
							</div>

							<div class="form-group required">
								<div class="col-lg-12 field">
									<select name="country_id" id="input-country" class="form-control country-selector select-secondary">
										<option value=""><?php echo $text_select; ?></option>
										<?php foreach ($countries as $country) { ?>
											<?php if ($country['country_id'] == $country_id) { ?>
												<option value="<?php echo $country['country_id']; ?>" selected="selected">
													<?php echo $country['name']; ?>
												</option>
												<?php } else { ?>
													<option value="<?php echo $country['country_id']; ?>">
														<?php echo $country['name']; ?>
													</option>
													<?php } ?>
												<?php } ?>
									</select>
                  <label>País</label>
									<?php if ($error_country) { ?>
										<div class="text-danger"><?php echo $error_country; ?></div>
									<?php } ?>
								</div>
							</div>


                          <?php foreach ($custom_fields as $custom_field) { ?>
                          <?php if ($custom_field['location'] == 'address') { ?>
                          <?php if ($custom_field['type'] == 'select') { ?>
                          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-lg-4 col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                            <div class="col-lg-8 col-sm-10">
                              <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
                                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                              </select>
                              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                              <?php } ?>
                            </div>
                          </div>
                          <?php } ?>
                          <?php if ($custom_field['type'] == 'radio') { ?>
                          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                            <label class="col-lg-4 col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                            <div class="col-lg-8 col-sm-10">
                              <div>
                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                <div class="radio">
                                  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
                                  <label>
                                    <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                    <?php echo $custom_field_value['name']; ?></label>
                                    <?php } else { ?>
                                    <label>
                                      <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                      <?php echo $custom_field_value['name']; ?></label>
                                      <?php } ?>
                                    </div>
                                    <?php } ?>
                                  </div>
                                  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                  <?php } ?>
                                </div>
                              </div>
                              <?php } ?>
                              <?php if ($custom_field['type'] == 'checkbox') { ?>
                              <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                <label class="col-lg-4 col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                <div class="col-lg-8 col-sm-10">
                                  <div>
                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                    <div class="checkbox">
                                      <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && in_array($register_custom_field_value['custom_field_value_id'], $register_custom_field[$custom_field['custom_field_id']])) { ?>
                                      <label>
                                        <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                        <?php echo $custom_field_value['name']; ?></label>
                                        <?php } else { ?>
                                        <label>
                                          <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                          <?php echo $custom_field_value['name']; ?></label>
                                          <?php } ?>
                                        </div>
                                        <?php } ?>
                                      </div>
                                      <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                      <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <?php if ($custom_field['type'] == 'text') { ?>
                                  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                    <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                    <div class="col-sm-10">
                                      <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                      <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                      <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <?php if ($custom_field['type'] == 'textarea') { ?>
                                  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                    <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                    <div class="col-sm-10">
                                      <textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                      <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                      <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <?php if ($custom_field['type'] == 'file') { ?>
                                  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                    <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                    <div class="col-sm-10">
                                      <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                      <input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : ''); ?>" />
                                      <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                      <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <?php if ($custom_field['type'] == 'date') { ?>
                                  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                    <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                    <div class="col-sm-10">
                                      <div class="input-group date">
                                        <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                        <span class="input-group-btn">
                                          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                        </span></div>
                                        <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                        <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                        <?php } ?>
                                      </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'time') { ?>
                                    <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                      <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                      <div class="col-sm-10">
                                        <div class="input-group time">
                                          <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                          <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                          </span></div>
                                          <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                          <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                          <?php } ?>
                                        </div>
                                      </div>
                                      <?php } ?>
                                      <?php if ($custom_field['type'] == 'datetime') { ?>
                                      <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                          <div class="input-group datetime">
                                            <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span></div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                          </div>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                        <?php } ?>
                                      </fieldset>
                                      <fieldset>
                                        <legend><?php echo $text_your_password; ?></legend>
											<div class="form-group required">
												<div class="field col-lg-12">
													<input type="password" class="fld fld-primary" id="input-password" value="<?php echo $password; ?>" name="password" required />
													<?php if ($error_password) { ?>
														<div class="text-danger"><?php echo $error_password; ?></div>
													<?php } ?>
													<label><?php echo $entry_password; ?></label>
												</div>
											</div>

											<div class="form-group required">
												<div class="field col-lg-12">
													<input type="password" class="fld fld-primary" id="input-confirm" value="<?php echo $confirm; ?>" name="confirm" required />
													<?php if ($error_confirm) { ?>
														<div class="text-danger"><?php echo $error_confirm; ?></div>
													<?php } ?>
													<label><?php echo $entry_confirm; ?></label>
												</div>
											</div>
										</fieldset>

                                      <fieldset style="display:none">
                                        <legend><?php echo $text_newsletter; ?></legend>
                                        <div class="form-group">
                                          <label class="col-lg-4 col-sm-2 control-label"><?php echo $entry_newsletter; ?></label>
                                          <div class="col-lg-8 col-sm-10">
                                           <label class="radio-inline">
                                            <input type="radio" name="newsletter" value="1" checked="checked" />
                                            <?php echo $text_yes; ?>
                                          </label>
                                          <label class="radio-inline">
                                           <input type="radio" name="newsletter" value="0" />
                                           <?php echo $text_no; ?>
                                         </label>
                                       </div>
                                     </div>
                                   </fieldset>
                                   <div style="margin-top:20px;">
                                     <?php if ($text_agree) { ?>
                                     <div class="buttons">
                                       <div class="pull-right"><?php echo $text_agree; ?>
                                        <?php if ($agree) { ?>
                                        <input type="checkbox" name="agree" value="1" checked="checked" />
                                        <?php } else { ?>
                                        <input type="checkbox" name="agree" value="1" />
                                        <?php } ?>
                                        &nbsp;
                                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                                      </div>
                                    </div>
                                    <?php } else { ?>
                                    <div class="buttons">
                                     <div class="pull-right">
                                      <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                                    </div>
                                  </div>
                                  <?php } ?>
                                </div>
                              </form>
                              <?php echo $content_bottom; ?></div>
                              <?php echo $column_right; ?></div>
                            </div>
                            <script type="text/javascript"><!--
// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('.custom-field').hide();
			$('.custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#custom-field' + custom_field['custom_field_id']).addClass('required');
				}
			}


		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
  $('button[id^=\'button-custom-field\']').on('click', function() {
   var node = this;

   $('#form-upload').remove();

   $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

   $('#form-upload input[name=\'file\']').trigger('click');

   $('#form-upload input[name=\'file\']').on('change', function() {
    $.ajax({
     url: 'index.php?route=tool/upload',
     type: 'post',
     dataType: 'json',
     data: new FormData($(this).parent()[0]),
     cache: false,
     contentType: false,
     processData: false,
     beforeSend: function() {
      $(node).button('loading');
    },
    complete: function() {
      $(node).button('reset');
    },
    success: function(json) {
      $('.text-danger').remove();

      if (json['error']) {
       $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
     }

     if (json['success']) {
       alert(json['success']);

       $(node).parent().find('input').attr('value', json['file']);
     }
   },
   error: function(xhr, ajaxOptions, thrownError) {
    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  }
});
  });
 });
 //--></script>
 <script type="text/javascript"><!--
  $('.date').datetimepicker({
   pickTime: false
 });

  $('.time').datetimepicker({
   pickDate: false
 });

  $('.datetime').datetimepicker({
   pickDate: true,
   pickTime: true
 });
 //--></script>
 <script type="text/javascript"><!--
  $('select[name=\'country_id\']').on('change', function() {
   $.ajax({
    url: 'conta/pais&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
     $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
   },
   complete: function() {
     $('.fa-spin').remove();
   },
   success: function(json) {
     if (json['postcode_required'] == '1') {
      $('input[name=\'postcode\']').parent().parent().addClass('required');
    } else {
      $('input[name=\'postcode\']').parent().parent().removeClass('required');
    }

    html = '<option value=""><?php echo $text_select; ?></option>';

    if (json['zone']) {
      for (i = 0; i < json['zone'].length; i++) {
       html += '<option value="' + json['zone'][i]['zone_id'] + '"';

       if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
        html += ' selected="selected"';
      }

      html += '>' + json['zone'][i]['name'] + '</option>';
    }
  } else {
    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
  }

  $('select[name=\'zone_id\']').html(html);
},
error: function(xhr, ajaxOptions, thrownError) {
 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
 });

  $('select[name=\'country_id\']').trigger('change');
  //--></script>

  <?php echo $footer; ?>
