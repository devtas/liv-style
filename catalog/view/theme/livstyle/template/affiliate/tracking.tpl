<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li>
				<a href="<?php echo $breadcrumb['href']; ?>">
					<?php echo $breadcrumb['text']; ?>
				</a>
			</li>
		<?php } ?>
	</ul>
	<div class="row">
		<?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-lg-6 col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-lg-9 col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-lg-12 col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>">
			<?php echo $content_top; ?>
			<h1><?php echo $heading_title; ?></h1>
			<p><?php echo $text_description; ?></p>
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-code">
						<?php echo $entry_code; ?>
					</label>
					<div class="col-sm-10">
						<!--- MUDAR -->
						<textarea cols="40" rows="5" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control">https://www.livstyle.com.br/?tracking=<?php echo $code; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-2 col-mg-3 col-sm-4">
						<img src="/image/catalog/config/partners/logo-livstyle-white.svg" height="120px"/>
					</div>
					
					<div class="col-lg-10 col-mg-9 col-sm-8">
						<textarea cols="40" rows="5" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control"><a href="https://www.livstyle.com.br/?tracking=<?php echo $code; ?>" target="_blank"><img src="https://www.livstyle.com.br/image/catalog/config/gifs/ancora-store-gif-01.gif"/></a></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-2 col-mg-3 col-sm-4">
						<img src="/image/catalog/config/partners/logo-livstyle-black.png" height="120px"/>
					</div>
					<div class="col-lg-10 col-mg-9 col-sm-8">
						<textarea cols="40" rows="5" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control"><a href="https://www.livstyle.com.br/?tracking=<?php echo $code; ?>" target="_blank"><img src="https://www.livstyle.com.br/image/catalog/config/gifs/ancora-store-gif-02.gif"/></a></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-generator"><span data-toggle="tooltip" title="<?php echo $help_generator; ?>"><?php echo $entry_generator; ?></span></label>
					<div class="col-sm-10">
						<input type="text" name="product" value="" placeholder="<?php echo $entry_generator; ?>" id="input-generator" class="form-control" /> </div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-link">
						<?php echo $entry_link; ?>
					</label>
					<div class="col-sm-10">
						<textarea name="link" cols="40" rows="5" placeholder="<?php echo $entry_link; ?>" id="input-link" class="form-control"></textarea>
					</div>
				</div>
			</form>
			<div class="buttons clearfix">
				<div class="pull-right">
					<a href="<?php echo $continue; ?>" class="btn btn-primary">
						<?php echo $button_continue; ?>
					</a>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>
<script type="text/javascript">
	<!--
	$('input[name=\'product\']').autocomplete({
		'source': function (request, response) {
			$.ajax({
				url: 'parceiros/gerador-links/autocompletar&filter_name=' + encodeURIComponent(request)
				, dataType: 'json'
				, success: function (json) {
					response($.map(json, function (item) {
						return {
							label: item['name']
							, value: item['link']
						}
					}));
				}
			});
		},
		'select': function (item) {
			$('input[name=\'product\']').val(item['label']);
			$('textarea[name=\'link\']').val(item['value']);
		}
	});
	//-->
</script>
<?php echo $footer; ?>