<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">×</button></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
	  <?php if ($column_left && $column_right) { ?>
		  <?php $class = 'col-lg-6 col-sm-6'; ?>
	  <?php } elseif ($column_left || $column_right) { ?>
		  <?php $class = 'col-lg-9 col-sm-9'; ?>
	  <?php } else { ?>
		  <?php $class = 'col-lg-12 col-sm-12'; ?>
	  <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_description; ?>
      <div class="row">
        <div class="col-sm-6">
          <div class="well">
            <h2><?php echo $text_new_affiliate; ?></h2>
            <p><?php echo $text_register_account; ?></p>
            <a class="btn btn-primary" href="<?php echo $register; ?>"><?php echo $button_continue; ?></a></div>
        </div>
        <div class="col-sm-6">
          <div class="well">
            <h2><?php echo $text_returning_affiliate; ?></h2>
            <p><strong><?php echo $text_i_am_returning_affiliate; ?></strong></p>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" novalidate>
            	<div class="form-group required">
					<div class="field">
						<input type="text" class="fld fld-primary" id="input-email" value="<?php echo $email; ?>" name="email" required />
						<label><?php echo $entry_email; ?></label>
					</div>
				</div>
				
				<div class="form-group required">
					<div class="field">
						<input type="password" class="fld fld-primary" id="input-password" value="<?php echo $password; ?>" name="password" required />
						<label><?php echo $entry_password; ?></label>
						<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
					</div>
				</div>
				
              <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>