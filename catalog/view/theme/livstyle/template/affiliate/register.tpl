<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
	  <?php if ($column_left && $column_right) { ?>
		  <?php $class = 'col-lg-6 col-sm-6'; ?>
	  <?php } elseif ($column_left || $column_right) { ?>
		  <?php $class = 'col-lg-9 col-sm-9'; ?>
	  <?php } else { ?>
		  <?php $class = 'col-lg-12 col-sm-12'; ?>
	  <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_account_already; ?></p>
      <p><?php echo $text_signup; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" novalidate>
        <fieldset class="col-lg-5">
        	<legend><?php echo $text_your_details; ?></legend>
			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-firstname" value="<?php echo $firstname; ?>" name="firstname" required />
					<?php if ($error_firstname) { ?>
						<div class="text-danger"><?php echo $error_firstname; ?></div>
					<?php } ?>
					<label><?php echo $entry_firstname; ?></label>
				</div>
            </div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-lastname" value="<?php echo $lastname; ?>" name="lastname" required />
					<?php if ($error_lastname) { ?>
						<div class="text-danger"><?php echo $error_lastname; ?></div>
					<?php } ?>
					<label><?php echo $entry_lastname; ?></label>
				</div>
            </div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-email" value="<?php echo $email; ?>" name="email" required />
					<?php if ($error_email) { ?>
						<div class="text-danger"><?php echo $error_email; ?></div>
					<?php } ?>
					<label><?php echo $entry_email; ?></label>
				</div>
            </div>

        	<div class="form-group required">
				<div class="field col-lg-12">
					<input type="tel" class="fld fld-primary" id="input-telephone" value="<?php echo $telephone; ?>" name="telephone" required />
					<?php if ($error_telephone) { ?>
						<div class="text-danger"><?php echo $error_telephone; ?></div>
					<?php } ?>
					<label><?php echo $entry_telephone; ?></label>
				</div>
            </div>

            <div class="form-group" style="display:none;">
              <label class="col-lg-4 col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
              <div class="col-lg-8 col-sm-10">
                <input type="tel" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
              </div>
            </div>

        	<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-website" value="<?php echo $website; ?>" name="website" required />
					<label><?php echo $entry_website; ?></label>
				</div>
            </div>
        </fieldset>

        <fieldset class="col-lg-6">
        	<legend><?php echo $text_your_address; ?></legend>
			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="tel" class="fld fld-primary" id="input-postcode" value="<?php echo $postcode; ?>" name="postcode" required />
					<?php if ($error_postcode) { ?>
						<div class="text-danger"><?php echo $error_postcode; ?></div>
					<?php } ?>
					<label><?php echo $entry_postcode; ?></label>
				</div>
			</div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-address-1" value="<?php echo $address_1; ?>" name="address_1" required />
					<?php if ($error_address_1) { ?>
						<div class="text-danger"><?php echo $error_address_1; ?></div>
					<?php } ?>
					<label><?php echo $entry_address_1; ?></label>
				</div>
			</div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-company" value="<?php echo $company; ?>" name="company" required />
					<label><?php echo $entry_company; ?></label>
				</div>
			</div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-address-2" value="<?php echo $address_2; ?>" name="address_2" required />
					<label><?php echo $entry_address_2; ?></label>
				</div>
			</div>

        	<div class="form-group required">
				<div class="field col-lg-12">
					<input type="text" class="fld fld-primary" id="input-city" value="<?php echo $city; ?>" name="city" required />
					<?php if ($error_city) { ?>
						<div class="text-danger"><?php echo $error_city; ?></div>
					<?php } ?>
					<label><?php echo $entry_city; ?></label>
				</div>
			</div>

			<div class="form-group required">
				<div class="col-lg-12">
					<select name="zone_id" id="input-zone" class="form-control select-primary" data-value="<?php echo $zone_id; ?>"></select>
					<?php if ($error_zone) { ?>
						<div class="text-danger"><?php echo $error_zone; ?></div>
					<?php } ?>
				</div>
			</div>

			<div class="form-group required">
				<div class="col-lg-12">
					<select name="country_id" id="input-country" class="form-control country-selector select-primary">
						<option value=""><?php echo $text_select; ?></option>
						<?php foreach ($countries as $country) { ?>
							<?php if ($country['country_id'] == $country_id) { ?>
								<option value="<?php echo $country['country_id']; ?>" selected="selected">
									<?php echo $country['name']; ?>
								</option>
								<?php } else { ?>
									<option value="<?php echo $country['country_id']; ?>">
										<?php echo $country['name']; ?>
									</option>
									<?php } ?>
								<?php } ?>
					</select>
					<?php if ($error_country) { ?>
						<div class="text-danger"><?php echo $error_country; ?></div>
					<?php } ?>
				</div>
			</div>
        </fieldset>
		<fieldset class="col-lg-5">
        	<legend><?php echo $text_your_password; ?></legend>
			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="password" class="fld fld-primary" id="input-password" value="<?php echo $password; ?>" name="password" required />
					<?php if ($error_password) { ?>
						<div class="text-danger"><?php echo $error_password; ?></div>
					<?php } ?>
					<label><?php echo $entry_password; ?></label>
				</div>
			</div>

			<div class="form-group required">
				<div class="field col-lg-12">
					<input type="password" class="fld fld-primary" id="input-confirm" value="<?php echo $confirm; ?>" name="confirm" required />
					<?php if ($error_confirm) { ?>
						<div class="text-danger"><?php echo $error_confirm; ?></div>
					<?php } ?>
					<label><?php echo $entry_confirm; ?></label>
				</div>
			</div>
        </fieldset>
        <fieldset class="col-lg-6">
          <legend><?php echo $text_payment; ?></legend>
          <div class="form-group" style="display:none;">
            <label class="col-lg-4 col-sm-2 control-label" for="input-tax"><?php echo $entry_tax; ?></label>
            <div class="col-lg-8 col-sm-10">
              <input type="text" name="tax" value="<?php echo $tax; ?>" placeholder="<?php echo $entry_tax; ?>" id="input-tax" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-8 col-sm-10">
              <div class="radio" style="display:none;">
                <label>
                  <?php if ($payment == ' cheque') { ?>
                  <input type="hidden" name="payment" value="cheque" checked="checked" />
                  <?php } else { ?>
                  <input type="hidden" name="payment" value="cheque" />
                  <?php } ?>
                  <?php echo $text_cheque; ?></label>
              </div>
              <div class="radio">
                <label>
                  <?php if ($payment == 'paypal') { ?>
                  <input type="radio" name="payment" value="paypal" checked="checked" />
                  <?php } else { ?>
                  <input type="radio" name="payment" value="paypal" checked="checked" />
                  <?php } ?>
                  <?php echo $text_paypal; ?></label>
              </div>
              <div class="radio" style="display:none;">
                <label>
                  <?php if ($payment == ' bank') { ?>
                  <input type="hidden" name="payment" value="bank" checked="checked" />
                  <?php } else { ?>
                  <input type="hidden" name="payment" value="bank" />
                  <?php } ?>
                  <?php echo $text_bank; ?></label>
              </div>
            </div>
          </div>
          <div class="form-group payment" id="payment-cheque">
            <label class="col-lg-4 col-sm-2 control-label" for="input-cheque"><?php echo $entry_cheque; ?></label>
            <div class="col-lg-8 col-sm-10">
              <input type="text" name="cheque" value="<?php echo $cheque; ?>" placeholder="<?php echo $entry_cheque; ?>" id="input-cheque" class="form-control" />
            </div>
          </div>
          <div class="form-group required" id="payment-paypal">
			<div class="field col-lg-12">
				<input type="text" class="fld fld-primary" id="input-paypal" value="<?php echo $paypal; ?>" name="paypal" required />
				<label><?php echo $entry_paypal; ?></label>
			</div>
          </div>
          <div class="payment" id="payment-bank">
            <div class="form-group">
              <label class="col-lg-4 col-sm-2 control-label" for="input-bank-name"><?php echo $entry_bank_name; ?></label>
              <div class="col-lg-8 col-sm-10">
                <input type="text" name="bank_name" value="<?php echo $bank_name; ?>" placeholder="<?php echo $entry_bank_name; ?>" id="input-bank-name" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-bank-branch-number"><?php echo $entry_bank_branch_number; ?></label>
              <div class="col-sm-10">
                <input type="text" name="bank_branch_number" value="<?php echo $bank_branch_number; ?>" placeholder="<?php echo $entry_bank_branch_number; ?>" id="input-bank-branch-number" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-bank-swift-code"><?php echo $entry_bank_swift_code; ?></label>
              <div class="col-sm-10">
                <input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" placeholder="<?php echo $entry_bank_swift_code; ?>" id="input-bank-swift-code" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-bank-account-name"><?php echo $entry_bank_account_name; ?></label>
              <div class="col-sm-10">
                <input type="text" name="bank_account_name" value="<?php echo $bank_account_name; ?>" placeholder="<?php echo $entry_bank_account_name; ?>" id="input-bank-account-name" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-bank-account-number"><?php echo $entry_bank_account_number; ?></label>
              <div class="col-sm-10">
                <input type="text" name="bank_account_number" value="<?php echo $bank_account_number; ?>" placeholder="<?php echo $entry_bank_account_number; ?>" id="input-bank-account-number" class="form-control" />
              </div>
            </div>
          </div>
        </fieldset>

		<div style="margin-top:20px;">
			<?php if ($text_agree) { ?>
			<div class="buttons clearfix">
			  <div class="pull-right"><?php echo $text_agree; ?>
				<?php if ($agree) { ?>
				<input type="checkbox" name="agree" value="1" checked="checked" />
				<?php } else { ?>
				<input type="checkbox" name="agree" value="1" />
				<?php } ?>
				&nbsp;
				<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
			  </div>
			</div>
			<?php } else { ?>
			<div class="buttons clearfix">
			  <div class="pull-right">
				<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
			  </div>
			</div>
			<?php } ?>
		</div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'parceiro/conta/pais&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
      $('.fa-spin').remove();

			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone']) {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
    	},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('input[name=\'payment\']').on('change', function() {
	$('.payment').hide();

	$('#payment-' + this.value).show();
});

$('input[name=\'payment\']:checked').trigger('change');
//--></script>
<?php echo $footer; ?>