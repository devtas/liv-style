jQuery(document).ready(function($){
	//if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
	var MqL = 1170;
	//move nav element position according to window width

	//mobile - open lateral menu clicking on the menu icon
	$('.kl-nav-trigger').on('click', function(event){
		event.preventDefault();
		if( $('.kl-main-content').hasClass('nav-is-visible') ) {
			closeNav();
			$('.kl-overlay').removeClass('is-visible');
		} else {
			$(this).addClass('nav-is-visible');
			$('.kl-primary-nav').addClass('nav-is-visible');
			$('.header').addClass('nav-is-visible');
			$('.topbar').addClass('nav-is-visible');
			$('.kl-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').addClass('overflow-hidden');
			});
			toggleSearch('close');
			$('.kl-overlay').addClass('is-visible');
		}
	});

	//open search form
	$('.kl-search-trigger').on('click', function(event){
		event.preventDefault();
		toggleSearch();
		closeNav();
	});

	//close lateral menu on mobile
	$('.kl-overlay').on('swiperight', function(){
		if($('.kl-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('.kl-overlay').removeClass('is-visible');
		}
	});
	$('.nav-on-left .kl-overlay').on('swipeleft', function(){
		if($('.kl-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('.kl-overlay').removeClass('is-visible');
		}
	});
	$('.kl-overlay').on('click', function(){
		closeNav();
		toggleSearch('close')
		$('.kl-overlay').removeClass('is-visible');
	});


	//prevent default clicking on direct children of .kl-primary-nav
	$('.kl-primary-nav').children('.has-children').children('a').on('click', function(event){
		event.preventDefault();
	});
	//open submenu
	$('.has-children').children('a').on('click', function(event){
		if( !checkWindowWidth() ) event.preventDefault();
		var selected = $(this);
		if( selected.next('ul').hasClass('is-hidden') ) {
			//desktop version only
			selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
			selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
			$('.kl-overlay').addClass('is-visible');
		} else {
			selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
			$('.kl-overlay').removeClass('is-visible');
		}
		toggleSearch('close');
	});

	//submenu items - go back link
	$('.go-back').on('click', function(){
		$(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
	});

	function closeNav() {
		$('.kl-nav-trigger').removeClass('nav-is-visible');
		$('.header').removeClass('nav-is-visible');
		$('.topbar').removeClass('nav-is-visible');
		$('.kl-primary-nav').removeClass('nav-is-visible');
		$('.has-children ul').addClass('is-hidden');
		$('.has-children a').removeClass('selected');
		$('.moves-out').removeClass('moves-out');
		$('.kl-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('body').removeClass('overflow-hidden');
		});
	}

	function toggleSearch(type) {
		if(type=="close") {
			//close serach
			$('.kl-search').removeClass('is-visible');
			$('.kl-search-trigger').removeClass('search-is-visible');
		} else {
			//toggle search visibility
			$('.kl-search').toggleClass('is-visible');
			$('.kl-search-trigger').toggleClass('search-is-visible');
			if($(window).width() > MqL && $('.kl-search').hasClass('is-visible')) $('.kl-search').find('input[type="search"]').focus();
			($('.kl-search').hasClass('is-visible')) ? $('.kl-overlay').addClass('is-visible') : $('.kl-overlay').removeClass('is-visible') ;
		}
	}

	function checkWindowWidth() {
		//check window width (scrollbar included)
		var e = window,
		a = 'inner';
		if (!('innerWidth' in window )) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		if ( e[ a+'Width' ] >= MqL ) {
			return true;
		} else {
			return false;
		}
	}

	function moveNavigation(){
		var navigation = $('.kl-nav');
		var desktop = checkWindowWidth();
		if ( desktop ) {
			navigation.detach();
			navigation.insertBefore('.kl-header-buttons');
		} else {
			navigation.detach();
			navigation.insertAfter('.kl-main-content');
		}
	}
});

/*
	Busca CEP
*/

$(function(){
	$('input[name="postcode"]').keyup(function() {

		if($('#address-form input[name="postcode"]')[0] === undefined && $('#checkout-form input[name="postcode"]')[0] === undefined) {
			var cep = $.trim($('input[name="postcode"]').val().replace(/\D/g, ''));

			if(cep.length === 8) {
				$.getScript("https://viacep.com.br/ws/"+ cep +"/json/?callback=var response=", function() {
					if(!response.erro) {
						if(response.logradouro !== '') {
							$('input[name="address_1"]').val(response.logradouro);
							$('input[name="address_2"]').val(response.bairro);
							$('input[name="city"]').val(response.localidade);
							$('option:contains("Brasil")', 'select[name="country_id"]')[0].selected = true;
							$('option:contains("'+getState(response.uf)+'")', 'select[name="zone_id"]')[0].selected = true;
							$('input[name="company"]').focus();
						}
						else {
							$('input[name="address_1"]').val(response.logradouro);
							$('input[name="address_2"]').val(response.bairro);
							$('input[name="city"]').val(response.localidade);
							$('option:contains("Brasil")', 'select[name="country_id"]')[0].selected = true;
							$('option:contains("'+getState(response.uf)+'")', 'select[name="zone_id"]')[0].selected = true;
							$('input[name="address_1"]').focus();
						}
					}
					else {
						$('input[name="address_1"]').val('');
						$('input[name="address_2"]').val('');
						$('input[name="city"]').val('');
						$('option:contains("Brasil")', 'select[name="country_id"]')[0].selected = true;
						$('option:contains("")', 'select[name="zone_id"]')[0].selected = true;
						$('input[name="address_1"]').focus();
					}
				});
			}
			else {
				$('input[name="address_1"]').val('');
				$('input[name="address_2"]').val('');
				$('input[name="city"]').val('');
				$('option:contains("Brasil")', 'select[name="country_id"]')[0].selected = true;
				$('option:contains("")', 'select[name="zone_id"]')[0].selected = true;
			}
		}
	});

	$('#address-form input[name="postcode"]').keyup(function() {
		var cep = $.trim($('#address-form input[name="postcode"]').val().replace(/\D/g, ''));

		if(cep.length === 8) {
			$.getScript("https://viacep.com.br/ws/"+ cep +"/json/?callback=var response=", function() {
				if(!response.erro) {
					if(response.logradouro !== '') {
						$('#address-form input[name="address_1"]').val(response.logradouro);
						$('#address-form input[name="address_2"]').val(response.bairro);
						$('#address-form input[name="city"]').val(response.localidade);
						$('option:contains("Brasil")', '#address-form select[name="country_id"]')[0].selected = true;
						$('option:contains("'+getState(response.uf)+'")', '#address-form select[name="zone_id"]')[0].selected = true;
						$('#address-form input[name="company"]').focus();
					}
					else {
						$('#address-form input[name="address_1"]').val(response.logradouro);
						$('#address-form input[name="address_2"]').val(response.bairro);
						$('#address-form input[name="city"]').val(response.localidade);
						$('option:contains("Brasil")', '#address-form select[name="country_id"]')[0].selected = true;
						$('option:contains("'+getState(response.uf)+'")', '#address-form select[name="zone_id"]')[0].selected = true;
						$('#address-form input[name="address_1"]').focus();
					}
				}
				else {
					$('#address-form input[name="address_1"]').val('');
					$('#address-form input[name="address_2"]').val('');
					$('#address-form input[name="city"]').val('');
					$('option:contains("Brasil")', '#address-form select[name="country_id"]')[0].selected = true;
					$('option:contains("")', '#address-form select[name="zone_id"]')[0].selected = true;
					$('#address-form input[name="address_1"]').focus();
				}
			});
		}
		else {
			$('#address-form input[name="address_1"]').val('');
			$('#address-form input[name="address_2"]').val('');
			$('#address-form input[name="city"]').val('');
			$('option:contains("Brasil")', '#address-form select[name="country_id"]')[0].selected = true;
			$('option:contains("")', '#address-form select[name="zone_id"]')[0].selected = true;
		}
	});

	$('#checkout-form input[name="postcode"]').keyup(function() {
		var cep = $.trim($('#checkout-form input[name="postcode"]').val().replace(/\D/g, ''));

		if(cep.length === 8) {
			$.getScript("https://viacep.com.br/ws/"+ cep +"/json/?callback=var response=", function() {
				if(!response.erro) {
					if(response.logradouro !== '') {
						$('#checkout-form input[name="address_1"]').val(response.logradouro);
						$('#checkout-form input[name="address_2"]').val(response.bairro);
						$('#checkout-form input[name="city"]').val(response.localidade);
						$('option:contains("Brasil")', '#checkout-form select[name="country_id"]')[0].selected = true;
						$('option:contains("'+getState(response.uf)+'")', '#checkout-form select[name="zone_id"]')[0].selected = true;
						$('#checkout-form input[name="company"]').focus();
					}
					else {
						$('#checkout-form input[name="address_1"]').val(response.logradouro);
						$('#checkout-form input[name="address_2"]').val(response.bairro);
						$('#checkout-form input[name="city"]').val(response.localidade);
						$('option:contains("Brasil")', '#checkout-form select[name="country_id"]')[0].selected = true;
						$('option:contains("'+getState(response.uf)+'")', '#checkout-form select[name="zone_id"]')[0].selected = true;
						$('#checkout-form input[name="address_1"]').focus();
					}
				}
				else {
					$('#checkout-form input[name="address_1"]').val('');
					$('#checkout-form input[name="address_2"]').val('');
					$('#checkout-form input[name="city"]').val('');
					$('option:contains("Brasil")', '#checkout-form select[name="country_id"]')[0].selected = true;
					$('option:contains("")', '#checkout-form select[name="zone_id"]')[0].selected = true;
					$('#checkout-form input[name="address_1"]').focus();
				}
			});
		}
		else {
			$('#checkout-form input[name="address_1"]').val('');
			$('#checkout-form input[name="address_2"]').val('');
			$('#checkout-form input[name="city"]').val('');
			$('option:contains("Brasil")', '#checkout-form select[name="country_id"]')[0].selected = true;
			$('option:contains("")', '#checkout-form select[name="zone_id"]')[0].selected = true;
		}
	});

	$(document).on('keyup', '.checkout-checkout input[name="postcode"]', function() {
		var cep = $.trim($('.checkout-checkout input[name="postcode"]').val().replace(/\D/g, ''));

		if(cep.length === 8) {
			$.getScript("https://viacep.com.br/ws/"+ cep +"/json/?callback=var response=", function() {
				if(!response.erro) {
					if(response.logradouro !== '') {
						$('.checkout-checkout input[name="address_1"]').val(response.logradouro);
						$('.checkout-checkout input[name="address_2"]').val(response.bairro);
						$('.checkout-checkout input[name="city"]').val(response.localidade);
						$('option:contains("Brasil")', '.checkout-checkout select[name="country_id"]')[0].selected = true;
						$('option:contains("'+getState(response.uf)+'")', '.checkout-checkout select[name="zone_id"]')[0].selected = true;
						// $('.checkout-checkout input[name="company"]').focus();
					}
					else {
						$('.checkout-checkout input[name="address_1"]').val(response.logradouro);
						$('.checkout-checkout input[name="address_2"]').val(response.bairro);
						$('.checkout-checkout input[name="city"]').val(response.localidade);
						$('option:contains("Brasil")', '.checkout-checkout select[name="country_id"]')[0].selected = true;
						$('option:contains("'+getState(response.uf)+'")', '.checkout-checkout select[name="zone_id"]')[0].selected = true;
						// $('.checkout-checkout input[name="address_1"]').focus();
					}
				}
				else {
					$('.checkout-checkout input[name="address_1"]').val('');
					$('.checkout-checkout input[name="address_2"]').val('');
					$('.checkout-checkout input[name="city"]').val('');
					$('option:contains("Brasil")', '.checkout-checkout select[name="country_id"]')[0].selected = true;
					$('option:contains("")', '.checkout-checkout select[name="zone_id"]')[0].selected = true;
					// $('.checkout-checkout input[name="address_1"]').focus();
				}
			});
		}
		else {
			$('.checkout-checkout input[name="address_1"]').val('');
			$('.checkout-checkout input[name="address_2"]').val('');
			$('.checkout-checkout input[name="city"]').val('');
			$('option:contains("Brasil")', '.checkout-checkout select[name="country_id"]')[0].selected = true;
			$('option:contains("")', '.checkout-checkout select[name="zone_id"]')[0].selected = true;
		}
	});


	function getState(uf) {
		switch(uf) {
			case 'AC':
			return 'Acre';
			case 'AL':
			return 'Alagoas';
			case 'AP':
			return 'Amapá';
			case 'AM':
			return 'Amazonas';
			case 'BA':
			return 'Bahia';
			case 'CE':
			return 'Ceará';
			case 'DF':
			return 'Distrito Federal';
			case 'ES':
			return 'Espírito Santo';
			case 'GO':
			return 'Goiás';
			case 'MA':
			return 'Maranhão';
			case 'MT':
			return 'Mato Grosso';
			case 'MS':
			return 'Mato Grosso do Sul';
			case 'MG':
			return 'Minas Gerais';
			case 'PA':
			return 'Pará';
			case 'PB':
			return 'Paraíba';
			case 'PR':
			return 'Paraná';
			case 'PE':
			return 'Pernambuco';
			case 'PI':
			return 'Piauí';
			case 'RJ':
			return 'Rio de Janeiro';
			case 'RN':
			return 'Rio Grande do Norte';
			case 'RS':
			return 'Rio Grande do Sul';
			case 'RO':
			return 'Rondônia';
			case 'RR':
			return 'Roraima';
			case 'SC':
			return 'Santa Catarina';
			case 'SP':
			return 'São Paulo';
			case 'SG':
			return 'Sergipe';
			case 'TO':
			return 'Tocantins';
			default:
			return '';
		}
	}
});



/* menu */

(function() {
	$('.icon-bag').click(function() {
		if(!$('.cart-content-caret').hasClass('show')) {
			$('.cart-content-caret').addClass('show');
			$('.cart').addClass('show');
			$('.header.box-search').removeClass('show');
		}
		else {
			$('.cart-content-caret').removeClass('show');
			$('.cart').removeClass('show');
		}
	});

	$('.icon-search').click(function() {
		if(!$('.header.box-search').hasClass('show')) {
			$('.header.box-search').addClass('show');
			$('.cart-content-caret').removeClass('show');
			$('.cart').removeClass('show');
		}
		else {
			$('.header.box-search').removeClass('show');
		}
	});

	$('.icon-account').click(function() {
		$('.cart-content-caret').removeClass('show');
		$('.cart').removeClass('show');
		$('.header.box-search').removeClass('show');
	});

	$('.menu-icon').click(function() {
		$('.cart-content-caret').removeClass('show');
		$('.cart').removeClass('show');
		$('.header.box-search').removeClass('show');

		if($('.icon-bag').hasClass('hide')) {
			$('.icon-bag').removeClass('hide');
			$('.header.box-search').removeClass('show');
		}
		else {
			$('.icon-bag').addClass('hide');
		}
		if($('.icon-favorite').hasClass('hide')) {
			$('.icon-favorite').removeClass('hide');
		}
		else {
			$('.icon-favorite').addClass('hide');
		}

		if(!$('.icon-account').hasClass('hide')) {
			$('.icon-account').addClass('hide');
		}
		else {
			$('.icon-account').removeClass('hide');
		}

		if(!$('.icon-search').hasClass('hide')) {
			$('.icon-search').addClass('hide');
		}
		else {
			$('.icon-search').removeClass('hide');
		}
	});
})();




/*
jQuery Masked Input Plugin
Version: 1.14.0
github.com/igorescobar/jQuery-Mask-Plugin
*/

!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a(require("jquery")):a(jQuery||Zepto)}(function(a){var b=function(b,c,d){var e={invalid:[],getCaret:function(){try{var a,c=0,d=b.get(0),f=document.selection,g=d.selectionStart;return f&&-1===navigator.appVersion.indexOf("MSIE 10")?(a=f.createRange(),a.moveStart("character",-e.val().length),c=a.text.length):(g||"0"===g)&&(c=g),c}catch(a){}},setCaret:function(a){try{if(b.is(":focus")){var c,d=b.get(0);d.setSelectionRange?(d.focus(),d.setSelectionRange(a,a)):(c=d.createTextRange(),c.collapse(!0),c.moveEnd("character",a),c.moveStart("character",a),c.select())}}catch(a){}},events:function(){b.on("keydown.mask",function(a){b.data("mask-keycode",a.keyCode||a.which)}).on(a.jMaskGlobals.useInput?"input.mask":"keyup.mask",e.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){b.keydown().keyup()},100)}).on("change.mask",function(){b.data("changed",!0)}).on("blur.mask",function(){g===e.val()||b.data("changed")||b.trigger("change"),b.data("changed",!1)}).on("blur.mask",function(){g=e.val()}).on("focus.mask",function(b){!0===d.selectOnFocus&&a(b.target).select()}).on("focusout.mask",function(){d.clearIfNotMatch&&!h.test(e.val())&&e.val("")})},getRegexMask:function(){for(var b,d,e,g,a=[],h=0;h<c.length;h++)(b=f.translation[c.charAt(h)])?(d=b.pattern.toString().replace(/.{1}$|^.{1}/g,""),e=b.optional,(b=b.recursive)?(a.push(c.charAt(h)),g={digit:c.charAt(h),pattern:d}):a.push(e||b?d+"?":d)):a.push(c.charAt(h).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));return a=a.join(""),g&&(a=a.replace(new RegExp("("+g.digit+"(.*"+g.digit+")?)"),"($1)?").replace(new RegExp(g.digit,"g"),g.pattern)),new RegExp(a)},destroyEvents:function(){b.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))},val:function(a){var c=b.is("input")?"val":"text";return 0<arguments.length?(b[c]()!==a&&b[c](a),c=b):c=b[c](),c},getMCharsBeforeCount:function(a,b){for(var d=0,e=0,g=c.length;e<g&&e<a;e++)f.translation[c.charAt(e)]||(a=b?a+1:a,d++);return d},caretPos:function(a,b,d,g){return f.translation[c.charAt(Math.min(a-1,c.length-1))]?Math.min(a+d-b-g,d):e.caretPos(a+1,b,d,g)},behaviour:function(c){c=c||window.event,e.invalid=[];var d=b.data("mask-keycode");if(-1===a.inArray(d,f.byPassKeys)){var g=e.getCaret(),h=e.val().length,i=e.getMasked(),j=i.length,k=e.getMCharsBeforeCount(j-1)-e.getMCharsBeforeCount(h-1),l=g<h;return e.val(i),l&&(8!==d&&46!==d&&(g=e.caretPos(g,h,j,k)),e.setCaret(g)),e.callbacks(c)}},getMasked:function(a,b){var p,q,g=[],h=void 0===b?e.val():b+"",i=0,j=c.length,k=0,l=h.length,m=1,n="push",o=-1;for(d.reverse?(n="unshift",m=-1,p=0,i=j-1,k=l-1,q=function(){return-1<i&&-1<k}):(p=j-1,q=function(){return i<j&&k<l});q();){var r=c.charAt(i),s=h.charAt(k),t=f.translation[r];t?(s.match(t.pattern)?(g[n](s),t.recursive&&(-1===o?o=i:i===p&&(i=o-m),p===o&&(i-=m)),i+=m):t.optional?(i+=m,k-=m):t.fallback?(g[n](t.fallback),i+=m,k-=m):e.invalid.push({p:k,v:s,e:t.pattern}),k+=m):(a||g[n](r),s===r&&(k+=m),i+=m)}return h=c.charAt(p),j!==l+1||f.translation[h]||g.push(h),g.join("")},callbacks:function(a){var f=e.val(),h=f!==g,i=[f,a,b,d],j=function(a,b,c){"function"==typeof d[a]&&b&&d[a].apply(this,c)};j("onChange",!0===h,i),j("onKeyPress",!0===h,i),j("onComplete",f.length===c.length,i),j("onInvalid",0<e.invalid.length,[f,a,b,e.invalid,d])}};b=a(b);var h,f=this,g=e.val();c="function"==typeof c?c(e.val(),void 0,b,d):c,f.mask=c,f.options=d,f.remove=function(){var a=e.getCaret();return e.destroyEvents(),e.val(f.getCleanVal()),e.setCaret(a-e.getMCharsBeforeCount(a)),b},f.getCleanVal=function(){return e.getMasked(!0)},f.getMaskedVal=function(a){return e.getMasked(!1,a)},f.init=function(c){c=c||!1,d=d||{},f.clearIfNotMatch=a.jMaskGlobals.clearIfNotMatch,f.byPassKeys=a.jMaskGlobals.byPassKeys,f.translation=a.extend({},a.jMaskGlobals.translation,d.translation),f=a.extend(!0,{},f,d),h=e.getRegexMask(),!1===c?(d.placeholder&&b.attr("placeholder",d.placeholder),b.data("mask")&&b.attr("autocomplete","off"),e.destroyEvents(),e.events(),c=e.getCaret(),e.val(e.getMasked()),e.setCaret(c+e.getMCharsBeforeCount(c,!0))):(e.events(),e.val(e.getMasked()))},f.init(!b.is("input"))};a.maskWatchers={};var c=function(){var c=a(this),e={},f=c.attr("data-mask");if(c.attr("data-mask-reverse")&&(e.reverse=!0),c.attr("data-mask-clearifnotmatch")&&(e.clearIfNotMatch=!0),"true"===c.attr("data-mask-selectonfocus")&&(e.selectOnFocus=!0),d(c,f,e))return c.data("mask",new b(this,f,e))},d=function(b,c,d){d=d||{};var e=a(b).data("mask"),f=JSON.stringify;b=a(b).val()||a(b).text();try{return"function"==typeof c&&(c=c(b)),"object"!=typeof e||f(e.options)!==f(d)||e.mask!==c}catch(a){}};a.fn.mask=function(c,e){e=e||{};var f=this.selector,g=a.jMaskGlobals,h=g.watchInterval,g=e.watchInputs||g.watchInputs,i=function(){if(d(this,c,e))return a(this).data("mask",new b(this,c,e))};return a(this).each(i),f&&""!==f&&g&&(clearInterval(a.maskWatchers[f]),a.maskWatchers[f]=setInterval(function(){a(document).find(f).each(i)},h)),this},a.fn.masked=function(a){return this.data("mask").getMaskedVal(a)},a.fn.unmask=function(){return clearInterval(a.maskWatchers[this.selector]),delete a.maskWatchers[this.selector],this.each(function(){var b=a(this).data("mask");b&&b.remove().removeData("mask")})},a.fn.cleanVal=function(){return this.data("mask").getCleanVal()},a.applyDataMask=function(b){b=b||a.jMaskGlobals.maskElements,(b instanceof a?b:a(b)).filter(a.jMaskGlobals.dataMaskAttr).each(c)};var e={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,useInput:function(a){var c,b=document.createElement("div");return a="on"+a,c=a in b,c||(b.setAttribute(a,"return;"),c="function"==typeof b[a]),c}("input"),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};a.jMaskGlobals=a.jMaskGlobals||{},e=a.jMaskGlobals=a.extend(!0,{},e,a.jMaskGlobals),e.dataMask&&a.applyDataMask(),setInterval(function(){a.jMaskGlobals.watchDataMask&&a.applyDataMask()},e.watchInterval)});

$(function() {
	$('input[name="postcode"]').mask("00000-000");
	$('input[name="fax"]').mask("000.000.000-00");
	$('input[name="amount"]').mask('###,00', {reverse: true});

	var maskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	options = {
		onKeyPress: function(val, e, field, options) {
			field.mask(maskBehavior.apply({}, arguments), options);
		}
	};

	$('input[name="telephone"]').mask(maskBehavior, options);
});


$(function() {
	$('.field > .fld').focus(function() {
		$(this).addClass('fld-active');
	});

	$('.field > .fld').blur(function() {
		if($(this).val().length === 0) {
			$(this).removeClass('fld-active');
		}
	});

	$('.field > .fld').on('invalid', function(event) {
		console.log(event.type);
	});

	$(document).on('change', '.field > .fld', function() {
		if($(this).val().length > 0) {
			$(this).addClass('fld-active');
		}
		else {
			$(this).removeClass('fld-active');
		}
	});
});
