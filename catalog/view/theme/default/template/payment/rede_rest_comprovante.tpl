<style>
  .payment-rede_rest_credito-comprovante #cupom.panel-info {
    border: none;
  }
  .payment-rede_rest_credito-comprovante #cupom.panel {
    border: none;
    box-shadow: none;
    -webkit-box-shadow: none;
  }

  .payment-rede_rest_credito-comprovante #cupom.panel-info>.panel-heading {
    color: #000;
    text-transform: uppercase;
    background-color: inherit;
    border: none;
  }
</style>

<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <div id="content" class="col-sm-12">
      <div class="row">
        <div class="col-sm-4">
          <div id="cupom" class="panel panel-info">
            <div class="panel-heading">
              <?php echo $heading_title; ?>
              </div>
            <div class="panel-body">
              <?php echo nl2br($text_comprovante); ?>

              <?php if (isset($plp)) { ?>
                <p>Para rastrear a sua entrega, utilize o código: <?php echo $plp; ?></p>
              <?php } ?>
            </div>
          </div>
          <div class="buttons" style="margin-left: 14px;">
            <a href="javascript:window.print()" target="_blank" class="btn btn-primary"><?php echo $button_imprimir; ?></a>
            <!--a href="<?php echo $print; ?>" target="_blank" class="btn btn-primary"><?php echo $button_imprimir; ?></a-->
          </div>
        </div>
        <div class="col-sm-8">
          <div id="cupom" class="panel panel-info">
            <div class="panel-body">
            <?php echo $text_importante; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8"></div>
        <div class="col-sm-4">
          <?php if (isset($items)): ?>
            <div class="order_items">
              <h3><?php echo $items_title; ?></h3>
              <div class="items">
                <?php foreach ($items as $key => $item): ?>
                  <div class="item">
                    <div class="image">
                      <img src="/image/<?php echo $item['image']; ?>" height="45" alt="<?php echo $item['name']; ?>">
                    </div>
                    <div class="text">
                      <p class="pull-right"><b>Total: <?php echo $item['total_format']; ?></b></p>
                      x<?php echo $item['quantity']; ?> <b><?php echo $item['name']; ?></b><br />
                      <p><?php echo $item['price_format']; ?></p>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
              <BR />
              <BR />
              <div class="item">
                <p class="pull-right"><?php echo $order_totals['value_format']; ?></p>
                SUBTOTAL
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
    fbq('track', 'Purchase', {
      content_type: 'product',
      currency: "BRL",
      value: <?php echo $order_totals['value']; ?>
    });
</script>