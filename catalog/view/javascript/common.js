function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

function getURLVarNew(key) {
	var value = [];

	var query = String(document.location).split('/');

	if (query[3]) {
		return query[3];
	}
	else {
		return '';
	}
}

function isTouchDevice(){
	return true === ('ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch);
}

$(document).ready(function() {

	if(isTouchDevice() === false) {
		// tooltips on hover
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

		// Makes tooltips work on ajax generated content
		$(document).ajaxStop(function() {
			$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
		});
	}

	$("a[href='#tab-review']").on('shown.bs.tab', function(e){
		$('html, body').animate({scrollTop: $('#review').offset().top - 120}, 300);
	});

	$('#button-cart').on('click', function(){
		if($('#product select').val() == "" && screen.width < 640) {
			$('html, body').animate({scrollTop: $('#product').offset().top - 120}, 300);
		}
	});

	$('.product-rating > p > a').click(function(e){
		var _review = $('#tab-review');
		var _description = $('#tab-description');
		var review = $("a[href='#tab-review']");
		var description = $("a[href='#tab-description']");

		$('html, body').animate({scrollTop: $(review).offset().top - 60}, 300);
		if(!review.parent().hasClass('active')){
			description.parent().removeClass('active');
			review.parent().addClass('active');

			_description.removeClass('active');
			_review.addClass('active');
		}
	});
	// Adding the clear Fix
	cols1 = $('#column-right, #column-left').length;

	if (cols1 == 2) {
		$('#content .product-layout:nth-child(2n+2)').after('<div class="clearfix visible-md visible-sm"></div>');
	} else if (cols1 == 1) {
		$('#content .product-layout:nth-child(3n+3)').after('<div class="clearfix visible-lg"></div>');
	} else {
		$('#content .product-layout:nth-child(4n+4)').after('<div class="clearfix"></div>');
	}

	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});

	// Language
	$('#language a').on('click', function(e) {
		e.preventDefault();

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
		url = $('base').attr('href') + 'busca';

		var value = $('header input[name=\'search\']').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-6');
		} else if (cols == 1) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-6');
		}

		localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}

	// // tooltips on hover
	// $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

	// // Makes tooltips work on ajax generated content
	// $(document).ajaxStop(function() {
	// 	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	// });
});

function closeAlert(time) {
	setTimeout(function(){
		$('.alert').alert('close');
	}, time);
}

// Cart add remove functions
var cart = {
	// 'add': function(product_id, quantity) {
	// 	$.ajax({
	//         url: 'carrinho/adicionar',
	//         type: 'post',
	//         data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
	//         dataType: 'json',
	//         beforeSend: function() {
	//           $('#button-cart').button('loading');
	//         },
	//         complete: function() {
	//           $('#button-cart').button('reset');
	//         },
	//         success: function(json) {
	//         	console.log(json);
	//           $('.alert, .text-danger').remove();
	//           $('.form-group').removeClass('has-error');

	//           if (json['error']) {
	//             if (json['error']['option']) {
	//               for (i in json['error']['option']) {
	//                 var element = $('#input-option' + i.replace('_', '-'));

	//                 if (element.parent().hasClass('input-group')) {
	//                   element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
	//                 } else {
	//                   element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
	//                 }
	//               }
	//             }

	//             if (json['error']['recurring']) {
	//               $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
	//             }

	//             // Highlight any found errors
	//             $('.text-danger').parent().addClass('has-error');
	//           }

	//           if (json['success']) {
	//             $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

	//             $('#cart-total').html(json['total']);

	//             $('#cart').load('/carrinho/info #cart');
	//           }

	//         	closeAlert(5000);
	//         }
	//     });
	// },
	'update': function(key, quantity) {
		$.ajax({
			url: 'carrinho/editar',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			success: function(json) {
				$('#cart > button').button('reset');

				$('#cart-total').html(json['total']);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout' || getURLVarNew('route') == 'carrinho') {
					location = 'carrinho';
				} else {
					$('#cart').load('/carrinho/info #cart');
	            	$('.cart-product-total-number').load('/carrinho/quantidade');
				}
			}
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'carrinho/remover',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			success: function(json) {
				$('#cart > button').button('reset');

				$('#cart-total').html(json['total']);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout' || getURLVarNew('route') == 'carrinho') {
					location = 'carrinho';
				} else {
					$('#cart').load('/carrinho/info #cart');
	            	$('.cart-product-total-number').load('/carrinho/quantidade');
				}
			}
		});
	},
	'redirect': function(product){
		window.location.href = product;
	},
	'increase': function(evt) {
		var ref = evt.getAttribute('data-ref');
		var value = $('.' + ref).val();
		value++;
		$('.' + ref).val(value);
	},
	'decrease': function(evt) {
		var ref = evt.getAttribute('data-ref');
		var value = $('.' + ref).val();
		value--;
		value > 0 ? $('.' + ref).val(value) : $('.' + ref).val(0);
	}
};

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'carrinho/remover',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('#cart-total').html(json['total']);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout' || getURLVarNew('route') == 'carrinho') {
					location = 'carrinho';
				} else {
					$('#cart').load('/carrinho/info #cart');
	            	$('.cart-product-total-number').load('/carrinho/quantidade');
				}
			}
		});
	}
};

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'conta/desejos/adicionar',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div id="f-modal" class="fadeModal"><div class="liv-alert">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
					document.querySelector('.close').addEventListener('click', function() {
						document.querySelector('#f-modal').remove();
					});
				}

				if (json['info']) {
					$('#content').parent().before('<div id="f-modal" class="fadeModal"><div class="liv-alert">' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
						document.querySelector('.close').addEventListener('click', function() {
							document.querySelector('#f-modal').remove();
						});
				}

				$('#wishlist-total').html(json['total']);

				closeAlert(50000);
			}
		});
	},
	'remove': function() {

	}
};

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'comparacao/adicionar',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);
				}
				closeAlert(50000);
			}
		});
	},
	'remove': function() {

	}
};

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '			 <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';
			console.log(data);
			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

/** Size */
$(document).delegate('.product-size-popup', 'click', function(e) {
	e.preventDefault();

	$('#modal-popup').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-popup" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '		 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';
			$('body').append(html);

			$('#modal-popup').modal('show');
		}
	});
});


function _setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function _getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function _checkCookie(cname) {
  var user = _getCookie(cname);
  if (user != "") {
    return true;
  } else {
    return false;
  }
}


/* Agree to Terms */
function openModalSubscribe() {
	$('#modal-first-day-look').remove();

	var element = this;
	$.ajax({
		url: '/termos&information_id=8',
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-first-day-look" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-first-day-look').modal('show');
		}
	});
}

function verifySubscribeModal2() {
	if (_checkCookie('liv_newsletter')) {
		let newsCookie = _getCookie('liv_newsletter');
		if (newsCookie > 0) {
			_setCookie('liv_newsletter', newsCookie - 1, 1);
		} else {
			openModalSubscribe();
		}
	} else {
		openModalSubscribe();
	}
}

function verifySubscribeModal() {
	if (! _checkCookie('liv_newsletter')) {
		openModalSubscribe();
	}
}

setTimeout(function() {
	verifySubscribeModal();
}, 3000);


$(document).on('click', '#modal-first-day-look button.close, #modal-first-day-look input[type="submit"]', function() {
	console.log('set cookie');
	_setCookie('liv_newsletter', 'viewed', 1);
})


$(document).delegate('.first-day-look', 'click', function(e) {
	e.preventDefault();

	$('#modal-first-day-look').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-first-day-look" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-first-day-look').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
					this.hide();
					break;
					default:
					this.request();
					break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			};

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			};

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			};

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			};

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			};

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	};
})(window.jQuery);

$(document).ready(function() {
	$('#carousel-principal').owlCarousel({
		loop: true,
		navigation : false,
		slideSpeed : 3000,
		items: 1,
		pagination: true,
		autoPlay: true,
	});
	
	$('#carousel-principal-mobile').owlCarousel({
		loop: true,
		navigation : false,
		slideSpeed : 3000,
		items: 1,
		pagination: true,
		autoPlay: true,
	});

	function getCookie(c_name) {
		var c_value = document.cookie;
		var c_start = c_value.indexOf(' ' + c_name + '=');
		if (c_start == -1) {
			c_start = c_value.indexOf(c_name + '=');
		}
		if (c_start == -1) {
			c_value = null;
		} else {
			c_start = c_value.indexOf('=', c_start) + 1;
			var c_end = c_value.indexOf(';', c_start);
			if (c_end == -1) {
				c_end = c_value.length;
			}
			c_value = unescape(c_value.substring(c_start, c_end));
		}
		return c_value;
	}

	function setCookie(c_name, value) {
		var c_value = escape(value);
		document.cookie = c_name + '=' + c_value;
	}

	function checkCookie() {
		// var showed = getCookie('popup-ok');
		// if (showed != null && showed != '') {
		// 	var date = showed;
		// 	var currentDate = Math.round(new Date().getTime() / 1000);

		// 	if (currentDate > date) {
		// 		return true;
		// 	}
		// 	return false;

		// }
		// return true;
		return false;
	}

	if(checkCookie()) {
		setTimeout(function() {
			document.getElementById('popup-access').click();
		}, 3000);

		var tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);
		setCookie('popup-ok', Math.round(tomorrow.getTime() / 1000));
	}
});
