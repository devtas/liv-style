<?php
class ControllerPaymentRedeRestCredito extends Controller {
    private $valorTotal = 0;

    public function index() {
        $this->load->language('payment/rede_rest_credito');

        $data['text_sandbox'] = $this->language->get('text_sandbox');
        $data['text_detalhes'] = $this->language->get('text_detalhes');
        $data['text_ate'] = $this->language->get('text_ate');
        $data['text_de'] = $this->language->get('text_de');
        $data['text_total'] = $this->language->get('text_total');
        $data['text_sem_juros'] = $this->language->get('text_sem_juros');
        $data['text_com_juros'] = $this->language->get('text_com_juros');
        $data['text_juros'] = $this->language->get('text_juros');
        $data['text_mes'] = $this->language->get('text_mes');
        $data['text_ano'] = $this->language->get('text_ano');
        $data['text_carregando'] = $this->language->get('text_carregando');
        $data['text_autorizando'] = $this->language->get('text_autorizando');
        $data['text_autorizou'] = $this->language->get('text_autorizou');

        $data['button_pagar'] = $this->language->get('button_pagar');

        $data['entry_bandeira'] = $this->language->get('entry_bandeira');
        $data['entry_cartao'] = $this->language->get('entry_cartao');
        $data['entry_titular'] = $this->language->get('entry_titular');
        $data['entry_parcelas'] = $this->language->get('entry_parcelas');
        $data['entry_validade_mes'] = $this->language->get('entry_validade_mes');
        $data['entry_validade_ano'] = $this->language->get('entry_validade_ano');
        $data['entry_codigo'] = $this->language->get('entry_codigo');
        $data['entry_captcha'] = $this->language->get('entry_captcha');

        $data['error_parcelas'] = $this->language->get('error_parcelas');
        $data['error_cartao'] = $this->language->get('error_cartao');
        $data['error_titular'] = $this->language->get('error_titular');
        $data['error_mes'] = $this->language->get('error_mes');
        $data['error_ano'] = $this->language->get('error_ano');
        $data['error_codigo'] = $this->language->get('error_codigo');

        $data['error_bandeiras'] = $this->language->get('error_bandeiras');
        $data['error_configuracao'] = $this->language->get('error_configuracao');

        $data['ambiente'] = $this->config->get('rede_rest_credito_ambiente');

        $data['exibir_juros'] = $this->config->get('rede_rest_credito_exibir_juros');

        $data['cor_normal_texto'] = $this->config->get('rede_rest_credito_cor_normal_texto');
        $data['cor_normal_fundo'] = $this->config->get('rede_rest_credito_cor_normal_fundo');
        $data['cor_normal_borda'] = $this->config->get('rede_rest_credito_cor_normal_borda');
        $data['cor_efeito_texto'] = $this->config->get('rede_rest_credito_cor_efeito_texto');
        $data['cor_efeito_fundo'] = $this->config->get('rede_rest_credito_cor_efeito_fundo');
        $data['cor_efeito_borda'] = $this->config->get('rede_rest_credito_cor_efeito_borda');

        $data['bandeiras'] = array();
        foreach ($this->getBandeiras() as $bandeira => $parcelamento) {
            ($this->config->get('rede_rest_credito_' . $bandeira)) ? array_push($data['bandeiras'], $bandeira) : '';
        }

        $meses = array();
        for ($i = 1; $i <= 12; $i++) {
            $meses[] = array(
                'text' => sprintf('%02d', $i),
                'value' => $i
            );
        }
        $data['meses'] = json_encode($meses);

        $anos = array();
        $hoje = getdate();
        for ($i = $hoje['year']; $i < $hoje['year'] + 12; $i++) {
            $anos[] = array(
                'text' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)),
                'value' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i))
            );
        }
        $data['anos'] = json_encode($anos);

        $data['captcha'] = $this->config->get('rede_rest_credito_recaptcha_status');
        if ($data['captcha']) {
            $data['site_key'] = $this->config->get('rede_rest_credito_recaptcha_site_key');
        }

        if (isset($this->session->data['attempts'])) {
            unset($this->session->data['attempts']);
        }
        $this->session->data['attempts'] = 3;

        include_once(DIR_SYSTEM . 'library/rede_rest/versao.php');

        if (version_compare(VERSION, '2.2') < 0) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/rede_rest_credito.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/payment/rede_rest_credito.tpl', $data);
            } else {
                return $this->load->view('default/template/payment/rede_rest_credito.tpl', $data);
            }
        } else {
            return $this->load->view('payment/rede_rest_credito', $data);
        }
    }

    public function bandeiras() {
        $json = array();

        if (isset($this->session->data['order_id']) && isset($this->session->data['attempts'])) {
            foreach ($this->getBandeiras() as $bandeira => $parcelas) {
                ($this->config->get('rede_rest_credito_' . $bandeira)) ? $json[] = array('bandeira' => $bandeira, 'titulo' => strtoupper($bandeira), 'imagem' => HTTPS_SERVER .'image/catalog/rede_rest/'. $bandeira .'.png', 'parcelas' => $parcelas) : '';
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function parcelas() {
        $json = array();

        if (isset($this->session->data['order_id']) && isset($this->session->data['attempts']) && isset($this->request->get['bandeira'])) {
            $valorMinimo = ($this->config->get('rede_rest_credito_minimo') > 0) ? $this->config->get('rede_rest_credito_minimo') : '0';

            $this->load->model('checkout/order');
            $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

            $this->valorTotal = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);

            $total = $this->valorTotal;
            $desconto = ($this->config->get('rede_rest_credito_desconto')) ? (float)$this->config->get('rede_rest_credito_desconto') : 0;

            $bandeira = $this->request->get['bandeira'];

            if (version_compare(VERSION, '2.2') < 0) {
                $currency_code = $this->currency->getCode();
            } else {
                $currency_code = $this->session->data['currency'];
            }

            if (strtoupper($currency_code) == 'BRL') {
                $parcelas = $this->config->get('rede_rest_credito_'. strtolower($bandeira) .'_parcelas');
                $sem_juros = $this->config->get('rede_rest_credito_'. strtolower($bandeira) .'_sem_juros');
                $juros = $this->config->get('rede_rest_credito_'. strtolower($bandeira) .'_juros');

                for ($i = 1; $i <= $parcelas; $i++) {
                    if ($i <= $sem_juros) {
                        if ($i == 1) {
                            if ($desconto > 0) {
                                $this->load->model('payment/rede_rest_credito');
                                $shipping = $this->model_payment_rede_rest_credito->getOrderShippingValue($this->session->data['order_id']);

                                $shipping = $this->currency->format($shipping, $order_info['currency_code'], $order_info['currency_value'], false);
                                $subtotal = $total-$shipping;
                                $desconto = ($subtotal*$desconto)/100;
                                $valorParcela = ($subtotal-$desconto)+$shipping;
                                $desconto = $this->currency->format($desconto, $order_info['currency_code'], '1.00', true);
                            } else {
                                $valorParcela = $total;
                            }

                            $json[] = array(
                                'parcela' => 1,
                                'desconto' => $desconto,
                                'valor' => $this->currency->format($valorParcela, $order_info['currency_code'], '1.00', true),
                                'juros' => 0,
                                'total' => $this->currency->format($valorParcela, $order_info['currency_code'], '1.00', true)
                            );
                        } else {
                            $valorParcela = ($total/$i);
                            if ($valorParcela >= $valorMinimo) {
                                $json[] = array(
                                    'parcela' => $i,
                                    'desconto' => 0,
                                    'valor' => $this->currency->format($valorParcela, $order_info['currency_code'], '1.00', true),
                                    'juros' => 0,
                                    'total' => $this->currency->format($total, $order_info['currency_code'], '1.00', true)
                                );
                            }
                        }
                    } else {
                        $total = $this->getParcelar($bandeira, $i);
                        if ($total['valorParcela'] >= $valorMinimo) {
                            $json[] = array(
                                'parcela' => $i,
                                'desconto' => 0,
                                'valor' => $this->currency->format($total['valorParcela'], $order_info['currency_code'], '1.00', true),
                                'juros' => $juros,
                                'total' => $this->currency->format($total['valorTotal'], $order_info['currency_code'], '1.00', true)
                            );
                        }
                    }
                }
            } else {
                if ($desconto > 0) {
                    $this->load->model('payment/rede_rest_credito');
                    $shipping = $this->model_payment_rede_rest_credito->getOrderShippingValue($this->session->data['order_id']);

                    $shipping = $this->currency->format($shipping, $order_info['currency_code'], $order_info['currency_value'], false);
                    $total = $total-$shipping;
                    $desconto = ($total*$desconto)/100;
                    $valorParcela = ($total-$desconto)+$shipping;
                    $desconto = $this->currency->format($desconto, $order_info['currency_code'], '1.00', true);
                } else {
                    $valorParcela = $total;
                }

                $json[] = array(
                    'parcela' => 1,
                    'desconto' => $desconto,
                    'valor' => $this->currency->format($valorParcela, $order_info['currency_code'], '1.00', true),
                    'juros' => 0,
                    'total' => $this->currency->format($valorParcela, $order_info['currency_code'], '1.00', true)
                );
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function comprovante() {
        if (isset($this->session->data['comprovante'])) {
            if (isset($this->session->data['order_id'])) {
                $this->cart->clear();

                $this->load->model('account/activity');

                $this->load->model('shipping/correiosliv');
                if (isset($this->session->data['order_id']) && isset($this->session->data['shipping_method'])) {
                    $vipp = $this->model_shipping_correiosliv->setData($this->session->data['order_id'], $this->session->data['shipping_method']);
                    $data['plp'] = $vipp;
                }

                if ($this->customer->isLogged()) {
                    $activity_data = array(
                        'customer_id' => $this->customer->getId(),
                        'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
                        'order_id' => $this->session->data['order_id']
                    );

                    $this->model_account_activity->addActivity('order_account', $activity_data);
                } else {
                    $activity_data = array(
                        'name' => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
                        'order_id' => $this->session->data['order_id']
                    );

                    $this->model_account_activity->addActivity('order_guest', $activity_data);
                }

                /// Items Área
                $data['items'] = array();
                $this->load->model('account/order');
                $items = $this->model_account_order->getOrderProducts($this->session->data['order_id']);

                foreach ($items as $key => $item) {
                    $this->load->model('catalog/product');
                    $productInfos = $this->model_catalog_product->getProduct($item['product_id']);
                    $productInfos['images'] = $this->model_catalog_product->getProductImages($item['product_id']);

                    $productInfos['price'] = $item['price'];
                    $productInfos['price_format'] = 'R$ ' . number_format($item['price'], 2, ',', '.');

                    $productInfos['quantity'] = $item['quantity'];

                    $productInfos['total'] = $item['total'];
                    $productInfos['total_format']= 'R$ ' . number_format($item['total'], 2, ',', '.');

                    $productInfos['description'] = '';

                    $data['items'][] = $productInfos;
                }

                $data['order_totals'] = $this->model_account_order->getOrderTotals($this->session->data['order_id']);
                $data['order_totals'] = $data['order_totals'][0];
                $data['order_totals']['value_format'] = 'R$ ' . number_format($data['order_totals']['value'], 2, ',', '.');

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);
                unset($this->session->data['guest']);
                unset($this->session->data['comment']);
                unset($this->session->data['order_id']);
                unset($this->session->data['coupon']);
                unset($this->session->data['reward']);
                unset($this->session->data['voucher']);
                unset($this->session->data['vouchers']);
                unset($this->session->data['totals']);
            }

            $this->load->language('payment/rede_rest_comprovante');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_basket'),
                'href' => $this->url->link('checkout/cart')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_checkout'),
                'href' => $this->url->link('checkout/checkout', '', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_success'),
                'href' => $this->url->link('payment/rede_rest_credito/comprovante')
            );

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_importante'] = $this->language->get('text_importante');
            $data['items_title'] = $this->language->get('text_itens_area');
            $data['text_comprovante'] = $this->session->data['comprovante'];

            $data['button_imprimir'] = $this->language->get('button_imprimir');

            $data['print'] = $this->url->link('payment/rede_rest_credito/imprimir');
            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (version_compare(VERSION, '2.2') < 0) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/rede_rest_comprovante.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/rede_rest_comprovante.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/payment/rede_rest_comprovante.tpl', $data));
                }
            } else {
                $this->response->setOutput($this->load->view('payment/rede_rest_comprovante', $data));
            }
        } else {
            $this->response->redirect($this->url->link('error/not_found'));
        }
    }

    public function imprimir() {
        if (isset($this->session->data['comprovante'])) {
            $this->load->language('payment/rede_rest_imprimir');

            $this->document->setTitle($this->config->get('config_name') . ' - ' . $this->language->get('text_title'));

            if ($this->request->server['HTTPS']) {
                $server = $this->config->get('config_ssl');
            } else {
                $server = $this->config->get('config_url');
            }

            if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
                $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
            }

            $data['title'] = $this->document->getTitle();

            $data['links'] = $this->document->getLinks();
            $data['lang'] = $this->language->get('code');
            $data['direction'] = $this->language->get('direction');
            $data['name'] = $this->config->get('config_name');

            $data['text_title'] = $this->language->get('text_title');
            $data['text_comprovante'] = $this->session->data['comprovante'];

            if (version_compare(VERSION, '2.2') < 0) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/rede_rest_imprimir.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/payment/rede_rest_imprimir.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/payment/rede_rest_imprimir.tpl', $data));
                }
            } else {
                $this->response->setOutput($this->load->view('payment/rede_rest_imprimir', $data));
            }
        } else {
            $this->response->redirect($this->url->link('error/not_found'));
        }
    }

    private function getBandeiras() {
        $bandeiras = array(
            "visa" => $this->config->get('rede_rest_credito_visa_parcelas'),
            "mastercard" => $this->config->get('rede_rest_credito_mastercard_parcelas'),
            "elo" => $this->config->get('rede_rest_credito_elo_parcelas'),
            "amex" => $this->config->get('rede_rest_credito_amex_parcelas'),
            "diners" => $this->config->get('rede_rest_credito_diners_parcelas'),
            "hipercard" => $this->config->get('rede_rest_credito_hipercard_parcelas'),
            "hiper" => $this->config->get('rede_rest_credito_hiper_parcelas'),
            "jcb" => $this->config->get('rede_rest_credito_jcb_parcelas'),
            "credz" => $this->config->get('rede_rest_credito_credz_parcelas')
        );

        return $bandeiras;
    }

    private function getParcelar($bandeira, $parcelas) {
        if (version_compare(VERSION, '2.2') < 0) {
            $currency_code = $this->currency->getCode();
        } else {
            $currency_code = $this->session->data['currency'];
        }

        if (strtoupper($currency_code) == 'BRL') {
            $parcelar = $this->config->get('rede_rest_credito_'. strtolower($bandeira) .'_parcelas');
            $juros = $this->config->get('rede_rest_credito_'. strtolower($bandeira) .'_juros')/100;
            $calculo  = $this->config->get('rede_rest_credito_calculo');

            if ($parcelas > $parcelar) {
                $parcelas = $parcelar;
            }

            if ($calculo) {
                $valorParcela = ($this->valorTotal*$juros)/(1-(1/pow(1+$juros, $parcelas)));
            } else {
                $valorParcela = ($this->valorTotal*pow(1+$juros, $parcelas))/$parcelas;
            }

            $valorParcela = round($valorParcela, 2);
            $valorTotal = $parcelas * $valorParcela;
        } else {
            $valorParcela = $this->valorTotal;
            $valorTotal = $this->valorTotal;
        }

        return array(
            'valorParcela' => $valorParcela,
            'valorTotal' => $valorTotal
        );
    }

    public function setTransacao() {
        $json = array();

        $this->language->load('payment/rede_rest_credito');

        if (isset($this->session->data['order_id']) && isset($this->session->data['attempts']) && $this->setValidarPost()) {

            if ($this->setValidarCaptcha()) {
                if (($this->session->data['attempts'] > 0) && ($this->session->data['attempts'] <= 3)) {
                    $bandeiraCartao = trim($this->request->post['bandeira']);
                    $parcelasCartao = preg_replace("/[^0-9]/", '', $this->request->post['parcelas']);
                    $titularCartao = trim($this->request->post['titular']);
                    $numeroCartao = preg_replace("/[^0-9]/", '', $this->request->post['cartao']);
                    $validadeMes = preg_replace("/[^0-9]/", '', $this->request->post['mes']);
                    $validadeAno = preg_replace("/[^0-9]/", '', $this->request->post['ano']);
                    $codigoCartao = preg_replace("/[^0-9]/", '', $this->request->post['codigo']);

                    $campos = array($bandeiraCartao, $parcelasCartao, $titularCartao, $numeroCartao, $validadeMes, $validadeAno, $codigoCartao);
                    $bandeiras = $this->getBandeiras();

                    if ($this->setValidarCampos($campos) && array_key_exists(strtolower($bandeiraCartao), $bandeiras) && ($parcelasCartao <= '12')) {
                        $this->session->data['attempts']--;
                        $numPedido = $this->session->data['order_id'];

                        $this->load->model('checkout/order');
                        $order_info = $this->model_checkout_order->getOrder($numPedido);

                        $this->valorTotal = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
                        $desconto = ($this->config->get('rede_rest_credito_desconto')) ? (float)$this->config->get('rede_rest_credito_desconto') : 0;
                        if ($parcelasCartao <= '1') {
                            if ($desconto > 0) {
                                $this->load->model('payment/rede_rest_credito');
                                $shipping = $this->model_payment_rede_rest_credito->getOrderShippingValue($this->session->data['order_id']);

                                $shipping = $this->currency->format($shipping, $order_info['currency_code'], $order_info['currency_value'], false);
                                $total = $this->valorTotal;
                                $total = $total-$shipping;
                                $desconto = ($total*$desconto)/100;
                                $total = ($total-$desconto)+$shipping;
                            } else {
                                $total = $this->valorTotal;
                            }
                        } else {
                            $sem_juros = $this->config->get('rede_rest_credito_'. strtolower($bandeiraCartao) .'_sem_juros');
                            if ($parcelasCartao <= $sem_juros) {
                                $total = $this->valorTotal;
                            } else {
                                $resultado = $this->getParcelar($bandeiraCartao, $parcelasCartao);
                                $total = $resultado['valorTotal'];
                            }
                        }
                        $total = number_format($total, 2, '', '');

                        $dados['reference'] = $numPedido;
                        $dados['amount'] = $total;
                        $dados['cardholderName'] = $titularCartao;
                        $dados['installments'] = $parcelasCartao;
                        $dados['cardNumber'] = $numeroCartao;
                        $dados['expirationMonth'] = $validadeMes;
                        $dados['expirationYear'] = $validadeAno;
                        $dados['securityCode'] = $codigoCartao;

                        $dados['capture'] = ($this->config->get('rede_rest_credito_captura')) ? 'true' : 'false';
                        $dados['softDescriptor'] = $this->config->get('rede_rest_credito_soft_descriptor');

                        $dados['chave'] = $this->config->get('rede_rest_credito_chave');
                        $dados['sandbox'] = $this->config->get('rede_rest_credito_ambiente');
                        $dados['debug'] = $this->config->get('rede_rest_credito_debug');
                        $dados['filiacao'] = $this->config->get('rede_rest_credito_filiacao');
                        $dados['token'] = $this->config->get('rede_rest_credito_token');

                        require_once(DIR_SYSTEM . 'library/rede_rest/debug.php');
                        require_once(DIR_SYSTEM . 'library/rede_rest/rede.php');

                        $rede = new Rede();
                        $rede->setParametros($dados);

                        $this->log->write($dados);
                        $resposta = $rede->setTransactionCredit();
                        $this->log->write($resposta);

                        if ($resposta) {
                            if (isset($resposta->returnCode)) {
                                $returnCode = $resposta->returnCode;

                                if (isset($resposta->reference) && !empty($resposta->reference)) {
                                    $transacaoData = date('d/m/Y H:i', strtotime(substr($resposta->dateTime, 0, -6)));
                                    $transacaoTid = $resposta->tid;
                                    $transacaoNsu = $resposta->nsu;
                                    $authorizationCode = $resposta->authorizationCode;
                                    $transacaoParcelas = $parcelasCartao;
                                    $transacaoBandeira = strtoupper($bandeiraCartao);
                                    $transacaoValor = $this->currency->format(($resposta->amount / 100), $order_info['currency_code'], '1.00', true);
                                    $transacaoStatus = ($this->config->get('rede_rest_credito_captura')) ? $this->language->get('text_capturado') : $this->language->get('text_autorizado');

                                    if ($returnCode == "00") {
                                        $comment = $this->language->get('entry_pedido') . $numPedido . "\n";
                                        $comment .= $this->language->get('entry_data') . $transacaoData . ' ' . $this->language->get('text_fuso_horario') . "\n";
                                        $comment .= $this->language->get('entry_tid') . $transacaoTid . "\n";
                                        $comment .= $this->language->get('entry_nsu') . $transacaoNsu . "\n";
                                        $comment .= $this->language->get('entry_tipo') . $this->language->get('text_cartao_credito') . "\n";
                                        $comment .= $this->language->get('entry_bandeira') . strtoupper($transacaoBandeira) . "\n";
                                        $comment .= $this->language->get('entry_parcelas') . $transacaoParcelas . 'x ' . $this->language->get('text_total') . $transacaoValor . "\n";
                                        $comment .= $this->language->get('entry_status') . $transacaoStatus;

                                        $campos = array(
                                            'order_id' => $numPedido,
                                            'returnCode' => $returnCode,
                                            'authorizationCode' => $authorizationCode,
                                            'tid' => $transacaoTid,
                                            'nsu' => $transacaoNsu,
                                            'tipo' => 'credito',
                                            'status' => ($this->config->get('rede_rest_credito_captura')) ? 'capturada' : 'autorizada',
                                            'parcelas' => $transacaoParcelas,
                                            'bandeira' => $transacaoBandeira,
                                            'autorizacaoData' => $resposta->dateTime,
                                            'autorizacaoValor' => ($resposta->amount / 100),
                                            'capturaData' => ($this->config->get('rede_rest_credito_captura')) ? $resposta->dateTime : '',
                                            'capturaValor' => ($this->config->get('rede_rest_credito_captura')) ? ($resposta->amount / 100) : '',
                                            'json' => json_encode($resposta)
                                        );

                                        $this->load->model('payment/rede_rest_credito');
                                        $this->model_payment_rede_rest_credito->addTransaction($campos);

                                        $situacao = ($this->config->get('rede_rest_credito_captura')) ? $this->config->get('rede_rest_credito_situacao_capturada_id') : $this->config->get('rede_rest_credito_situacao_autorizada_id');

                                        $this->load->model('checkout/order');
                                        $this->model_checkout_order->addOrderHistory($numPedido, $situacao, $comment, true);

                                        if(isset($this->session->data['comprovante'])) {
                                            unset($this->session->data['comprovante']);
                                        }
                                        $this->session->data['comprovante'] = $comment;

                                        $json['redirect'] = $this->url->link('payment/rede_rest_credito/comprovante', '', true);

                                    } else {
                                        $comment = $this->language->get('entry_pedido') . $numPedido . "\n";
                                        $comment .= $this->language->get('entry_data') . $transacaoData . "\n";
                                        $comment .= $this->language->get('entry_tid') . $transacaoTid . "\n";
                                        if ($transacaoNsu) {
                                            $comment .= $this->language->get('entry_nsu') . $transacaoNsu . "\n";
                                        }
                                        $comment .= $this->language->get('entry_tipo') . $this->language->get('text_cartao_credito') . "\n";
                                        $comment .= $this->language->get('entry_bandeira') . strtoupper($transacaoBandeira) . "\n";
                                        $comment .= $this->language->get('entry_parcelas') . $transacaoParcelas . 'x ' . $this->language->get('text_total') . $transacaoValor . "\n";
                                        $comment .= $this->language->get('entry_status') . $this->language->get('text_nao_autorizado');

                                        $this->load->model('checkout/order');
                                        $this->model_checkout_order->addOrderHistory($numPedido, $this->config->get('rede_rest_credito_situacao_nao_autorizada_id'), $comment, true);

                                        $json['error'] = $this->language->get('error_autorizacao');
                                    }
                                } else {
                                    switch($returnCode) {
                                        case '24': case '25': case '26': case '31': case '32': case '51': case '53': case '54': case '57': case '63': case '65': case '69': case '88': case '89': case '151':
                                            $json['error'] = $this->language->get('error_configuracao');
                                            break;
                                        default:
                                            $json['error'] = $this->language->get('error_autorizacao');
                                            break;
                                    }
                                }
                            } else {
                                $json['error'] = $this->language->get('error_status');
                            }
                        } else {
                            $json['error'] = $this->language->get('error_configuracao');
                        }
                    } else {
                        $json['error'] = $this->language->get('error_preenchimento');
                    }
                } else {
                    if ($this->session->data['attempts'] == 0) {
                        $this->session->data['attempts']--;

                        $this->load->model('checkout/order');
                        $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('rede_rest_credito_situacao_nao_autorizada_id'), $this->language->get('text_tentativas'), true);
                    }

                    $json['error'] = $this->language->get('error_tentativas');
                }
            } else {
                if (($this->session->data['attempts'] < 0) || ($this->session->data['attempts'] > 3)) {
                    $json['error'] = $this->language->get('error_tentativas');
                } else {
                    $json['error'] = $this->language->get('error_captcha');
                }
            }
        } else {
            $json['error'] = $this->language->get('error_permissao');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function setValidarPost() {
        $campos = array('bandeira', 'parcelas', 'titular', 'cartao', 'mes', 'ano', 'codigo');

        $erros = 0;
        foreach ($campos as $campo) {
            if (!isset($this->request->post[$campo])) {
                $erros++;
                break;
            }
        }

        if ($erros == 0) {
            return true;
        } else {
            return false;
        }
    }

    private function setValidarCampos($campos) {
        $erros = 0;

        foreach ($campos as $campo) {
            if (empty($campo)) {
                $erros++;
                break;
            }
        }

        if ($erros == 0) {
            return true;
        } else {
            return false;
        }
    }

    private function setValidarCaptcha() {
        if (!$this->config->get('rede_rest_credito_recaptcha_status')) {
            return true;
        }

        if (!isset($this->session->data['attempts'])) {
            return false;
        }

        if (($this->session->data['attempts'] < 0) || ($this->session->data['attempts'] > 3)) {
            return false;
        }

        if (!isset($this->request->post['g-recaptcha-response'])) {
            return false;
        }

        if (empty($this->request->post['g-recaptcha-response'])) {
            return false;
        }

        $recaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($this->config->get('rede_rest_credito_recaptcha_secret_key')) . '&response=' . $this->request->post['g-recaptcha-response'] . '&remoteip=' . $this->request->server['REMOTE_ADDR']);
        $recaptcha = json_decode($recaptcha, true);

        if ($recaptcha['success']) {
            return true;
        } else {
            return false;
        }
    }
}