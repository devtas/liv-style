<?php

class ControllerCheckoutSuccess extends Controller {
	private $sigepConfig;
	private $sigepParams;
	private $sigepDv;
	private $sigepPlp;
	private $accessData;

	private $addressNome;
	private $addressLogradouro;
	private $addressNumero;
	private $addressComplemento;
	private $addressBairro;
	private $addressCep;
	private $addressCidade;
	private $addressUf;

	private $orderTotal;

	public function index() {
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {

			$this->cart->clear();

			// Add to activity log
			$this->load->model('account/activity');

			// Gera Etiqueta Correios Liv
			// $this->load->model('shipping/correiosliv');
			// $etiqueta = $this->model_shipping_correiosliv->fechaPlp($this->session->data['order_id'], $this->session->data['shipping_method']);

			$data['plp'] = null;

			// echo "<pre><br /><br /><br /><br /><br /><br /><br />";

			$this->load->model('shipping/correiosliv');
			if (isset($this->session->data['order_id']) && isset($this->session->data['shipping_method'])) {
				$vipp = $this->model_shipping_correiosliv->setData($this->session->data['order_id'], $this->session->data['shipping_method']);
				// var_dump($vipp);
				$data['plp'] = $vipp;
				// die();
			}

			if ($this->customer->isLogged()) {
				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
					'order_id'    => $this->session->data['order_id']
				);

				$this->model_account_activity->addActivity('order_account', $activity_data);
			} else {
				$activity_data = array(
					'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
					'order_id' => $this->session->data['order_id']
				);

				$this->model_account_activity->addActivity('order_guest', $activity_data);
			}

			$data['items'] = array();
			$this->load->model('account/order');
			$items = $this->model_account_order->getOrderProducts($this->session->data['order_id']);

			foreach ($items as $key => $item) {
				$this->load->model('catalog/product');
				$productInfos = $this->model_catalog_product->getProduct($item['product_id']);
				$productInfos['images'] = $this->model_catalog_product->getProductImages($item['product_id']);

				$productInfos['price'] = $item['price'];
				$productInfos['price_format'] = 'R$ ' . number_format($item['price'], 2, ',', '.');

				$productInfos['quantity'] = $item['quantity'];

				$productInfos['total'] = $item['total'];
				$productInfos['total_format']= 'R$ ' . number_format($item['total'], 2, ',', '.');

				$productInfos['description'] = '';

				$data['items'][] = $productInfos;
			}

			$data['order_totals'] = $this->model_account_order->getOrderTotals($this->session->data['order_id']);
			$data['order_totals'] = $data['order_totals'][0];
			$data['order_totals']['value_format'] = 'R$ ' . number_format($data['order_totals']['value'], 2, ',', '.');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		$data['items_title'] = $this->language->get('text_itens_area');

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/download', '', 'SSL'), $this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}
}