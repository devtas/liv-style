<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// ini_set('error_reporting', 'E_ALL|E_STRICT');
// error_reporting(E_ALL);

class ModelShippingCorreiosliv extends Model {
	private $sigepDv;
	private $sigepPlp;
	private $accessData;

	private $addressNome;
	private $addressLogradouro;
	private $addressNumero;
	private $addressComplemento;
	private $addressBairro;
	private $addressCep;
	private $addressCidade;
	private $addressUf;

	private $orderTotal;
	
	public function setData()
	{
		$accessData = new \PhpSigep\Model\AccessData();
		$accessData->setUsuario('20852860000241');
		$accessData->setSenha('lv0i05');
		$accessData->setCodAdministrativo('18295347');
		$accessData->setNumeroContrato('9912448157');
		$accessData->setCartaoPostagem('74377710');
		$accessData->setDiretoria(72);
		$accessData->setCnpjEmpresa('20852860000241');
		$this->accessData = $accessData;
	}

	public function setConfig()
	{
		$this->setData();

		$config = new \PhpSigep\Config();
		$config->setAccessData($this->accessData);
		$config->setEnv(\PhpSigep\Config::ENV_PRODUCTION);
		$config->setCacheOptions(
		    array(
		        'storageOptions' => array(
		            // Qualquer valor setado neste atributo será mesclado ao atributos das classes 
		            // "\PhpSigep\Cache\Storage\Adapter\AdapterOptions" e "\PhpSigep\Cache\Storage\Adapter\FileSystemOptions".
		            // Por tanto as chaves devem ser o nome de um dos atributos dessas classes.
		            'enabled' => false,
		            'ttl' => 10,// "time to live" de 10 segundos
		            'cacheDir' => sys_get_temp_dir(), // Opcional. Quando não inforado é usado o valor retornado de "sys_get_temp_dir()"
		        ),
		    )
		);

		\PhpSigep\Bootstrap::start($config);
	}

	public function fechaPlp($order_id, $shipping_method)
	{
		require_once(DIR_VENDOR . '/autoload.php');

		// Retrieving Data
		$s_method_code = $shipping_method['code'];
		$s_method_code_number = explode('.', $s_method_code);

		if ($s_method_code_number[0] == "correios") {
			$this->shippingMethodCodeNumber = ($s_method_code_number[1] == "40096") ? "04162" : $s_method_code_number[1];

			$s_method_title = $shipping_method['title'];
			$s_method_cost = $shipping_method['cost'];
			$s_method_cost_text = $shipping_method['text'];
			$s_method_tax_class_id = $shipping_method['tax_class_id'];

			// Order Model in view
			$this->load->model('checkout/order');
			$orderDetails = $this->model_checkout_order->getOrder($order_id);

			$this->orderTotal = $orderDetails['total'];
			$this->addressNome = $orderDetails['shipping_firstname'] . ' ' . $orderDetails['shipping_lastname'];
			$this->addressLogradouro = $orderDetails['shipping_address_1'];
			$this->addressNumero = $orderDetails['shipping_company'];
			// $this->addressComplemento = $orderDetails['shipping_company'];
			$this->addressComplemento = '';
			$this->addressBairro = $orderDetails['shipping_address_2'];
			$this->addressCep = $orderDetails['shipping_postcode'];
			$this->addressCidade = $orderDetails['shipping_city'];
			$this->addressUf = $orderDetails['shipping_zone_code'];

			$this->setConfig();
			$this->preListaServicos($order_id);

			$phpSigepReal = new PhpSigep\Services\SoapClient\Real();
			if ($this->sigepPlp != null) {
				$result = $phpSigepReal->fechaPlpVariosServicos($this->sigepPlp);
				$this->db->query("
					CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_correios_liv` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `order_id` int(11) NOT NULL,
					  `method_code` text NOT NULL,
					  `method_label` text NOT NULL,
					  `etiqueta` text NOT NULL,
					  `plp_id` text NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
				");

				$plpID = $result->getResult()->getIdPlp();

				$this->db->query("INSERT INTO `" . DB_PREFIX."order_correios_liv` SET
					`order_id` = '" . (int)$this->db->escape($order_id) . "',
					`method_code` = '" . $this->shippingMethodCodeNumber . "',
					`method_label` = '" . $this->shippingMethodCodeText . "',
					`etiqueta` = '" . $this->sigepDv . "',
					`plp_id` = '" . $plpID . "'");

				return $this->sigepDv;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public function preListaServicos($order_id)
	{
		$this->load->model('account/order');
		$products = $this->model_account_order->getOrderProducts($order_id);

		$peso = 0;
		$comprimento = 0;
		$largura = 0;
		$altura = 0;
		$diametro = 0;

		foreach ($products as $key => $product) {
			$this->load->model('catalog/product');
			$productInfos = $this->model_catalog_product->getProduct($product['product_id']);

			$peso = $peso + intval($productInfos['weight']);
			$comprimento = $comprimento + intval($productInfos['length']);
			$largura = $largura + intval($productInfos['width']);
			$altura = $altura + intval($productInfos['height']);
			// $diametro 		+= $productInfos['height'];
		}

		$peso = ($peso > 0) ? $peso : 0.500;
		$comprimento = ($comprimento > 0) ? $comprimento : 20;
		$largura = ($largura > 0) ? $largura : 20;
		$altura = ($altura > 0) ? $altura : 20;
		$diametro = ($diametro > 0) ? $diametro : 20;

	    $dimensao = new \PhpSigep\Model\Dimensao();
	    $dimensao->setAltura($altura);
	    $dimensao->setLargura($largura);
	    $dimensao->setComprimento($comprimento);
	    $dimensao->setDiametro($diametro);
	    $dimensao->setTipo(\PhpSigep\Model\Dimensao::TIPO_PACOTE_CAIXA);

	    $destinatario = new \PhpSigep\Model\Destinatario();
	    $destinatario->setNome($this->addressNome);
	    $destinatario->setLogradouro($this->addressLogradouro);
	    $destinatario->setNumero($this->addressNumero);
	    $destinatario->setComplemento($this->addressComplemento);

	    $destino = new \PhpSigep\Model\DestinoNacional();
	    $destino->setBairro($this->addressBairro);
	    $destino->setCep($this->addressCep);
	    $destino->setCidade($this->addressCidade);
	    $destino->setUf($this->addressUf);

	    // Estamos criando uma etique falsa, mas em um ambiente real voçê deve usar o método
	    // {@link \PhpSigep\Services\SoapClient\Real::solicitaEtiquetas() } para gerar o número das etiquetas

	    $etiqueta = new \PhpSigep\Model\Etiqueta();
	    $this->solicitaEtiqueta();

	    if (!is_string($this->sigepDv)) {
			$this->sigepPlp = null;
			return;
	    }
	    $etiqueta->setEtiquetaComDv($this->sigepDv);

	    $servicoAdicional = new \PhpSigep\Model\ServicoAdicional();
	    $servicoAdicional->setCodigoServicoAdicional(\PhpSigep\Model\ServicoAdicional::SERVICE_REGISTRO);
	    // Se não tiver valor declarado informar 0 (zero)
	    $servicoAdicional->setValorDeclarado(0);

	    $encomenda = new \PhpSigep\Model\ObjetoPostal();
	    $encomenda->setServicosAdicionais(array($servicoAdicional));
	    $encomenda->setDestinatario($destinatario);
	    $encomenda->setDestino($destino);
	    $encomenda->setDimensao($dimensao);
	    $encomenda->setEtiqueta($etiqueta);
	    $encomenda->setPeso($peso);

	    $encomenda->setServicoDePostagem(new \PhpSigep\Model\ServicoDePostagem($this->shippingMethodCodeNumber));
	    //$encomenda->setServicoDePostagem(new \PhpSigep\Model\ServicoDePostagem(\PhpSigep\Model\ServicoDePostagem::SERVICE_SEDEX_CONTRATO_AGENCIA));
		// ***  FIM DOS DADOS DA ENCOMENDA QUE SERÁ DESPACHADA *** //

		// *** DADOS DO REMETENTE *** //
	    $remetente = new \PhpSigep\Model\Remetente();
	    $remetente->setNome('LIV Style');
	    $remetente->setLogradouro('Rua Correia de Melo');
	    $remetente->setNumero('84');
	    $remetente->setComplemento('Sala 206');
	    $remetente->setBairro('Bom Retiro');
	    $remetente->setCep('01123-020');
	    $remetente->setUf('SP');
	    $remetente->setCidade('São Paulo');
		// *** FIM DOS DADOS DO REMETENTE *** //

		$plp = new \PhpSigep\Model\PreListaDePostagem();
		$plp->setAccessData($this->accessData);
		$plp->setEncomendas(array($encomenda));
		$plp->setRemetente($remetente);

		$this->sigepPlp = $plp;
	}


	public function findConstantsFromObject($object, $filter = null, $find_value = null)
	{
	    $reflect = new ReflectionClass($object);
	    $constants = $reflect->getConstants();
	    
	    foreach ($constants as $name => $value)
	    {
	        if (!is_null($filter) && !preg_match($filter, $name))
	        {
	            unset($constants[$name]);
	            continue;
	        }
	        
	        if (!is_null($find_value) && $value != $find_value)
	        {
	            unset($constants[$name]);
	            continue;
	        }
	    }
	    
	    return $constants;
	}


	public function solicitaEtiqueta()
	{
		$params = new \PhpSigep\Model\SolicitaEtiquetas();
		$params->setQtdEtiquetas(1);
		$params->setAccessData($this->accessData);
	    $params->setServicoDePostagem($this->shippingMethodCodeNumber);
		// $params->setServicoDePostagem(\PhpSigep\Model\ServicoDePostagem::SERVICE_SEDEX_40096);

	    $cText = $this->findConstantsFromObject('\PhpSigep\Model\ServicoDePostagem', '/^SERVICE_/', $this->shippingMethodCodeNumber);
	    $this->shippingMethodCodeText = key($cText);

		$phpSigep = new PhpSigep\Services\SoapClient\Real();
		$solicitacao = $phpSigep->solicitaEtiquetas($params);
        
        // echo "<pre><br><br><br><br><br>";
        // print_r($solicitacao);
        // echo "</pre>";

		if ($solicitacao->hasError()) {
			$return = $solicitacao;
		} else {
			$solicitacaoEtiqueta = $solicitacao->getResult();
			$return = $solicitacaoEtiqueta[0]->getEtiquetaComDv();
		}

		$this->sigepDv = $return;

	}
}
?>
