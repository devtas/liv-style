<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// ini_set('error_reporting', 'E_ALL|E_STRICT');
// error_reporting(E_ALL);

class ModelShippingCorreiosliv extends Model {
	protected $client;
	protected $params;

	protected $order_id;
	protected $shipping_method;

	// <Servico>
	public $ServicoECT;
	public $ServicoText;

	// <PerfilVipp>
	public $Usuario;
	public $Token;
	public $IdPerfil;

	// <ContratoEct>
	public $NrContrato;
	public $CodigoAdministrativo;
	public $NrCartao;

	// <Destinatario>
	public $CnpjCpf;
	public $IeRg;
	public $Nome;
    public $SegundaLinhaDestinatario;
	public $Endereco;
	public $Numero;
	public $Complemento;
	public $Bairro;
	public $Cidade;
	public $UF;
	public $Cep;
	public $Telefone;
	public $TelefoneSMS;
	public $Email;

	// <NotasFiscais/NotaFiscal>
    public $NotasFiscais = array();

	// <Volumes/VolumeObjeto>
    public $Volumes = array();

    // <ItemConteudo>
    public $ItemConteudo = array();

    public function setData($order_id, $shipping_method)
    {
    	$this->order_id = $order_id;
    	$this->shipping_method = $shipping_method;
        $this->client = new \SoapClient('http://vpsrv.visualset.com.br/?wsdl', array('trace' => true));

		$this->setUsuario("livstyle");
		$this->setToken("livstyle2019");
		$this->setIdPerfil("6483");

        $this->getEtiqueta();
    }

    public function getEtiqueta() {
		// Retrieving Data
		$s_method_code = $this->shipping_method['code'];
		$s_method_code_number = explode('.', $s_method_code);

		if ($s_method_code_number[0] == "correios") {
			$this->ServicoECT = ($s_method_code_number[1] == "40096") ? "4162" : "4669";
			$this->ServicoText = $this->shipping_method['title'];

			// Order Model in view
			$this->load->model('checkout/order');
			$orderDetails = $this->model_checkout_order->getOrder($this->order_id);

			$this->orderTotal = $orderDetails['total'];
			$this->CnpjCpf = preg_replace("/[^0-9]/", "", $orderDetails['fax']);
			$this->Nome = $orderDetails['shipping_firstname'] . ' ' . $orderDetails['shipping_lastname'];
			$this->Endereco = $orderDetails['shipping_address_1'];
			$this->Numero = $orderDetails['shipping_company'];
			$this->Complemento = $orderDetails['shipping_company'];
			$this->Bairro = $orderDetails['shipping_address_2'];
			$this->Cep = str_replace('-', '', $orderDetails['shipping_postcode']);
			$this->Cidade = $orderDetails['shipping_city'];
			$this->UF = $orderDetails['shipping_zone_code'];
			$this->Telefone = $orderDetails['telephone'];
			$this->TelefoneSMS = $orderDetails['telephone'];
			$this->Email = $orderDetails['email'];

			$this->insereConteudos();
		} else {
			return null;
		}
    }

	public function insereConteudos()
	{
		$this->load->model('account/order');
		$products = $this->model_account_order->getOrderProducts($this->order_id);

		// Conteudos
		$peso = 0;
		$comprimento = 0;
		$largura = 0;
		$altura = 0;
		$diametro = 0;

		foreach ($products as $key => $product) {
			$produto = array(
				'DescricaoConteudo' => $product['name'],
				'Quantidade' => $product['quantity'],
				'Valor' => $product['total']
			);

			$this->setConteudos($produto);

			$this->load->model('catalog/product');
			$productInfos = $this->model_catalog_product->getProduct($product['product_id']);

			$peso = $peso + intval($productInfos['weight']);
			$comprimento = $comprimento + intval($productInfos['length']);
			$largura = $largura + intval($productInfos['width']);
			$altura = $altura + intval($productInfos['height']);
			// $diametro 		+= $productInfos['height'];

			$peso = ($peso > 0) ? $peso : 0.500;
			$comprimento = ($comprimento > 0) ? $comprimento : 20;
			$largura = ($largura > 0) ? $largura : 20;
			$altura = ($altura > 0) ? $altura : 20;
			$diametro = ($diametro > 0) ? $diametro : 20;

			// Volumes
			$volume = array(
				'VolumeObjeto' => array(
					'Peso' => $peso,
					'Altura' => $altura,
					'Largura' => $largura,
					'Comprimento' => $comprimento,
					'CodigoBarraVolume' => '987654',
					'CodigoBarraCliente' => '654321',
					'ObservacaoVisual' => 'teste obs',
					'PosicaoVolume' => '1',
					'Conteudo' => 'teste conteudo',
					'DeclaracaoConteudo' => array(
						'DocumentoRemetente' => '03998581000190',
						'DocumentoDestinatario' => '23463958000114',
						'PesoTotal' => $peso,
						'ItemConteudo' => $this->getConteudos()
					),
					'ValorDeclarado' => '',
					'AdicionaisVolume' => 'AD',
					'VlrACobrar' => '',
					'Etiqueta' => ''
				)
			);
			$this->setVolumes($volume);
		}

		$this->postaObjeto();
	}
    
    public function postaObjeto()
    {
        $params = array(
            'PostagemVipp' => array(
                'PerfilVipp' => array(
                    'Usuario' => $this->Usuario,
                    'Token' => $this->Token,
                    'IdPerfil' => $this->IdPerfil
                ),
                // 'ContratoEct' => array(
                //     'NrContrato' => $this->NrContrato,
                //     'CodigoAdministrativo' => $this->CodigoAdministrativo,
                //     'NrCartao' => $this->NrCartao
                // ),
                'Destinatario' => array(
                    'CnpjCpf' => $this->CnpjCpf,
                    // 'IeRg' => $this->IeRg,
                    'Nome' => $this->Nome,
                    // 'SegundaLinhaDestinatario' => $this->SegundaLinhaDestinatario,
                    'Endereco' => $this->Endereco,
                    'Numero' => $this->Numero,
                    'Complemento' => $this->Complemento,
                    'Bairro' => $this->Bairro,
                    'Cidade' => $this->Cidade,
                    'UF' => $this->UF,
                    'Cep' => $this->Cep,
                    // 'Telefone' => $this->Telefone,
                    'TelefoneSMS' => $this->TelefoneSMS,
                    'Email' => $this->Email
                ),
                'Servico' => array(
                    'ServicoECT' => $this->ServicoECT
                ),
                'NotasFiscais' => $this->NotasFiscais,
                'Volumes' => $this->Volumes
            )
        );

        try {
            $postar = $this->client->PostarObjeto($params);
            $result = htmlspecialchars($this->client->__getLastResponse());

            // print_r($result); echo "<br /><br />";
            // var_dump($s_method_code_number);

            $dom = new \DOMDocument();
            $dom->prevservWhiteSpace = false;
            $dom->loadXML(htmlspecialchars_decode($result), LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);

            $statusList = $dom->getElementsByTagName('StatusPostagem');
            $statusCnt = $statusList->length;

            if ($statusCnt > 0) {
                $status = ($statusList->item(0)->nodeValue == 'Valida') ? true : false;
            } else {
				return null;
            }

            if ($status) {
                $etiquetaList = $dom->getElementsByTagName('Etiqueta');
                $etiquetaCnt = $etiquetaList->length;

                if ($etiquetaCnt > 0) {
                    $etiqueta = ($etiquetaList->item(0)->nodeValue) ? $etiquetaList->item(0)->nodeValue : '';
                    $this->salvarEtiquetaToOrder($etiqueta);
                } else {
					return null;
                }

				return $etiqueta;
            } else {

                $erros = "";
                $descricaoList = $dom->getElementsByTagName('Descricao');
                $descricaoCnt = $descricaoList->length;
                if ($descricaoCnt > 0) {
                    $erros .= $descricaoList->item(0)->nodeValue;
                }

                $descricao2List = $dom->getElementsByTagName('Mensagem');
                $descricao2Cnt = $descricao2List->length;

                if ($descricao2Cnt > 0) {
                    $erros .= '; \n' . $descricao2List->item(0)->nodeValue;
                }

                $this->salvarErroEtiqueta($erros);

				return null;
            }

        } catch (SoapFault $exception) {
			return null;
        } 
    }



    public function salvarEtiquetaToOrder($etiqueta) {
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_correios_liv` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `order_id` int(11) NOT NULL,
              `method_code` text NOT NULL,
              `method_label` text NOT NULL,
              `etiqueta` text NOT NULL,
              `plp_id` text NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
        ");

        $this->db->query("INSERT INTO `" . DB_PREFIX."order_correios_liv` SET
            `order_id` = '" . (int)$this->db->escape($this->order_id) . "',
            `method_code` = '" . $this->ServicoECT . "',
            `method_label` = '" . $this->ServicoText . "',
            `etiqueta` = '" . $etiqueta . "',
            `plp_id` = '" . '0000000' . "'");
    }



    public function salvarErroEtiqueta($erros) {
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_correios_liv_error_logs` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `order_id` int(11) NOT NULL,
              `erro` text NULL,
              `data` datetime NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
        ");

        $errorDate = date();
        $this->db->query("INSERT INTO `" . DB_PREFIX."order_correios_liv_error_logs` SET
            `order_id` = '" . (int)$this->db->escape($this->order_id) . "',
            `erro` = '" . $erros . "',
            `data` = '" . $errorDate . "'");
    }

    public function setUsuario($text) {
        $this->Usuario = $text;
    }

    public function setToken($text) {
        $this->Token = $text;
    }

    public function setIdPerfil($text) {
        $this->IdPerfil = $text;
    }

    public function setNumeroContrato($text) {
        $this->NrContrato = $text;
    }

    public function setCodigoAdministrativo($text) {
        $this->CodigoAdministrativo = $text;
    }

    public function setNrCartao($text) {
        $this->NrCartao = $text;
    }

    public function setCnpjCpf($text) {
        $this->CnpjCpf = $text;
    }

    public function setIeRg($text) {
        $this->IeRg = $text;
    }

    public function setNome($text) {
        $this->Nome = $text;
    }

    public function setSegundaLinhaDestinatario($text) {
        $this->SegundaLinhaDestinatario = $text;
    }

    public function setEndereco($text) {
        $this->Endereco = $text;
    }

    public function setNumero($text) {
        $this->Numero = $text;
    }

    public function setComplemento($text) {
        $this->Complemento = $text;
    }

    public function setBairro($text) {
        $this->Bairro = $text;
    }

    public function setCidade($text) {
        $this->Cidade = $text;
    }

    public function setUF($text) {
        $this->UF = $text;
    }

    public function setCep($text) {
        $this->Cep = $text;
    }

    public function setTelefone($text) {
        $this->Telefone = $text;
    }

    public function setTelefoneSMS($text) {
        $this->TelefoneSMS = $text;
    }

    public function setEmail($text) {
        $this->Email = $text;
    }

    public function setServicoECT($text) {
        $this->ServicoECT = $text;
    }

    public function setConteudos($conteudo) {
        array_push($this->ItemConteudo, $conteudo);
    }

    public function getConteudos() {
        return $this->ItemConteudo;
    }

    public function setNotasFiscais($nota) {
        array_push($this->NotasFiscais, $nota);
    }

    public function setVolumes($volume) {
        array_push($this->Volumes, $volume);
    }
}
?>
