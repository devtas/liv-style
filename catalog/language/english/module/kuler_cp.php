<?php

// Live Search
$_['text_load_more']        = 'Ver mais...';
$_['text_no_results']       = 'Não há resultado';

// Newsletter
$_['error_the_newsletter_system_is_not_configured_please_contact_to_site_manager']      = 'O sistema de newsletter não está configurado, entre em contato com o administrador.';
$_['error_your_email_is_already_subscribed_to_list']                                    = 'Seu e-mail já está cadastrado em nossa newsletter.';

// One page checkout
$_['text_already_registered_click_here_to_login']   = 'Se você já possui uma conta, clique aqui.';

$_['text_opcheckout_shipping_method']   = 'Método de Entrega';
$_['text_opcheckout_payment_method']    = 'Método de Pagamento';
$_['text_opcheckout_confirm']           = 'Confirmação de Pedido';
$_['error_shipping_method']             = 'Selecione o Método de Entrega';
$_['error_payment_method']             = 'Selecione o Método de Pagamento';

$_['text_my_delivery_address_and_personal_details_are_the_same']    = 'Endereço de entrega é o mesmo informado acima.';
$_['text_my_payment_address_and_personal_details_are_the_same']     = 'Endereço de cobrança é o mesmo informado acima.';

$_['text_create_new_account']   = 'Cadastrar-se na loja';
$_['text_subscribe_newsletter'] = 'Deseja receber promoções, ofertas e novidades por e-mail?';

$_['text_please_select']                    = 'Selecione';
$_['text_i_want_to_use_a_new_address']      = 'Quero cadastrar outro endereço';

$_['error_shipping_address']                = 'Selecione um endereço de entrega!';
$_['error_payment_address']                 = 'Selecione um endereço de cobrança!';