<?php
$_['heading_module']        = 'Kuler Showcase';

$_['text_years']           = 'Anos';
$_['text_months']          = 'Meses';
$_['text_weeks']           = 'Sems';
$_['text_days']            = 'Dias';
$_['text_hours']           = 'Horas';
$_['text_minutes']         = 'Mins';
$_['text_seconds']         = 'Segs';
$_['text_year']            = 'Ano';
$_['text_month']           = 'Mês';
$_['text_week']            = 'Sem';
$_['text_day']             = 'Dia';
$_['text_hour']            = 'Hora';
$_['text_minute']          = 'Min';
$_['text_second']          = 'Seg';
