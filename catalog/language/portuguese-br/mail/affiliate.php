<?php
// Text
$_['text_subject']		        = 'Confirmação de Cadastro - %s';
$_['text_welcome']		        = 'Obrigado por se cadastrar no programa de parceiros da %s.';
$_['text_login']                = 'Sua conta foi cadastrada no nosso programa de parceiros, agora você pode acessá-la utilizando seu e-mail e senha através do link: %s';
$_['text_approval']             = 'Sua conta de parceiro precisa ser aprovada para que você possa acessar os dados de sua conta do programa de parceiros. Assim que sua conta for aprovada, você poderá acessar sua conta utilizando seu e-mail e sua senha através do link %s:';
$_['text_services']             = 'Ao acessar sua conta de parceiro, você será capaz de gerar links com seu código de parceiro, acompanhar os pagamentos de comissão, alterar as informações de sua conta e muito mais.';
$_['text_thanks']		        = 'Atenciosamente,';
$_['text_new_affiliate']        = 'Um novo parceiro se cadastrou.';
$_['text_signup']		        = 'Dados do novo parceiro:';
$_['text_store']		        = 'Loja:';
$_['text_firstname']	        = 'Nome:';
$_['text_lastname']		        = 'Sobrenome:';
$_['text_company']		        = 'Empresa:';
$_['text_email']		        = 'E-mail:';
$_['text_telephone']	        = 'Telefone:';
$_['text_website']		        = 'Site:';
$_['text_order_id']             = 'Pedido nº:';
$_['text_transaction_subject']  = '%s - Pagamento de comissão';
$_['text_transaction_received'] = 'Você recebeu %s de comissão.';
$_['text_transaction_total']    = 'O total de sua comissão é: %s.';
