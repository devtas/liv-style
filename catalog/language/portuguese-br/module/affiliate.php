<?php
// Heading
$_['heading_title']    = 'Programa de Parceiros';

// Text
$_['text_register']    = 'Cadastre-se';
$_['text_login']       = 'Acessar';
$_['text_logout']      = 'Sair';
$_['text_forgotten']   = 'Esqueci Minha Senha';
$_['text_account']     = 'Minha Conta';
$_['text_edit']        = 'Dados Pessoais';
$_['text_password']    = 'Alterar Senha';
$_['text_payment']     = 'Informações para Comissões';
$_['text_tracking']    = 'Gerador de Links e Imagens';
$_['text_transaction'] = 'Meu Saldo';
