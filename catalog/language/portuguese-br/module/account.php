<?php
// Heading
$_['heading_title']    = 'Minha Conta';

// Text
$_['text_register']    = 'Cadastre-se';
$_['text_login']       = 'Acessar';
$_['text_logout']      = 'Sair';
$_['text_forgotten']   = 'Esqueci Minha Senha';
$_['text_account']     = 'Minha Conta';
$_['text_edit']        = 'Dados Pessoais';
$_['text_password']    = 'Alterar Senha';
$_['text_address']     = 'Lista de Endereços';
$_['text_wishlist']    = 'Lista de Desejos';
$_['text_order']       = 'Meus Pedidos';
$_['text_download']    = 'Meus Downloads';
$_['text_reward']      = 'Pontos de Fidelidade';
$_['text_return']      = 'Devoluções';
$_['text_transaction'] = 'Transações';
$_['text_newsletter']  = 'Newsletter';
$_['text_recurring']   = 'Assinaturas';