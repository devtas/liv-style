<?php
// Heading
$_['heading_title']             = 'Informações para comissões';

// Text
$_['text_account']              = 'Programa de Parceiros';
$_['text_payment']              = 'Informações para comissões';
$_['text_your_payment']         = 'Pagamento das comissões';
$_['text_your_password']        = 'Sua senha';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'Paypal';
$_['text_bank']                 = 'Depósito bancário';
$_['text_success']              = 'As informações para o pagamento de comissões foram modificadas.';

// Entry
$_['entry_tax']                 = 'CPF';
$_['entry_payment']             = 'Receber por';
$_['entry_cheque']              = 'Beneficiário';
$_['entry_paypal']              = 'E-mail no Paypal';
$_['entry_bank_name']           = 'Banco';
$_['entry_bank_branch_number']  = 'Agência';
$_['entry_bank_swift_code']     = 'Código do banco';
$_['entry_bank_account_name']   = 'Titular da conta';
$_['entry_bank_account_number'] = 'Conta corrente';
