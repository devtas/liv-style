<?php
// Heading
$_['heading_title']    = 'Gerador de Links e Imagens';

// Text
$_['text_account']     = 'Programa de Parceiros';
$_['text_description'] = 'Para ter certeza que suas indicações vão ser registradas corretamente, gere os links para nossos produtos através do formulário abaixo:';

// Entry
$_['entry_code']       = 'Código de parceiro';
$_['entry_generator']  = 'Digite o nome do produto';
$_['entry_link']       = 'Link para divulgação';

// Help
$_['help_generator']   = 'Digite o nome de um produto para gerar o link para divulgação';