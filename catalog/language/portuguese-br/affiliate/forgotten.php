<?php
// Heading
$_['heading_title']   = 'Recuperação de Senha';

// Text
$_['text_account']    = 'Programa de parceiros';
$_['text_forgotten']  = 'Recuperação de Senha';
$_['text_your_email'] = 'Seu e-mail cadastrado na loja';
$_['text_email']      = 'Digite o e-mail associado à sua conta. Depois pressione o botão <b>continuar</b> para enviarmos um e-mail para você.';
$_['text_success']    = 'Enviamos a nova senha para o seu e-mail.';

// Entry
$_['entry_email']     = 'E-mail';

// Error
$_['error_email']     = 'Atenção: O e-mail informado não foi localizado em nossa loja, tente novamente!';