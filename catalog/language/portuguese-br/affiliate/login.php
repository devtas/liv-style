<?php
// Heading
$_['heading_title']                 = 'Programa de Parceiros';

// Text
$_['text_account']                  = 'Programa de Parceiros';
$_['text_login']                    = 'Acessar';
$_['text_description']              = '<p>O programa de parceiros da %s oferece a possibilidade de você gerar uma renda extra através da indicação de nossos produtos. Após realizar o seu cadastro no programa de parceiros, todas as vendas realizadas para clientes que tiverem acessado a %s através do seu link de parceiro, vão gerar uma comissão para você. A comissão que oferecemos é de %s sobre o valor do pedido.</p><p>Para mais detalhes sobre o programa de parceiros, leia nossos termos de parceiros ao realizar o seu cadastro.</p>';
$_['text_new_affiliate']            = 'Ainda não é parceiro?';
$_['text_register_account']         = '<p>Cadastre-se agora, pois é simples e rápido.</p><p>Ao criar sua conta, você terá acesso ao seu código de parceiro, e poderá gerar links para divulgação de nossos produtos. Se você já tem uma conta de cliente em nossa loja, note que sua conta de cliente não será ligada a sua conta de parceiro.</p>';
$_['text_returning_affiliate']      = 'Já é parceiro?';
$_['text_i_am_returning_affiliate'] = 'Se você já faz parte do programa de parceiros, informe abaixo os dados de acesso:';
$_['text_forgotten']                = 'Esqueci minha senha';

// Entry
$_['entry_email']                   = 'E-mail ou CPF';
$_['entry_password']                = 'Senha';

// Error
$_['error_login']                   = 'Seus dados de acesso não estão corretos. Se você tem certeza que o e-mail ou CPF está correto, solicite uma nova senha.';
$_['error_attempts']                = 'Você excedeu o limite de tentativas de acesso. Por segurança, tente acessar novamente após 1 hora.';
$_['error_approved']                = 'Sua conta ainda não foi aprovada. Quando sua conta for aprovada, você receberá um e-mail de confirmação.';