<?php
// Heading
$_['heading_title']     = 'Dados Pessoais';

// Text
$_['text_account']      = 'Parceiros';
$_['text_edit']         = 'Dados Pessoais';
$_['text_your_details'] = 'Dados de Contato';
$_['text_your_address'] = 'Seu site e endereço';
$_['text_success']      = 'Seus dados pessoais foram alterados.';

// Entry
$_['entry_firstname']   = 'Nome';
$_['entry_lastname']    = 'Sobrenome';
$_['entry_email']       = 'E-mail';
$_['entry_telephone']   = 'Telefone';
$_['entry_fax']         = 'CPF';
$_['entry_company']     = 'Número';
$_['entry_website']     = 'Site / Blog';
$_['entry_address_1']   = 'Endereço';
$_['entry_address_2']   = 'Bairro';
$_['entry_postcode']    = 'CEP';
$_['entry_city']        = 'Cidade';
$_['entry_country']     = 'País';
$_['entry_zone']        = 'Estado';

// Error
$_['error_exists']      = 'Atenção: Este e-mail já está registrado!';
$_['error_firstname']   = 'O nome deve ter entre 2 e 32 caracteres!';
$_['error_lastname']    = 'O sobrenome deve ter entre 2 e 32 caracteres!';
$_['error_email']       = 'O e-mail não é válido!';
$_['error_telephone']   = 'O telefone deve ter entre 10 e 32 caracteres!';
$_['error_address_1']   = 'O endereço deve ter entre 3 e 128 caracteres!';
$_['error_city']        = 'A cidade deve ter entre 2 e 128 caracteres!';
$_['error_country']     = 'Selecione o país!';
$_['error_zone']        = 'Selecione o estado!';
$_['error_postcode']    = 'O CEP deve ter 8 caracteres!';
