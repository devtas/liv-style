<?php
// Heading
$_['heading_title']  = 'Alterar Senha';

// Text
$_['text_account']   = 'Programas de parceiros';
$_['text_password']  = 'Sua senha de acesso';
$_['text_success']   = 'Sua senha foi alterada.';

// Entry
$_['entry_password'] = 'Senha';
$_['entry_confirm']  = 'Repetir a senha';

// Error
$_['error_password'] = 'A senha dever ter entre 4 e 20 caracteres!';
$_['error_confirm']  = 'A senha repetida esta errada!';