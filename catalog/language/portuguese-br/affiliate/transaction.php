<?php
// Heading
$_['heading_title']      = 'Meu Saldo';

// Column
$_['column_date_added']  = 'Paga em';
$_['column_description'] = 'Descrição';
$_['column_amount']      = 'Comissão (%s)';

// Text
$_['text_account']       = 'Programa de Parceiros';
$_['text_transaction']   = 'Meu Saldo';
$_['text_balance']       = 'O seu saldo é:';
$_['text_empty']         = 'Você ainda não recebeu comissões!';