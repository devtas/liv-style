<?php
// Heading
$_['heading_title']        = 'Minha Conta de Parceiro';

// Text
$_['text_account']         = 'Programa de Parceiros';
$_['text_my_account']      = 'Minha Conta';
$_['text_my_tracking']     = 'Ferramenta';
$_['text_my_transactions'] = 'Minhas Comissões';
$_['text_edit']            = 'Dados Pessoais';
$_['text_password']        = 'Alterar Senha';
$_['text_payment']         = 'Informações para Comissões';
$_['text_tracking']        = 'Gerador de Links e Imagens';
$_['text_transaction']     = 'Meu Saldo';