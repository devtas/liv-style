<?php
// Text
$_['text_title']				= 'Transferência/Depósito bancário';
$_['text_instruction']			= 'Instruções';
$_['text_description']			= 'Transfira ou deposite o valor total do pedido na conta abaixo:';
$_['text_payment']				= 'Após a transferência ou depósito, envie a cópia do comprovante por e-mail, acompanhado do número do seu pedido.';
