<?php
// Heading
$_['heading_title']   = 'Comprovante';

// Text
$_['text_basket']     = 'Carrinho de compras';
$_['text_checkout']   = 'Finalizar pedido';
$_['text_success']    = 'Comprovante';
$_['text_importante'] = '
              <img src="/image/catalog/config/mail/mail-success.png" class="pull-left title-icon" /> <h2>COMPRA REALIZADA COM SUCESSO!</h2>

<br/>

Seu pagamento foi aprovado e recebemos o seu pedido com sucesso!

<br/>

<br/>

Agora nossa equipe irá separar o seu produto com muito carinho, para que ele chegue até você o mais rápido possível.

<br/>

<br/>

Temos um enorme prazer de servir você, e desejamos que nossa marca possa fazer parte de momentos incríveis em sua vida.

<br/>

<br/>

Agora enviaremos um email com todos os detalhes de sua compra. Em caso de qualquer dúvida, basta enviar um email para contato@livstyle.com.br ou pelo Whatsapp:(11) 99010-9555 que nossa equipe está pronta para lhe atender.

<br/>
<br/>

<strong>Com amor,</strong>

<br/>

<strong>LIVSTYLE</strong>';

$_['text_itens_area']         = 'Itens do pedido';

// Button
$_['button_imprimir'] = 'Imprimir';