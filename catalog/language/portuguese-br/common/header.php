<?php
// Text
$_['text_home']          = 'Início';
$_['text_wishlist']      = 'Lista de desejos (%s)';
$_['text_shopping_cart'] = 'Carrinho de compras';
$_['text_category']      = 'Categorias';
$_['text_account']       = 'Minha Conta';
$_['text_register']      = 'Cadastre-se';
$_['text_login']         = 'Acesse sua conta';
$_['text_order']         = 'Meus Pedidos';
$_['text_transaction']   = 'Transações';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Sair';
$_['text_checkout']      = 'Finalizar pedido';
$_['text_search']        = 'Busca';
$_['text_all']           = 'Exibir';