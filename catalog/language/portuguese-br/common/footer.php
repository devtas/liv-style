<?php
// Text
$_['text_information']  = 'Informações';
$_['text_service']      = 'Serviços ao cliente';
$_['text_extra']        = 'Outros serviços';
$_['text_contact']      = 'Contato';
$_['text_return']       = 'Trocas e Devoluções';
$_['text_sitemap']      = 'Mapa do site';
$_['text_manufacturer'] = 'Produtos por marca';
$_['text_voucher']      = 'Vale presentes';
$_['text_affiliate']    = 'Parceria';
$_['text_special']      = 'Produtos em Oferta';
$_['text_account']      = 'Minha Conta';
$_['text_order']        = 'Meus Pedidos';
$_['text_wishlist']     = 'Lista de Desejos';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Desenvolvido com tecnologia <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';