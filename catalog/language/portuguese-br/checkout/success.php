<?php
// Heading
$_['heading_title']        = 'Compra realizada com sucesso';

// Text
$_['text_basket']          = 'Carrinho de compras';
$_['text_checkout']        = 'Finalizar pedido';
$_['text_success']         = 'Confirmação';
$_['text_customer2']        = '<p>O seu pedido foi realizado com sucesso.</p><p>Você pode ver o histórico dos seus pedidos através de sua <a href="%s">conta</a>, acessando o menu <a href="%s">meus pedidos</a>.</p><p>Caso seu pedido esteja associado a arquivos para download, acesse em sua conta o menu <a href="%s">downloads</a> para baixá-los.</p><p>Entre em contato com o nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado e volte sempre.</p>';
$_['text_customer']			= '
<br/>

Seu pagamento foi aprovado e recebemos o seu pedido com sucesso!

<br/>

<br/>

Agora nossa equipe irá separar o seu produto com muito carinho, para que ele chegue até você o mais rápido possível.

<br/>

<br/>

Temos um enorme prazer de servir você, e desejamos que nossa marca possa fazer parte de momentos incríveis em sua vida.

<br/>

<br/>

Agora enviaremos um email com todos os detalhes de sua compra. Em caso de qualquer dúvida, basta enviar um email para contato@livstyle.com.br ou pelo Whatsapp:(11) 99010-9555 que nossa equipe está pronta para lhe atender.

<br/>
<br/>

<strong>Com amor,</strong>

<br/>

<strong>LIVSTYLE</strong>';

$_['text_itens_area']         = 'Itens do pedido';

$_['text_guest']           = '<p>O seu pedido foi realizado com sucesso.</p><p>Entre em contato com o nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado e volte sempre.</p>';
