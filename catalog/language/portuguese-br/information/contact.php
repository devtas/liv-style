<?php
// Heading
$_['heading_title']  = 'Contato';

// Text
$_['text_location']  = 'Nossa localização';
$_['text_store']     = 'Nossas lojas';
$_['text_contact']   = 'Formulário de contato';
$_['text_address']   = 'Endereço';
$_['text_telephone'] = 'Telefone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Horário de funcionamento';
$_['text_comment']   = 'Comentários';
$_['text_success']   = '<p>Sua mensagem foi enviada com sucesso, em breve entraremos em contato!</p>';

// Entry
$_['entry_name']     = 'Nome';
$_['entry_email']    = 'E-mail';
$_['entry_enquiry']  = 'Mensagem';
$_['entry_captcha']  = 'Código';

// Email
$_['email_subject']  = 'Contato - %s';

// Errors
$_['error_name']     = 'O seu nome deve ter entre 3 e 32 caracteres!';
$_['error_email']    = 'O e-mail não é válido!';
$_['error_enquiry']  = 'A mensagem deve ter entre 10 e 3000 caracteres!';
$_['error_captcha']  = 'O código informado não é o mesmo da imagem!';
