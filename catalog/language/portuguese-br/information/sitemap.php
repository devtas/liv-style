<?php
// Heading
$_['heading_title']    = 'Mapa do Site';

// Text
$_['text_special']     = 'Produtos em Oferta';
$_['text_account']     = 'Minha Conta';
$_['text_edit']        = 'Dados Pessoais';
$_['text_password']    = 'Alterar Senha';
$_['text_address']     = 'Lista de Endereços';
$_['text_history']     = 'Meus Pedidos';
$_['text_download']    = 'Meus Downloads';
$_['text_cart']        = 'Carrinho de Compras';
$_['text_checkout']    = 'Finalizar Pedido';
$_['text_search']      = 'Buscar Produtos';
$_['text_information'] = 'Informações';
$_['text_contact']     = 'Contato';