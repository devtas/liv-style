<?php
// Heading
$_['heading_title']    = 'Promoções, ofertas e novidades por e-mail';

// Text
$_['text_account']     = 'Minha Conta';
$_['text_newsletter']  = 'Newsletter';
$_['text_success']     = 'A sua assinatura em nossa newsletter foi modificada.';

// Entry
$_['entry_newsletter'] = 'Deseja receber?';
