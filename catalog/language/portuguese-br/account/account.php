<?php
// Heading
$_['heading_title']      = 'Minha Conta';

// Text
$_['text_account']       = 'Minha Conta';
$_['text_my_account']    = 'Minha Conta';
$_['text_my_orders']     = 'Meus Pedidos';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Dados Pessoais';
$_['text_password']      = 'Alterar Senha';
$_['text_address']       = 'Lista de Endereços';
$_['text_wishlist']      = 'Lista de Desejos';
$_['text_order']         = 'Meus Pedidos';
$_['text_download']      = 'Meus Downloads';
$_['text_reward']        = 'Pontos de Fidelidade';
$_['text_return']        = 'Minhas Trocas e Devoluções';
$_['text_transaction']   = 'Transações';
$_['text_newsletter']    = 'Promoções, ofertas e novidades por e-mail';
$_['text_recurring']     = 'Assinaturas';
$_['text_transactions']  = 'Transações';