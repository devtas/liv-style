<?php
// Heading
$_['heading_title']      = 'Pontos de Fidelidade';

// Column
$_['column_date_added']  = 'Cadastro';
$_['column_description'] = 'Descrição';
$_['column_points']      = 'Pontos de Fidelidade';

// Text
$_['text_account']       = 'Minha Conta';
$_['text_reward']        = 'Pontos de Fidelidade';
$_['text_total']         = 'Total de Pontos de Fidelidade:';
$_['text_empty']         = 'Você ainda não possui pontos de fidelidade!';