<?php
// Heading
$_['heading_title']      = 'Dados Pessoais';

// Text
$_['text_account']       = 'Minha Conta';
$_['text_edit']          = 'Dados Pessoais';
$_['text_your_details']  = 'Caso deseje, edite seus dados pessoais';
$_['text_success']       = 'Seus dados pessoais foram alterados.';

// Entry
$_['entry_firstname']    = 'Nome';
$_['entry_lastname']     = 'Sobrenome';
$_['entry_email']        = 'E-mail';
$_['entry_telephone']    = 'Telefone';
$_['entry_fax']          = 'CPF';

// Error
$_['error_exists']       = 'Atenção: Este e-mail já está cadastrado!';
$_['error_firstname']    = 'O nome deve ter entre 2 e 32 caracteres!';
$_['error_lastname']     = 'O sobrenome deve entre 2 e 32 caracteres!';
$_['error_fax']     	 = 'O CPF é inválido.';
$_['error_email']        = 'E-mail inválido!';
$_['error_telephone']    = 'O telefone deve ter entre 10 e 32 caracteres!';
$_['error_custom_field'] = 'O campo %s é obrigatório!';
