<?php
// Heading
$_['heading_title'] = 'Lista de desejos';

// Text
$_['text_account']  = 'Minha Conta';
$_['text_instock']  = 'Em estoque';
$_['text_wishlist'] = 'Lista de desejos (%s)';
$_['text_login']    = '<h3>acesse ou cadastre uma conta</h3><p>para adicionar o produto em sua lista de desejos:)</p><img src="image/cart-icon.svg"/><a class="black" href="/conta">Acessar ou Cadastrar</a><a class="white" href="/conta/desejos">Lista de Desejos</a>';
$_['text_success']  = '<h3>Produto Adicionado na Lista de Desejos :)</h3><img src="image/cart-icon.svg"/><a class="black" href="/conta/desejos">Lista de Desejos</a>';
$_['text_exists']   = '<h3>Lista de Desejos</h3><p>O Produto já está em sua lista de Lista de Desejos :)</p><img src="image/cart-icon.svg"/><a class="black" href="/conta/desejos">Lista de Desejos</a>';
$_['text_remove']   = 'Você alterou sua lista de desejos.';
$_['text_empty']    = 'Você ainda não adicionou itens a sua lista de desejos.';

// Column
$_['column_image']  = 'Imagem';
$_['column_name']   = 'Produto';
$_['column_model']  = 'Modelo';
$_['column_stock']  = 'Estoque';
$_['column_price']  = 'Preço';
$_['column_action'] = 'Ação';