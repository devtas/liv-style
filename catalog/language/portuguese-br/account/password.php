<?php
// Heading
$_['heading_title']  = 'Alterar Senha';

// Text
$_['text_account']   = 'Minha Conta';
$_['text_password']  = 'Preencha abaixo a sua nova senha.';
$_['text_success']   = 'Sua senha foi alterada.';

// Entry
$_['entry_password'] = 'Senha';
$_['entry_confirm']  = 'Repetir a senha';

// Error
$_['error_password'] = 'A senha deve ter entre 4 e 20 caracteres!';
$_['error_confirm']  = 'A senha repetida esta errada!';