<?php
// Heading
$_['heading_title']                = 'Acessar Conta';

// Text
$_['text_account']                 = 'Minha Conta';
$_['text_login']                   = 'Acessar';
$_['text_new_customer']            = 'Ainda não tem sua conta?';
$_['text_register']                = 'Cadastre sua conta.';
$_['text_register_account']        = 'Ao criar sua conta, você será capaz de comprar mais rápido, ficar atualizado sobre a situação de um pedido e acompanhar o histórico dos pedidos que você já fez.';
$_['text_register_account_resume'] = 'É rápido e fácil :)';
$_['text_register_now']        	   = 'clique aqui para criar sua conta';
$_['text_returning_customer']      = 'Dados de Acesso';
$_['text_i_am_returning_customer'] = 'Se você já possui uma conta, informe abaixo os dados de acesso:';
$_['text_forgotten']               = 'Esqueci minha senha';

// Entry
$_['entry_email']                  = 'E-mail';
$_['entry_password']               = 'Senha';

// Error
$_['error_login']                  = 'Seus dados de acesso não estão corretos. Se você tem certeza que o e-mail ou CPF está correto, solicite uma nova senha.';
$_['error_attempts']               = 'Você excedeu o limite de tentativas de acesso. Por segurança, tente acessar novamente após 1 hora.';
$_['error_approved']               = 'Sua conta ainda não foi aprovada. Quando sua conta for aprovada, você receberá um e-mail de confirmação.';