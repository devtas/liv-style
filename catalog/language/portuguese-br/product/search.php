<?php
// Heading
$_['heading_title']     = 'Buscar Produtos';
$_['heading_tag']		= 'Etiqueta - ';

// Text
$_['text_search']       = 'Resultado da busca';
$_['text_keyword']      = 'Buscar';
$_['text_category']     = 'Todos as categorias';
$_['text_sub_category'] = 'Pesquisar nas subcategorias';
$_['text_empty']        = 'Não há nenhum produto que corresponda aos critérios de busca.';
$_['text_quantity']     = 'Qtd:';
$_['text_manufacturer'] = 'Marca:';
$_['text_model']        = 'Ref:';
$_['text_points']       = 'Pontos:';
$_['text_price']        = 'Preço:';
$_['text_tax']          = 'Sem impostos:';
$_['text_reviews']      = 'Baseada em %s comentários.';
$_['text_compare']      = 'Comparação de produtos (%s)';
$_['text_sort']         = 'Ordernar por:';
$_['text_default']      = 'Padrão';
$_['text_name_asc']     = 'Nome (A - Z)';
$_['text_name_desc']    = 'Nome (Z - A)';
$_['text_price_asc']    = 'Preço (menor &gt; maior)';
$_['text_price_desc']   = 'Preço (maior &gt; menor)';
$_['text_rating_asc']   = 'Avaliação (menor)';
$_['text_rating_desc']  = 'Avaliação (maior)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Exibir:';

// Entry
$_['entry_search']      = 'Critérios da busca:';
$_['entry_description'] = 'Pesquisar na descrição dos produtos';