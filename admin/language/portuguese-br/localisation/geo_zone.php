<?php
// Heading
$_['heading_title']      = 'Regiões';

// Text
$_['text_success']       = 'Região  modificada com sucesso!';
$_['text_list']          = 'Listando regiões';
$_['text_add']           = 'Nova região';
$_['text_edit']          = 'Editando região';

// Column
$_['column_name']        = 'Região';
$_['column_description'] = 'Descrição';
$_['column_action']      = 'Ação';

// Entry
$_['entry_name']         = 'Região';
$_['entry_description']  = 'Descrição';
$_['entry_country']      = 'País';
$_['entry_zone']         = 'Estado';

// Error
$_['error_permission']   = 'Atenção: Você não possui permissão para modificar as regiões!';
$_['error_name']         = 'A região deve ter entre 3 e 32 caracteres!';
$_['error_description']  = 'A descrição deve ter entre 3 e 255 caracteres!';
$_['error_tax_rate']     = 'Atenção: Esta região não pode ser excluída, pois está vinculada a um ou mais tipos de impostos!';