<?php
// Heading
$_['heading_title']     = 'Transações e.Rede REST';

// Text
$_['text_list']         = 'Listagem de transações';
$_['text_autorizada']   = 'Autorizada';
$_['text_capturada']    = 'Capturada';
$_['text_processando']  = 'Processando cancelamento';
$_['text_cancelada']    = 'Cancelada';
$_['text_negada']       = 'Cancelamento negado';
$_['text_debito']       = 'Cartão de débito';
$_['text_credito']      = 'Cartão de crédito';
$_['text_confirmacao']  = 'Você tem certeza que deseja excluir esta transação?';

// Column
$_['column_order_id']   = 'Pedido nº';
$_['column_date_added'] = 'Data do pedido';
$_['column_customer']   = 'Cliente';
$_['column_metodo']     = 'Método';
$_['column_status']     = 'Status';
$_['column_action']     = 'Ação';

// Button
$_['button_excluir']    = 'Excluir transação';
$_['button_info']       = 'Informações';

// Error
$_['error_permission']  = 'Atenção: Você não tem permissão para visualizar as transações!';
$_['error_warning']     = 'Selecione uma transação válida!';