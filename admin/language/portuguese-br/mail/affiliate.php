<?php
// Text
$_['text_approve_subject']      = '%s - Sua conta de parceiro foi aprovada!';
$_['text_approve_welcome']      = 'Obrigado por se cadastar no Programa de parceiros da loja %s!';
$_['text_approve_login']        = 'Sua conta de parceiro precisa ser aprovada para que você possa acessar os dados de sua conta do programa de parceiros. Assim que sua conta for aprovada, você poderá acessar sua conta utilizando seu e-mail e sua senha através do link %s:';
$_['text_approve_services']     = 'Ao acessar sua conta de parceiro, você será capaz de gerar links com seu código de parceiro, acompanhar os pagamentos de comissão, alterar as informações de sua conta e muito mais.';
$_['text_approve_thanks']       = 'Atenciosamente,';
$_['text_transaction_subject']  = '%s - Pagamento de comissão';
$_['text_transaction_received'] = 'Você recebeu %s de comissão.';
$_['text_transaction_total']    = 'O total de sua comissão é: %s.';
