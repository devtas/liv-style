<?php
// Heading
$_['heading_title']    = 'Menu de categorias';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Módulo Menu de categorias modificado com sucesso!';
$_['text_edit']        = 'Configurações do módulo Menu de categorias';

// Entry
$_['entry_status']     = 'Situação';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o módulo Menu de categorias!';