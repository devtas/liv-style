<?php
// Heading
$_['heading_title']    = 'Menu do parceiro';

$_['text_module']      = 'Módulos';
$_['text_success']     = 'Módulo Menu do parceiro modificado com sucesso!';
$_['text_edit']        = 'Configurações do módulo Menu do parceiro';

// Entry
$_['entry_status']     = 'Situação';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o módulo Menu do parceiro!';