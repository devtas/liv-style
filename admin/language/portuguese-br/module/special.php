<?php
// Heading
$_['heading_title']    = 'Ofertas';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Módulo Produtos em oferta modificado com sucesso!';
$_['text_edit']        = 'Configurações do módulo Produtos em oferta';

// Entry
$_['entry_name']       = 'Módulo';
$_['entry_limit']      = 'Limite';
$_['entry_width']      = 'Largura';
$_['entry_height']     = 'Altura';
$_['entry_status']     = 'Situação';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o módulo Produtos em oferta!';
$_['error_name']       = 'O módulo deve ter entre 3 e 64 caracteres!';
$_['error_width']      = 'A largura é obrigatória!';
$_['error_height']     = 'A altura é obrigatória!';