<?php
// Heading
$_['heading_title']    = 'Frete por região';

// Text
$_['text_shipping']    = 'Frete';
$_['text_success']     = 'Frete por região modificado com sucesso!';

// Entry
$_['entry_cost']       = 'Valor';
$_['entry_country']    = 'País';
$_['entry_zone']       = 'Região';
$_['entry_status']     = 'Situação';
$_['entry_sort_order'] = 'Posição';
$_['entry_first']      = 'Primeiro produto';
$_['entry_next']       = 'Produto adicional';
$_['entry_tax_class']  = 'Grupo de impostos';

//buttons
$_['button_add']       = 'Adicionar';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o Frete por região!';
?>