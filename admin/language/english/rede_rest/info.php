<?php
// Heading
$_['heading_title']          = 'Transação e.Rede REST';

// Text
$_['text_list']              = 'Transações';
$_['text_edit']              = 'Informações da transação';
$_['text_autorizada']        = 'Autorizada';
$_['text_capturada']         = 'Capturada';
$_['text_processando']       = 'Processando cancelamento';
$_['text_cancelada']         = 'Cancelada';
$_['text_negada']            = 'Cancelamento negado';
$_['text_consultando']       = 'Consultando a transação na Rede...';
$_['text_cancelando']        = 'Cancelando a transação na Rede...';
$_['text_capturando']        = 'Capturando a transação na Rede...';
$_['text_aguarde']           = 'Aguarde...';
$_['text_dia']               = 'dia';
$_['text_dias']              = 'dias';
$_['text_dias_capturar']     = 'Capturar até %s (restam %s %s).';
$_['text_dias_cancelar']     = 'Cancelar até %s (restam %s %s).<br>Alguns cartões só podem ser cancelados 24 horas após o pagamento.<br>Caso você não consiga cancelar, entre em contato com a Rede para mais informações.';
$_['text_debito']            = 'Cartão de débito';
$_['text_credito']           = 'Cartão de crédito';
$_['text_fuso_horario']      = '(Horário de Brasília)';

// Tab
$_['tab_details']            = 'Detalhes';
$_['tab_json']               = 'Último JSON';

// Button
$_['button_consultar']       = 'Consultar na Rede';
$_['button_capturar']        = 'Capturar na Rede';
$_['button_cancelar']        = 'Cancelar na Rede';
$_['button_antifraude']      = 'Solicitar análise';

// Entry
$_['entry_order_id']         = 'Pedido nº:';
$_['entry_added']            = 'Data do pedido:';
$_['entry_total']            = 'Total do pedido:';
$_['entry_customer']         = 'Cliente:';
$_['entry_bandeira']         = 'Bandeira:';
$_['entry_parcelamento']     = 'Parcelado em:';
$_['entry_tid']              = 'TID:';
$_['entry_nsu']              = 'NSU:';
$_['entry_autorizacao']      = 'Data da autorização:';
$_['entry_valor_autorizado'] = 'Valor autorizado:';
$_['entry_captura']          = 'Data da captura:';
$_['entry_valor_capturado']  = 'Valor capturado:';
$_['entry_cancelamento']     = 'Data do cancelamento:';
$_['entry_valor_cancelado']  = 'Valor cancelado:';
$_['entry_status']           = 'Status da transação:';
$_['entry_clearsale']        = 'Antifraude ClearSale:';
$_['entry_fcontrol']         = 'Antifraude Fcontrol:';

// Error
$_['error_permission']       = 'Atenção: Você não tem permissão para modificar transações!';
$_['error_iframe']           = 'Seu navegador não tem suporte para iframes.';
$_['error_warning']          = 'Selecione uma transação válida!';
$_['error_consultar']        = 'Não foi possível consultar a transação!';
$_['error_capturar']         = 'Não foi possível capturar a transação!';
$_['error_cancelar']         = 'Não foi possível cancelar a transação!';