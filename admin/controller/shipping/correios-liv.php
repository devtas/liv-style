<?php

// Requiring Composer
// $vendor = dirname(dirname(dirname(__DIR__))) . '/vendor/';
// $autoload = $vendor . 'autoload.php';

class ControllerShippingCorreiosLiv extends Controller {
    // private $image = dirname(dirname(dirname(__DIR__))) . '/vendor/';

    public function index() {
        $data = array();

        if (isset($_GET['action'])) {
            $ACTION = $_GET['action'];

            switch ($ACTION) {
                case 'imprime_etiqueta':
                    if (isset($_GET['order_id'])) {
                        $order = $_GET['order_id'];
                        $this->imprimeEtiqueta($order);
                    } else {
                        $this->genericView();
                    }
                    break;

                case 'imprime_plp':
                    if (isset($_GET['order_id'])) {
                        $order = $_GET['order_id'];
                        $this->imprimePlp($order);
                    } else {
                        $this->genericView();
                    }
                    break;
                
                default:
                    $this->genericView();
                    break;
            }
        } else {
            $this->homeView();
        }
    }


    public function homeView() {
        $this->load->language('shipping/correios');
        $this->document->setTitle($this->language->get('heading_title'));
        
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('shipping/correios-liv-home.tpl', $data));
    }


    public function genericView() {
        $this->load->language('shipping/correios');
        $this->document->setTitle($this->language->get('heading_title'));
        
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('shipping/correios-liv.tpl', $data));
    }


    public function imprimeEtiqueta($order_id) {
        $this->load->language('shipping/correios');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('shipping/correiosliv');

        $queryString = "SELECT * FROM " . DB_PREFIX . "order_correios_liv WHERE order_id = '" . $this->db->escape($order_id) . "'";
        $shippingQuery = $this->db->query($queryString);
        if ($shippingQuery->num_rows > 0) {
            $correios = $shippingQuery->rows[0];

            $this->model_shipping_correiosliv->imprimirEtiqueta($correios);
        }
    }


    public function imprimePlp($order_id) {
        $this->load->language('shipping/correios');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('shipping/correiosliv');

        $queryString = "SELECT * FROM " . DB_PREFIX . "order_correios_liv WHERE order_id = '" . $this->db->escape($order_id) . "'";
        $shippingQuery = $this->db->query($queryString);
        if ($shippingQuery->num_rows > 0) {
            $correios = $shippingQuery->rows[0];

            $this->model_shipping_correiosliv->imprimirPlp($correios);
        }
    }
}
?>