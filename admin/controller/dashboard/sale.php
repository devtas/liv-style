<?php
class ControllerDashboardSale extends Controller {
	public function index() {
		$this->load->language('dashboard/sale');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_view'] = $this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		$this->load->model('report/sale');

		$today = $this->model_report_sale->getTotalSales(array('filter_date_added' => date('Y-m-d', strtotime('-1 day'))));

		$yesterday = $this->model_report_sale->getTotalSales(array('filter_date_added' => date('Y-m-d', strtotime('-2 day'))));

		$difference = $today - $yesterday;

		if ($difference && $today) {
			$data['percentage'] = round(($difference / $today) * 100);
		} else {
			$data['percentage'] = 0;
		}

		$sale_total = $this->model_report_sale->getTotalSales();
		$sale_completed = $this->model_report_sale->getTotalSalesApproved();

		if ($sale_total > 1000000000000) {
			$data['total'] = round($sale_total / 1000000000000, 1) . 'T';
		} elseif ($sale_total > 1000000000) {
			$data['total'] = round($sale_total / 1000000000, 1) . 'B';
		} elseif ($sale_total > 1000000) {
			$data['total'] = round($sale_total / 1000000, 1) . 'M';
		} elseif ($sale_total > 1000) {
			$data['total'] = round($sale_total / 1000, 1) . 'K';
		} else {
			$data['total'] = round($sale_total);
		}

		if ($sale_completed > 1000000000000) {
			$data['total_completed'] = round($sale_completed / 1000000000000, 1) . 'T';
		} elseif ($sale_completed > 1000000000) {
			$data['total_completed'] = round($sale_completed / 1000000000, 1) . 'B';
		} elseif ($sale_completed > 1000000) {
			$data['total_completed'] = round($sale_completed / 1000000, 1) . 'M';
		} elseif ($sale_completed > 1000) {
			$data['total_completed'] = round($sale_completed / 1000, 1) . 'K';
		} else {
			$data['total_completed'] = round($sale_completed);
		}

		$data['sale'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL');
		$data['percentage_completed'] = $sale_total ? round((($sale_completed * 100)/$sale_total), 2) : 0;

		return $this->load->view('dashboard/sale.tpl', $data);
	}
}
