<?php
class ControllerDashboardOrder extends Controller {
	public function index() {
		$this->load->language('dashboard/order');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_view'] = $this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

		// Total Orders
		$this->load->model('sale/order');

		$today = $this->model_sale_order->getTotalOrders(array('filter_date_added' => date('Y-m-d', strtotime('-1 day'))));

		$yesterday = $this->model_sale_order->getTotalOrders(array('filter_date_added' => date('Y-m-d', strtotime('-2 day'))));

		$difference = $today - $yesterday;

		if ($difference && $today) {
			$data['percentage'] = round(($difference / $today) * 100);
		} else {
			$data['percentage'] = 0;
		}

		$order_total = $this->model_sale_order->getTotalOrders();
		$order_completed = $this->model_sale_order->getTotalOrders(array('filter_order_status' => '9'));

		if ($order_total > 1000000000000) {
			$data['total'] = round($order_total / 1000000000000, 1) . 'T';
		} elseif ($order_total > 1000000000) {
			$data['total'] = round($order_total / 1000000000, 1) . 'B';
		} elseif ($order_total > 1000000) {
			$data['total'] = round($order_total / 1000000, 1) . 'M';
		} elseif ($order_total > 1000) {
			$data['total'] = round($order_total / 1000, 1) . 'K';
		} else {
			$data['total'] = $order_total;
		}

		if ($order_completed > 1000000000000) {
			$data['total_completed'] = round($order_completed / 1000000000000, 1) . 'T';
		} elseif ($order_completed > 1000000000) {
			$data['total_completed'] = round($order_completed / 1000000000, 1) . 'B';
		} elseif ($order_completed > 1000000) {
			$data['total_completed'] = round($order_completed / 1000000, 1) . 'M';
		} elseif ($order_completed > 1000) {
			$data['total_completed'] = round($order_completed / 1000, 1) . 'K';
		} else {
			$data['total_completed'] = $order_completed;
		}

		$data['order'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL');

		// Percentage Completed
		$data['percentage_completed'] = $order_total ? round((($order_completed * 100)/$order_total), 2) : 0;

		return $this->load->view('dashboard/order.tpl', $data);
	}
}
