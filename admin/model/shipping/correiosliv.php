<?php

// Requiring Composer
$vendor = dirname(dirname(dirname(__DIR__))) . '/vendor/';
$autoload = $vendor . 'autoload.php';

if (file_exists($autoload)) {
    require_once $autoload;
}

class ModelShippingCorreiosLiv extends Model {
	private $accessData;
	private $sigepConfig;
	private $orderId;
	private $sigepDv;
	private $shippingMethodCodeNumber;
	private $sigepPlp;

	public function setConfig() {
		$this->setData();

		$this->sigepConfig = new \PhpSigep\Config();
		$this->sigepConfig->setAccessData($this->accessData);
		$this->sigepConfig->setEnv(\PhpSigep\Config::ENV_PRODUCTION);
		$this->sigepConfig->setCacheOptions(
		    array(
		        'storageOptions' => array(
		            // Qualquer valor setado neste atributo será mesclado ao atributos das classes
		            // "\PhpSigep\Cache\Storage\Adapter\AdapterOptions" e "\PhpSigep\Cache\Storage\Adapter\FileSystemOptions".
		            // Por tanto as chaves devem ser o nome de um dos atributos dessas classes.
		            'enabled' => false,
		            'ttl' => 10,// "time to live" de 10 segundos
		            'cacheDir' => sys_get_temp_dir(), // Opcional. Quando não inforado é usado o valor retornado de "sys_get_temp_dir()"
		        ),
		    )
		);

		\PhpSigep\Bootstrap::start($this->sigepConfig);
	}

	public function setData() {
		$accessData = new \PhpSigep\Model\AccessData();
		$accessData->setUsuario('20852860000241');
		$accessData->setSenha('lv0i05');
		$accessData->setCodAdministrativo('18295347');
		$accessData->setNumeroContrato('9912448157');
		$accessData->setCartaoPostagem('74377710');
		$accessData->setDiretoria(72);
		$accessData->setCnpjEmpresa('20852860000241');
		$this->accessData = $accessData;
	}

	public function preListaServicos() {
		$this->load->model('sale/order');
		$products = $this->model_sale_order->getOrderProducts($this->orderId);

		$peso = 0;
		$comprimento = 0;
		$largura = 0;
		$altura = 0;
		$diametro = 0;

		foreach ($products as $key => $product) {
			$this->load->model('catalog/product');
			$productInfos = $this->model_catalog_product->getProduct($product['product_id']);

			$peso = $peso + intval($productInfos['weight']);
			$comprimento = $comprimento + intval($productInfos['length']);
			$largura = $largura + intval($productInfos['width']);
			$altura = $altura + intval($productInfos['height']);
			// $diametro 		+= $productInfos['height'];
		}

		$peso = ($peso > 0) ? $peso : 0.500;
		$comprimento = ($comprimento > 0) ? $comprimento : 20;
		$largura = ($largura > 0) ? $largura : 20;
		$altura = ($altura > 0) ? $altura : 20;
		$diametro = ($diametro > 0) ? $diametro : 20;

	    $dimensao = new \PhpSigep\Model\Dimensao();
	    $dimensao->setAltura($altura);
	    $dimensao->setLargura($largura);
	    $dimensao->setComprimento($comprimento);
	    $dimensao->setDiametro($diametro);
	    $dimensao->setTipo(\PhpSigep\Model\Dimensao::TIPO_PACOTE_CAIXA);


		$destinatarioDb = $this->model_sale_order->getOrder($this->orderId);

	    $destinatario = new \PhpSigep\Model\Destinatario();
	    $destinatario->setNome($destinatarioDb['shipping_firstname'] . ' ' . $destinatarioDb['shipping_lastname']);
	    $destinatario->setLogradouro($destinatarioDb['shipping_address_1']);
	    $destinatario->setNumero($destinatarioDb['shipping_company']);
	    $destinatario->setComplemento('');

	    $destino = new \PhpSigep\Model\DestinoNacional();
	    $destino->setBairro($destinatarioDb['payment_address_2']);
	    $destino->setCep($destinatarioDb['payment_postcode']);
	    $destino->setCidade($destinatarioDb['payment_city']);
	    $destino->setUf($destinatarioDb['payment_zone']);

	    // Estamos criando uma etique falsa, mas em um ambiente real voçê deve usar o método
	    // {@link \PhpSigep\Services\SoapClient\Real::solicitaEtiquetas() } para gerar o número das etiquetas

	    $etiqueta = new \PhpSigep\Model\Etiqueta();
	    $etiqueta->setEtiquetaComDv($this->sigepDv);

	    $servicoAdicional = new \PhpSigep\Model\ServicoAdicional();
	    $servicoAdicional->setCodigoServicoAdicional(\PhpSigep\Model\ServicoAdicional::SERVICE_REGISTRO);
	    // Se não tiver valor declarado informar 0 (zero)
	    $servicoAdicional->setValorDeclarado(0);

	    $encomenda = new \PhpSigep\Model\ObjetoPostal();
	    $encomenda->setServicosAdicionais(array($servicoAdicional));
	    $encomenda->setDestinatario($destinatario);
	    $encomenda->setDestino($destino);
	    $encomenda->setDimensao($dimensao);
	    $encomenda->setEtiqueta($etiqueta);
	    $encomenda->setPeso($peso);

	    $encomenda->setServicoDePostagem(new \PhpSigep\Model\ServicoDePostagem($this->shippingMethodCodeNumber));
		// ***  FIM DOS DADOS DA ENCOMENDA QUE SERÁ DESPACHADA *** //

		// *** DADOS DO REMETENTE *** //
	    $remetente = new \PhpSigep\Model\Remetente();
	    $remetente->setNome('LIV Style Ltda.');
	    $remetente->setLogradouro('Rua Correia de Melo');
	    $remetente->setNumero('84');
	    $remetente->setComplemento('Sala 206');
	    $remetente->setBairro('Bom Retiro');
	    $remetente->setCep('01123-020');
	    $remetente->setUf('SP');
	    $remetente->setCidade('São Paulo');
	    $remetente->setTelefone('11 945852940');
		// *** FIM DOS DADOS DO REMETENTE *** //

		$plp = new \PhpSigep\Model\PreListaDePostagem();
		$plp->setAccessData($this->accessData);
		$plp->setEncomendas(array($encomenda));
		$plp->setRemetente($remetente);

		$this->sigepPlp = $plp;
	}

	public function imprimirEtiqueta($data) {
		$this->orderId = $data['order_id'];
		$this->sigepDv = $data['etiqueta'];
		$this->shippingMethodCodeNumber = $data['method_code'];

		$this->setConfig();
		$this->preListaServicos();

        $layoutChancela = array(\PhpSigep\Pdf\CartaoDePostagem::TYPE_CHANCELA_SEDEX);
   		$images = dirname(dirname(dirname(__DIR__))) . '/image/';
        $logo = $images . '/logo-etiqueta.png';

        $pdf = new \PhpSigep\Pdf\CartaoDePostagem($this->sigepPlp, $data['plp_id'], $logo, $layoutChancela);
        $pdf->render();
	}

	public function imprimirPlp($data) {
		$this->orderId = $data['order_id'];
		$this->sigepDv = $data['etiqueta'];
		$this->shippingMethodCodeNumber = $data['method_code'];

		$this->setConfig();
		$this->preListaServicos();

		$pdf  = new \PhpSigep\Pdf\ListaDePostagem($this->sigepPlp, $data['plp_id']);
		$pdf->render('I');
	}
}
?>