<?php
class ModelPaymentRedeRest extends Model {
    public function install() {
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_rede_rest` (
            `order_rede_rest_id` INT(11) NOT NULL AUTO_INCREMENT,
            `order_id` INT(11) NULL,
            `returnCode` VARCHAR(3) NULL,
            `authorizationCode` VARCHAR(6) NULL,
            `tid` VARCHAR(20) NULL,
            `nsu` VARCHAR(12) NULL,
            `tipo` VARCHAR(7) NULL,
            `status` VARCHAR(15) NULL,
            `parcelas` VARCHAR(2) NULL,
            `bandeira` VARCHAR(10) NULL,
            `autorizacaoData` VARCHAR(29) NULL,
            `autorizacaoValor` DECIMAL(15,4) NULL,
            `capturaData` VARCHAR(29) NULL,
            `capturaValor` DECIMAL(15,4) NULL,
            `cancelaData` VARCHAR(29) NULL,
            `cancelaValor` DECIMAL(15,4) NULL,
            `json` TEXT NULL,
            PRIMARY KEY (`order_rede_rest_id`) );
        ");
    }

    public function updateTable() {
        $this->install();

        $fields = array(
            'order_rede_rest_id' => 'int(11)',
            'order_id' => 'int(11)',
            'returnCode' => 'varchar(256)',
            'authorizationCode' => 'varchar(6)',
            'tid' => 'varchar(20)',
            'nsu' => 'varchar(12)',
            'tipo' => 'varchar(7)',
            'status' => 'varchar(15)',
            'parcelas' => 'varchar(2)',
            'bandeira' => 'varchar(10)',
            'autorizacaoData' => 'varchar(29)',
            'autorizacaoValor' => 'decimal(15,4)',
            'capturaData' => 'varchar(29)',
            'capturaValor' => 'decimal(15,4)',
            'cancelaData' => 'varchar(29)',
            'cancelaValor' => 'decimal(15,4)',
            'json' => 'text'
        );

        $table = DB_PREFIX . "order_rede_rest";

        $field_query = $this->db->query("SHOW COLUMNS FROM `" . $table . "`");
        foreach ($field_query->rows as $field) {
            $field_data[$field['Field']] = $field['Type'];
        }

        foreach ($field_data as $key => $value) {
            if (!array_key_exists($key, $fields)) {
                $this->db->query("ALTER TABLE `" . $table . "` DROP COLUMN `" . $key . "`");
            }
        }

        $this->session->data['after_column'] = 'order_rede_rest_id';
        foreach ($fields as $key => $value) {
            if (!array_key_exists($key, $field_data)) {
                $this->db->query("ALTER TABLE `" . $table . "` ADD `" . $key . "` " . $value . " AFTER `" . $this->session->data['after_column'] . "`");
            }
            $this->session->data['after_column'] = $key;
        }
        unset($this->session->data['after_column']);

        foreach ($fields as $key => $value) {
            if ($key == 'order_rede_rest_id') {
                $this->db->query("ALTER TABLE `" . $table . "` CHANGE COLUMN `" . $key . "` `" . $key . "` " . $value . " NOT NULL AUTO_INCREMENT");
            } else {
                $this->db->query("ALTER TABLE `" . $table . "` CHANGE COLUMN `" . $key . "` `" . $key . "` " . $value);
            }
        }
    }

    public function uninstall() {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "order_rede_rest`;");
    }

    public function getOrderColumns() {
        $sql = "SHOW COLUMNS FROM `" . DB_PREFIX . "order`;";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTransactions() {
        $query = $this->db->query("
            SELECT orr.order_id, orr.order_rede_rest_id, o.date_added, CONCAT(o.firstname, ' ', o.lastname) as customer, orr.tipo, orr.status
            FROM `" . DB_PREFIX . "order_rede_rest` orr 
            INNER JOIN `" . DB_PREFIX . "order` o ON (o.order_id = orr.order_id)
            WHERE orr.order_id > '0' ORDER BY orr.order_id DESC
        ");

        return $query->rows;
    }

    public function getOrder($data, $order_id) {
        $columns = implode(", ", array_values($data));

        $qry = $this->db->query("
            SELECT " . $columns . "
            FROM `" . DB_PREFIX . "order`
            WHERE order_id = '" . (int) $order_id . "';
        ");

        if ($qry->num_rows) {
            return $qry->row;
        } else {
            return false;
        }
    }

    public function getTransaction($order_rede_rest_id) {
        $qry = $this->db->query("
            SELECT orr.*, o.date_added
            FROM `" . DB_PREFIX . "order_rede_rest` orr
            INNER JOIN `" . DB_PREFIX . "order` o ON (o.order_id = orr.order_id)
            WHERE orr.order_rede_rest_id = '" . (int) $order_rede_rest_id . "';
        ");

        if ($qry->num_rows) {
            return $qry->row;
        } else {
            return false;
        }
    }

    public function updateTransactionStatus($data) {
        $this->db->query("
            UPDATE " . DB_PREFIX . "order_rede_rest
            SET status = '" . $this->db->escape($data['status']) . "'
            WHERE order_rede_rest_id = '" . (int) $data['order_rede_rest_id'] . "';
        ");
    }

    public function updateTransaction($data) {
        $this->db->query("
            UPDATE " . DB_PREFIX . "order_rede_rest
            SET status = '" . $this->db->escape($data['status']) . "',
                autorizacaoData = '" . $this->db->escape($data['autorizacaoData']) . "',
                autorizacaoValor = '" . $this->db->escape($data['autorizacaoValor']) . "',
                capturaData = '" . $this->db->escape($data['capturaData']) . "',
                capturaValor = '" . $this->db->escape($data['capturaValor']) . "',
                cancelaData = '" . $this->db->escape($data['cancelaData']) . "',
                cancelaValor = '" . $this->db->escape($data['cancelaValor']) . "'
            WHERE order_rede_rest_id = '" . (int) $data['order_rede_rest_id'] . "';
        ");
    }

    public function captureTransaction($data) {
        $this->db->query("
            UPDATE " . DB_PREFIX . "order_rede_rest
            SET status = '" . $this->db->escape($data['status']) . "',
                json = '" . $data['json'] . "'
            WHERE order_rede_rest_id = '" . (int) $data['order_rede_rest_id'] . "';
        ");
    }

    public function cancelTransaction($data) {
        $this->db->query("
            UPDATE " . DB_PREFIX . "order_rede_rest
            SET status = '" . $this->db->escape($data['status']) . "',
                json = '" . $data['json'] . "'
            WHERE order_rede_rest_id = '" . (int) $data['order_rede_rest_id'] . "';
        ");
    }

    public function deleteTransaction($order_rede_rest_id) {
        $this->db->query("
            DELETE FROM `" . DB_PREFIX . "order_rede_rest`
            WHERE order_rede_rest_id = '" . (int) $order_rede_rest_id . "';
        ");
    }

    public function getOrderShipping($order_id) {
        $query = $this->db->query("
            SELECT * FROM `" . DB_PREFIX . "order_total`
            WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC;
        ");

        $result = array();

        foreach ($query->rows as $total) {
            if ($total['value'] > 0) {
                if ($total['code'] == "shipping") {
                    $result[] = array(
                        'code'  => $total['code'],
                        'title' => $total['title'],
                        'value' => $total['value']
                    );
                }
            }
        }

        return $result;
    }
}