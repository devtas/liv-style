<div class="tile">
  <div class="tile-heading"><?php echo $heading_title; ?> <span class="pull-right">
    <?php echo $percentage_completed; ?>% </span></div>
  <div class="tile-body"><i class="fa fa-credit-card"></i>
    <h2 class="pull-right"><?php echo $total_completed; ?><small style="color:white"> (<?php echo $total; ?>)</small></h2>
  </div>
  <div class="tile-footer"><a href="<?php echo $sale; ?>"><?php echo $text_view; ?></a></div>
</div>
