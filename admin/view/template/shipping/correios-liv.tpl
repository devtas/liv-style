<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content">
  	<div class="page-header">
    	<div class="container-fluid">
			<h1>Pedido não encontrado!</h1>
			<p>Algum dado não foi informado corretamente.</p>
		</div> 
	</div> 
</div> 

<?php echo $footer; ?>