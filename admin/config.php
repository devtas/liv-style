<?php

	define('HTTP_SERVER', 'http://livstyle.localhost/admin/');
	define('HTTP_CATALOG', 'http://livstyle.localhost/');

	define('HTTPS_SERVER', 'http://livstyle.localhost/admin/');
	define('HTTPS_CATALOG', 'http://livstyle.localhost/');

	define('DIR_APPLICATION', '/Users/thiagoandrade/projects/liv-style/admin/');
	define('DIR_SYSTEM', '/Users/thiagoandrade/projects/liv-style/system/');
	define('DIR_LANGUAGE', '/Users/thiagoandrade/projects/liv-style/admin/language/');
	define('DIR_TEMPLATE', '/Users/thiagoandrade/projects/liv-style/admin/view/template/');
	define('DIR_CONFIG', '/Users/thiagoandrade/projects/liv-style/system/config/');
	define('DIR_IMAGE', '/Users/thiagoandrade/projects/liv-style/image/');
	define('DIR_CACHE', '/Users/thiagoandrade/projects/liv-style/system/cache/');
	define('DIR_DOWNLOAD', '/Users/thiagoandrade/projects/liv-style/system/download/');
	define('DIR_UPLOAD', '/Users/thiagoandrade/projects/liv-style/system/upload/');
	define('DIR_LOGS', '/Users/thiagoandrade/projects/liv-style/system/logs/');
	define('DIR_MODIFICATION', '/Users/thiagoandrade/projects/liv-style/system/modification/');
	define('DIR_CATALOG', '/Users/thiagoandrade/projects/liv-style/catalog/');

	define('DB_DRIVER', 'mysqli');
	define('DB_HOSTNAME', 'localhost');
	define('DB_USERNAME', 'root');
	define('DB_PASSWORD', 'root');
	define('DB_DATABASE', 'livstyle');
	define('DB_PREFIX', 'oc_');
